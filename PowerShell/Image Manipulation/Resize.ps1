
<# 
    .SYNOPSIS 
    Its a script to resize images based on given width and height
 
    .DESCRIPTION 
    Its a script for resizing all images in a directory based on input 
	width and height. If height or width is less than 1 i.e., 0.11 
	or 0.5 or 0.98 then its taken as percentage and when width or height
	is more than 1 then its taken as exact pixels and then resized.
		
	.Parameter dir
		directory of images which needs to be resized

	.Parameter width
		width of image or percentage of total width needed to resized.

	.Parameter height
		height of image or percentage of total height needed to resized.

		
	.Example
		# PS> Resize -dir "C:\Users\others\Documents\Scanned Documents" -width 0.15 -height 0.15

		Description
		-----------
		Resizes images in directory with 15% of original aspects
		
	.Example
		# PS> Resize -dir "C:\Users\others\Documents\Scanned Documents" -width 230 -height 340

		Description
		-----------
		Resizes images in directory with exact pixels of original image
		
	.NOTES 
		File Name  : Resize.ps1
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3 
		Date 	   : 15-July-2014
#> 

Param (
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string] $dir = $(Throw New-Object System.ArgumentException "-Directory Path","dir"),
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [decimal] $width = $(Throw New-Object System.ArgumentException "-Width","width"),	
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [decimal] $height = $(Throw New-Object System.ArgumentException "-Height","height")	
)

[bool]$isPercentage = $False

Get-ChildItem -Path $dir | 
    Get-image | 
    ForEach-Object {
        $image = $_
		if(($width -lt 1) -and ($height -lt 1))
		{
			$isPercentage = $True
		}
		
		if($isPercentage)
		{
			$filter = Add-ScaleFilter -width $width -height $height -image $image -passThru
		}
		else
		{
			$filter = Add-ScaleFilter -width $width -height $height -passThru
		}
        $image = $image | Set-ImageFilter -filter $filter -passThru
		If (test-path $_.FullName) {Remove-Item $_.FullName}
        $image.SaveFile($_.FullName)
    }

<#
$images = Get-ChildItem -Path $dir -Include *.jpg

Foreach($image in $images)
{
	$imageFile  = New-Object -ComObject Wia.ImageFile        
	$imageProcess  = New-Object -ComObject Wia.ImageProcess
	$vector = New-Object -ComObject Wia.Vector
	$imageFile.LoadFile($image.FullName)
	$vector = $imageFile.ARGBData;
	Write-Output $vector.GetType()
	
	For ($arg=1;$arg -lt $vector.Count;$arg+=21)
	{
		#Write-Output $arg
		if($arg -lt $vector.Count)
		{
			#$vector[$arg] = 0xFFFF00FF
		}
		#opaque pink (A=255,R=255,G=0,B=255)
	}
	
	#Write-Output $imageFile.Width
	#Write-Output $imageFile.Height
}
#>
