#----------------------------------------------------------------------------
#Variables
#----------------------------------------------------------------------------

[xml]$global:rssFeed=$null
[xml]$global:xmlDoc=$null
[System.XML.XMLElement]$global:xmlRoot=$null
[bool]$global:firstRun=$True
[string]$global:fileName="updates.xml"
[string]$global:dirName="C:\Users\others\Downloads\Firefox Downloads\FilehippoSoftwares\"
[string[]]$global:softwaresAvailable = $()

#----------------------------------------------------------------------------
#Functions
#----------------------------------------------------------------------------

#Usage
function Get-Help
{
	Write-Host "Usage:`n"
	Write-Host 'Additional Information : 
	This script generates/updates software updates from filehippo.com 
	'
}

#Download XML
function DownloadFileHippoRSSFeeds
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$path
)
	[xml]$filehippoRss = (New-Object System.Net.WebClient).DownloadString($path)
	Set-Variable -Name rssFeed -Value $filehippoRss -Scope Global
}


#Create empty XML Document with 'softwares' as its first node
function CreateFirstRunXML
{
	[System.XML.XMLDocument]$global:xmlDoc=New-Object System.XML.XMLDocument

	# New Node
	[System.XML.XMLElement]$global:xmlRoot=$global:xmlDoc.CreateElement("softwares") 

	# Append as child to an existing node
	$global:xmlDoc.appendChild($global:xmlRoot) | Out-Null
	
	#Write-Output $global:xmlDoc
}


#Get software name from filehippo download url
function GetNameFromUrl
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$url,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$downloadNumRegex
)
	[string]$url_temp = ""
	#Sample http://filehippo.com/download_utorrent/57837/
	$url_temp = $url -replace "http://filehippo.com/download_", ""
	$url_temp = $url_temp -replace $downloadNumRegex,""
	return $url_temp
}

#Create empty XML Document with 'softwares' as its first node
function GenerateFirstRunXML
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$rssNode,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$imgSrcRegex
)
	#Write-Output $global:rssFeed."/rss/channel/item"
	#Iterate over rss items and gather info
	
	Select-Xml -Xml $global:rssFeed -Xpath $rssNode | ForEach-Object {
		[System.XML.XMLElement]$oXMLSystem=$global:xmlRoot.appendChild($global:xmlDoc.CreateElement("software"))
		$cdataSectionString = $_.Node.encoded.InnerText
		$cdataSectionString | ? {$_-match  $imgSrcRegex } | %{$matches} | Out-Null
		$imgSrc = $matches[2]
		$key = GetNameFromUrl -url $_.Node.guid -downloadNumRegex "/[0-9]+/"
		$oXMLSystem.SetAttribute("key",$key)
		$oXMLSystem.SetAttribute("title",$_.Node.title)
		$oXMLSystem.SetAttribute("pubdate",$_.Node.pubdate)
		$oXMLSystem.SetAttribute("guid",$_.Node.guid)
		$oXMLSystem.SetAttribute("img",$imgSrc)
		$regex = "\d+\.\d+|\d+"
		$splittedTitle = [regex]::Split($_.Node.title, $regex)
		$name = $splittedTitle[0].Trim()
		$oXMLSystem.SetAttribute("name",$name)
	}
}


#Save XML
function SaveXML
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$dirPath,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$fileName
)
	$file = $dirPath+ $fileName

	# Save File
	$global:xmlDoc.Save($file)
}

#Check if filehippo software updates xml is existing
function CheckFileExists
{
	if($global:dirName.EndsWith("\") -ne $True)
	{
		$global:dirName = $global:dirName + "\"
	}
	$path = $global:dirName + $global:fileName 
	if((Test-Path -path $path) -eq $True)
	{
		$global:firstRun=$False
	}
}

#Load FileHippo Software updates XML file
function LoadUpdatesXmlFile
{
	$path = $global:dirName + $global:fileName 
	
	#create XML document to hold the next set of updates
	[System.XML.XMLDocument]$global:xmlDoc=New-Object System.XML.XMLDocument
	
	# load it into an XML object:
	$global:xmlDoc.Load($path)
}

#Update XML Document with 'softwares' from subsequent RSS calls on a daily basis
function UpdateSoftwareXmlFile
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$nodePath,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$rssNode,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$imgSrcRegex
)
	
	$global:xmlRoot = $global:xmlDoc.SelectSingleNode($nodePath)
	
	Select-Xml -Xml $global:rssFeed -Xpath $rssNode | ForEach-Object {
		[System.XML.XMLElement]$oXMLSystem=$global:xmlRoot.appendChild($global:xmlDoc.CreateElement("software"))
		$cdataSectionString = $_.Node.encoded.InnerText
		$cdataSectionString | ? {$_-match  $imgSrcRegex } | %{$matches} | Out-Null
		$imgSrc = $matches[2]
		$key = GetNameFromUrl -url $_.Node.guid -downloadNumRegex "/[0-9]+/"
		$oXMLSystem.SetAttribute("key",$key)
		$oXMLSystem.SetAttribute("title",$_.Node.title)
		$oXMLSystem.SetAttribute("pubdate",$_.Node.pubdate)
		$oXMLSystem.SetAttribute("guid",$_.Node.guid)
		$oXMLSystem.SetAttribute("img",$imgSrc)
		$regex = "\d+\.\d+|\d+"
		$splittedTitle = [regex]::Split($_.Node.title, $regex)
		$name = $splittedTitle[0].Trim()
		$oXMLSystem.SetAttribute("name",$name)
	}
}


Try
{
	DownloadFileHippoRSSFeeds -path "http://feeds.feedburner.com/filehippo?format=xml" 
	
	#check if file exists already then append results to it
	CheckFileExists
	
	if($global:firstRun -eq $True)
	{
		CreateFirstRunXML 
	
		GenerateFirstRunXML -rssNode "//rss/channel/item" -imgSrcRegex '(src=\")(.*?)(\")'
	}
	else
	{
		LoadUpdatesXmlFile
		
		UpdateSoftwareXmlFile -nodePath "//softwares" -rssNode "//rss/channel/item" -imgSrcRegex '(src=\")(.*?)(\")'
	}
	
	SaveXML -dirPath "C:\Users\others\Downloads\Firefox Downloads\FilehippoSoftwares\" -fileName "updates.xml"
}
Catch
{
	#Write-Error "Error : $($_.Exception.Message)" 
	Write-Host "Error : $($_.Exception.Message)"  -ForegroundColor red
	Get-Help
} 




<#
$tmp = $global:xmlDoc.SelectNodes("//softwares/*")
$cnt = $tmp.Count

Write-Output = $global:xmlDoc.SelectNodes("//softwares/software") | ForEach-Object {
		Write-Output $_.Node.title
	}

Select-Xml -Xml $global:xmlDoc -Xpath "//softwares/software" | ForEach-Object {
	$global:softwaresAvailable += $_.Node.title
}
#>
<#
foreach( $item in $global:rssFeed)
{
	Write-Output $item	
	$cdataSectionString = $item
	#Write-Output $cdataSectionString
	$cdataSectionString | ? {$_-match  '(src=\")(.*?)(\")' } | %{$matches} | Out-Null
	$imgSrc = $matches[2]
	
	[System.XML.XMLElement]$oXMLSystem=$global:xmlRoot.appendChild($global:xmlDoc.CreateElement("software"))
	$oXMLSystem.SetAttribute("title",$item.title)
	$oXMLSystem.SetAttribute("pubdate",$item.pubdate)
	$oXMLSystem.SetAttribute("guid",$item.guid)
	$oXMLSystem.SetAttribute("img",$imgSrc)
	$cdataSectionString = ""
} 
#>

<#
$_.Node | Select-Xml -XPath $cdataNode | ForEach-Object {
	Write-Output $_.Node.InnerText
	$cdataSectionString = $_.Node.InnerText
	Write-Output $cdataSectionString
}
#>
#$dateString = Get-Date -format d-M-yyyyTH.m.s
#[System.XML.XMLElement]$oXMLSystem=$oXMLRoot.appendChild($oXMLDocument.CreateElement("software"))
#[System.Xml.XmlNamespaceManager] $nsm = New-Object System.Xml.XmlNamespaceManager $filehippoRss.NameTable
#$nsm.AddNamespace("ns1", "http://purl.org/rss/1.0/modules/content/")
# Try and get the node, then return it. Returns $null if the node was not found.
#$node = $item.SelectNodes("//::ns1:encoded['#cdata-section']", $nsm)
#Write-Output Select-Xml -Xml $item -XPath "content:encoded" -Namespace $nsm 
#Write-Output $node
#$cdataSectionString = $node."#cdata-section"
#$cdataAsXml =[xml]$cdataSectionString | ConvertTo-XML -As string
#$cdataAsXml = "<?xml version=`"1.0`" encoding=`"utf-8`" ?>" + $cdataSectionString
#Write-Output $cdataAsXml
#$img= $cdataAsXml.SelectNodes("/div")
#$imgSrc = $cdataSectionString | % { [regex]::matches( $_ , '(<img\s+)(.*?)(\s+/>){1}' ) } | Select -expa Value | % { [regex]::matches( $_ , '(?<=src\=\")(.*?)(?=\")' ) } | Select
#$matches = @()
#$some = $cdataSectionString | ? {$_  -match '(<img\s+)(.*?)(\s/>)' } | %{$matches}
#Write-Output $cdataSectionString | ? {$_  -match '(http:\/\/cache.filehippo)' } | %{$matches}
#Write-Output $some.GetType()
#| % { [regex]::match( $_ , '(<img\s+)(.*?)(\s+/>){1}' ) } | Select Value 
#| % { [regex]::matches( $_ , '(?<=src\=\")(.*?)(?=\")' ) } | Select
#Write-Output $matches[2]
#Write-Output "\r\n"
#exit