<# 
.SYNOPSIS 
This script downloads all modules defined in the specific url
https://bitbucket.org/vbsampath/scripts/src/master/PowerShell/Profile/?at=master

.DESCRIPTION 
This script downloads modules from above url and then installs them into user powershell modules directory

.NOTES 
	File Name  : InstallModules.ps1
	Author     : Sampath Vangari - vbsampath@gmail.com
	Requires   : PowerShell V2 CTP3  
	Date 	   : 15-June-2014
	
.LINK 
	http://psget.net/

.LINK
	https://bitbucket.org/vbsampath/scripts
#> 
$global:isDryRun=$False
[string]$global:tempDownloadDir=$env:TEMP 
[string]$global:remoteRepositoryKeyName="scripts"
[string]$global:remoteRepositoryKey="master"
[string]$global:remoteRepositoryBaseUrl="https://bitbucket.org/vbsampath/"
[string]$global:remoteScriptsBasePath = $null
[System.Array]$global:modulesDownloaded=$null
[xml]$global:xmlDoc=$null
$global:userDocumentsDir = [Environment]::GetFolderPath('MyDocuments')
$global:defaultPowershellDir="WindowsPowerShell"
[string]$global:webfolderDownloadsXml="https://bitbucket.org/vbsampath/scripts/raw/master/PowerShell/Utilities/InstallModules.xml"


function Main
{
<# 
    .SYNOPSIS 
    This function is main caller for all other functions
 
    .DESCRIPTION 
    This function is main caller for all other functions. This is the source of execution
	
	.NOTES 
		Function Name  : Main
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3  
		Date 	   : 15-June-2014
#> 
	Download-ProfileScripts
	Generate-ProfilePaths
	Import-UserModules
}

function DownloadFile  
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$directory,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$file,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$url
)  
	[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
	$webclient = New-Object System.Net.WebClient  
	
	$path = Join-Path $directory $file
	
	try  
	{  
		Write-Host "Downloading $file from $url" -Foreground Green
		if($global:isDryRun -eq $False)
		{
			$webclient.DownloadFile($url, $path)  
		}
	}  
	catch [System.Net.WebException]  
	{  
		Write-Host("Cannot download $url")  
		Write-Error "Error : $($_.Exception.Message)" 
	} 
	finally  
	{    
		$webclient.Dispose()
	}  
}  

function Download-ProfileScripts
{
<# 
    .SYNOPSIS 
    This function downloads scripts from the web into necessary folders
 
    .DESCRIPTION 
    This function downloads profile scripts from the web directory and stores them into a temporary location.
	It also creates a identifier directory for easy recognition
	
	.NOTES 
		Function Name  : Download-ProfileScripts
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3  
		Date 	   : 15-June-2014
#>

	#If temp identifier directory is already existing then delete it to start from fresh
	If (Test-Path $(Join-Path $global:tempDownloadDir "DownloadedPowerShellProfile")){
		Remove-Item -Recurse -Force $(Join-Path $global:tempDownloadDir "DownloadedPowerShellProfile")
	}
	
	#Creating a directory is temp folder to identify our folder easily
	New-Item -Path $(Join-Path $global:tempDownloadDir "DownloadedPowerShellProfile") -Type Directory | Out-Null
	
	[string]$modulePath = Join-Path $global:tempDownloadDir "DownloadedPowerShellProfile"
	$global:remoteScriptsBasePath = $global:remoteRepositoryBaseUrl+$global:remoteRepositoryKeyName+"/raw/"+$global:remoteRepositoryKey+"/PowerShell/Profile/"
	$path = $global:webfolderDownloadsXml
	
	#create XML document to hold the next set of updates
	[System.XML.XMLDocument]$global:xmlDoc=New-Object System.XML.XMLDocument
	
	# load it into an XML object:
	$global:xmlDoc.Load($path)
	
	$nodes = $global:xmlDoc.SelectNodes("//downloads/*")
	
	foreach($node in $nodes)
	{
		if($node.type -eq "File")
		{
			$url = $global:remoteScriptsBasePath + $node.name
			if($global:isDryRun -eq $False)
			{
				DownloadFile -directory $modulePath -file $node.name -url $url
			}
		}
		if($node.type -eq "Folder")
		{
			$path = Join-Path $modulePath $node.name
			if($global:isDryRun -eq $False)
			{
				New-Item -Path "$path" -Type Directory | Out-Null
			}
		}
		if($node.HasChildNodes)
		{
			#usage of $node.name is not good but it works. Change it to more better logic
			$modulePath = Join-Path $modulePath $node.name
			$folderNodes = $node.ChildNodes
			foreach($folderNode in $folderNodes)
			{
				$path = Join-Path $modulePath $folderNode.name
				$global:modulesDownloaded += $folderNode.name
				if($global:isDryRun -eq $False)
				{
					New-Item -Path "$path" -Type Directory | Out-Null
				}
				if($folderNode.HasChildNodes)
				{
					$fileNodes = $folderNode.ChildNodes
					foreach($fileNode in $fileNodes)
					{
						$url = $global:remoteScriptsBasePath + $node.name +"/"+$folderNode.name+"/"+$fileNode.name
						if($global:isDryRun -eq $False)
						{
							DownloadFile -directory $path -file $fileNode.name -url $url
						}
					}
				}
			}
		}
	}
}

function Generate-ProfilePaths
{
<# 
    .SYNOPSIS 
    This function generates profile paths and copies scripts from temp location
 
    .DESCRIPTION 
    This function generates profile paths if not existing and then copies downloaded scripts to this new path. ie., user powershell path
	
	.NOTES 
		Function Name  : Generate-ProfilePaths
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3  
		Date 	   : 15-June-2014
#>
	$destinationDir = Join-Path $global:userDocumentsDir $global:defaultPowershellDir
	if(Test-Path -Path $destinationDir)
	{
		Copy-ScriptsFromTemp
	}
	else
	{
		New-Item -Path $destinationDir -Type Directory | Out-Null
		Copy-ScriptsFromTemp
	}
}

function Copy-ScriptsFromTemp
{
<# 
    .SYNOPSIS 
    This function copies profile scripts from temp to destination dir 
 
    .DESCRIPTION 
    This function copies profile scripts from temp to destination dir 
	
	.NOTES 
		Function Name  : Copy-ScriptsFromTemp
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3  
		Date 	   : 15-June-2014
#>
	$destinationDir = Join-Path $global:userDocumentsDir $global:defaultPowershellDir
	$tempDir = Join-Path $global:tempDownloadDir "DownloadedPowerShellProfile"
	
	Copy-Item $tempDir\* $destinationDir -recurse
}

function Import-UserModules
{
<# 
    .SYNOPSIS 
    This function imports downloaded user modules
 
    .DESCRIPTION 
    This function imports downloaded user modules and also checks if the script has executionPolicy permissions and guides accordingly.
	After installation it gives necessary information on how to use it and what all are installed
	
	.NOTES 
		Function Name  : Import-UserModules
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3  
		Date 	   : 15-June-2014
#>
	$executionPolicy = (Get-ExecutionPolicy)
    $executionRestricted = ($executionPolicy -eq "Restricted")
    if ($executionRestricted){
        Write-Warning @"
Your execution policy is $executionPolicy, this means you will not be able import or use any scripts including modules.
To fix this change your execution policy to something like RemoteSigned.

PS> Set-ExecutionPolicy RemoteSigned

For more information execute:
PS> Get-Help about_execution_policies

"@
    }

    if (!$executionRestricted){
		#nothing to import as of now because in the profile every time it loads all profiles
		$modulesDownloadedString = [string]::join(", ",$global:modulesDownloaded)
		Write-Host "Following modules '$modulesDownloadedString' were installed. Just close this session and open it again for changes to reflect" -Foreground Magenta
		Write-Host @"
Modules :

$($global:modulesDownloaded[0]) : Aliases
$($global:modulesDownloaded[1]) : User profile related functions
$($global:modulesDownloaded[2]) : UI related stuff
$($global:modulesDownloaded[3]) : General utility functions
$($global:modulesDownloaded[4]) : This module contains ZenburnPowerShell which is an alternative to native powershell

General : Type "my" in shell to get functions, aliases and other stuff defined in user modules

ZenburnPowerShell : Type "New-ZenburnPowerShell "ZenburnPowerShell"" to install ZenburnPowerShell and then follow onscreen information

Examples :

PS> my
PS> New-ZenburnPowerShell "ZenburnPowerShell"

"@ -Foreground Green
    }
}


Try
{	
	Main
}
Catch
{
	#Write-Error "Error : $($_.Exception.Message)" 
	Write-Host "Error : $($_.Exception.Message)"  -ForegroundColor red
}