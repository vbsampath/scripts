<# 
.SYNOPSIS 
This script gets various information of google play games

.DESCRIPTION 
This script gets most prominent information of google play games
like developer name, price, ratings, downloads, comments etc.,

.NOTES 
    File Name  : GooglePlayGames.ps1
    Author     : Sampath Vangari - vbsampath@gmail.com
    Requires   : PowerShell V2 CTP3  
    Date       : 21-July-2014
    
.LINK 
    https://play.google.com/store/apps/category/GAME

#> 

[System.Reflection.Assembly]::loadwithpartialname('System.Data') | Out-Null

$global:isDryRun=$False
[string]$global:googlePlayUrl="https://play.google.com"
[string]$global:googlePlayGamePermissionsUrl = "https://play.google.com/store/xhr/getdoc"
[regex]$global:permissionsRegex = "(\[\[\""Google\sPlay\slicence\scheck\"",)(([\w+\""\]\[\s\,\.\'\-\;\:])+)(\]\n\]\n\]\n\]\n)"
#ids=com.gameloft.android.ANMP.GloftGGHM&xhr=1
[string]$global:gamesBaseUrl="https://play.google.com/store/apps/category/GAME"
[string]$global:gamesCardsBaseUrl="https://play.google.com/store/apps/category/"
[string]$global:topsellingFree = "/collection/topselling_free"
[string]$global:topsellingPaid = "/collection/topselling_paid"
[string]$global:gamesSubCamegory="GAME_ACTION"
#[string]$global:subCategoriesXPath="/html[1]/body[1]/div[3]/div[4]/div[1]/div[1]/div[2]/div[3]/div[2]/div[1]/ul/*"
[string]$global:subCategoriesXPath="//div[@id=""action-dropdown-children-Subcategories""]"
[string]$global:subCategoriesNameUrlXPath="/div/a"
#reviewType=0&pageNum=3&id=com.gameloft.android.ANMP.GloftGGHM&reviewSortOrder=2&xhr=1
#card image sizes
#170
#340
#max 512
#detail page 300
$script:Cards = @{}
$script:CategoryPriceCards = @{}
$script:Authors = @{}
[array]$script:categories = @()
$script:dbserver="WAVE\MSSQLSERVER2008"
$script:dbname="PlayStore"
[System.Data.SqlClient.SqlConnection]$script:sqlConnection = $Null
[System.Data.SqlClient.SqlCommand]$script:sqlCommand = New-Object System.Data.SqlClient.SqlCommand
[System.Data.SqlClient.SqlParameterCollection]$script:sqlParameterCollection = $Null
$script:queries = @{}
$script:queries.Add("GameCard","SET NOCOUNT ON; INSERT INTO dbo.GameCard (Name,Code,Url) VALUES (@Name,@Code,@Url); SELECT SCOPE_IDENTITY() as [InsertedID];")
$script:queries.Add("GameCategory","SET NOCOUNT ON; INSERT INTO dbo.Category (Name,Url) VALUES (@Name,@Url); SELECT SCOPE_IDENTITY() as [InsertedID];")
$script:queries.Add("GameCategories","SELECT * FROM dbo.Category")
$script:queries.Add("GameCards","SELECT * FROM dbo.GameCard")
$script:queries.Add("GameCardsComments","SELECT gc.Id, gc.Code, gc.Url FROM dbo.GameCard gc LEFT JOIN dbo.GameDetails gd ON gc.Code = gd.GameId WHERE gd.GameId IS NOT NULL AND gc.Id > 29 ORDER BY gc.Id")
#$script:queries.Add("GameCards","SELECT * FROM dbo.GameCard WHERE Code IN ('tap.the.white.guitar.tile.flag')")
$script:authorCardsPerPage = 60
$script:reviewCardsPerPage = 20
$script:maxGameCardsToFetch = 100
$script:previousCardsCount = 0
$script:gameCardPagesInfo = @{}
$script:gameCommentsPagesInfo = @{}
[int]$script:pageNum = 0
$script:DBCategories = New-Object System.Data.DataTable
$script:DBCards = New-Object System.Data.DataTable
$script:XPATH = @{}
$gameCardXPATH = @{}
$script:xmlData = $Null
$dbErrorProperties = @{
    ErrorNumber=''; 
    ErrorSeverity= '';
    ErrorState= '';
    ErrorProcedure= '';
    ErrorLine= '';
    ErrorMessage= '';
    Message= '';
}
$script:dbErrorTemplate= New-Object -TypeName PSObject -Property $dbErrorProperties
$script:recordsInserted=0
$DBNull = [System.DBNull]::Value 
$script:currentPageCommentsFetched=0
$script:previousPageCommentsFetched=0
$script:totalPageCommentsFetched=0


function Main
{
<# 
    .SYNOPSIS 
    This function is main caller for all other functions
 
    .DESCRIPTION 
    This function is main caller for all other functions. This is the source of execution
	If you don't know where this is called look to the bottom of the script
    
    .NOTES 
        Function Name  : Main
        Author     : Sampath Vangari - vbsampath@gmail.com
        Requires   : PowerShell V2 CTP3  
        Date       : 21-July-2014
#> 
	
	Setup-HtmlAgilityPack
    Setup-JsonLibrary
    #Get-SubCategories  -url $global:gamesBaseUrl
	#Get-CardsInfo -url $global:gamesCardsBaseUrl
	Get-DBConnection
    #Get-DBSubCategories
    #Get-DBGameCards
    #Get-GameCardDetails
    Get-DBGameCardsForComments
    Get-Comments
	#Save-SubCategories
	#Save-Cards
	Close-DBConnection
}


function Get-SubCategories  
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$url
)  
    try  
    {  
        Write-Host "Getting Games Subcategories from $url" -Foreground Green
        if($global:isDryRun -eq $False)
        {
			[HtmlAgilityPack.HtmlDocument] $htmlDoc = new-object HtmlAgilityPack.HtmlDocument
			[HtmlAgilityPack.HtmlWeb] $webGet = new-object HtmlAgilityPack.HtmlWeb
			$htmlDoc = $webGet.Load($url);
			$subMenuNodes = $htmlDoc.DocumentNode.SelectNodes($global:subCategoriesXPath + '/div[1]/ul[1]/*')
			
			$subMenuNodes | ForEach-Object {
				[HtmlAgilityPack.HtmlNode] $subMenuNode = $_
				$nodeXPath = $_.XPATH
				$subMenuInfoNode = $subMenuNode.SelectSingleNode($nodeXPath + $global:subCategoriesNameUrlXPath)
				
				$Category = New-Object PSObject                                       
				   $Category | add-member -MemberType Noteproperty -Name Name -Value $subMenuInfoNode.GetAttributeValue("title","")
				   $Category | add-member -MemberType Noteproperty -Name Url -Value ($global:googlePlayUrl + $subMenuInfoNode.GetAttributeValue("href",""))
				   
				$CardPageInfo = New-Object PSObject                                       
				   $CardPageInfo | add-member -MemberType Noteproperty -Name Page -Value 0
				   $CardPageInfo | add-member -MemberType Noteproperty -Name Offset -Value 0
				
				$script:gameCardPagesInfo.Add(("Free"+$Category.Name),$CardPageInfo)
                
                if($script:CategoryPriceCards.ContainsKey("Free"+$Category.Name) -eq $False)
				{
                    $gameList = @{}
					$script:CategoryPriceCards.Add(("Free"+$Category.Name),$gameList)
				}
                
				$CardPageInfo = New-Object PSObject                                       
				   $CardPageInfo | add-member -MemberType Noteproperty -Name Page -Value 0
				   $CardPageInfo | add-member -MemberType Noteproperty -Name Offset -Value 0
				   
				$script:gameCardPagesInfo.Add(("Paid"+$Category.Name),$CardPageInfo)
                
                if($script:CategoryPriceCards.ContainsKey("Paid"+$Category.Name) -eq $False)
				{
                    $gameList = @{}
					$script:CategoryPriceCards.Add(("Paid"+$Category.Name),$gameList)
				}
				   
				$script:categories += $Category
			}
        }
    }  
    catch [System.Net.WebException]  
    {  
        Write-Host("Cannot download $url")  
        Write-Error "Error : $($_.Exception.Message)" 
    } 
} 

function Get-GameCardsFirstPage  
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $category,
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$Price
)  
	try  
    {  
		switch ($Price)
		{
			"Free" { $url=$global:topsellingFree }
			"Paid" { $url=$global:topsellingPaid }
		}
        
        if($global:isDryRun -eq $False)
        {
			[HtmlAgilityPack.HtmlDocument] $htmlDoc = new-object HtmlAgilityPack.HtmlDocument
			[HtmlAgilityPack.HtmlWeb] $webGet = new-object HtmlAgilityPack.HtmlWeb
			$htmlDoc = $webGet.Load($category.Url + $url);
			$cardListXPath = "//div[@class=""card-list""]/*"
			$cardList = $htmlDoc.DocumentNode.SelectNodes($cardListXPath)
			$script:gameCardsCount = $cardList.Count
			
			Generate-GameObjectsFromHtml -cardList $cardList -category $category -Price $Price
        }
    }  
    catch [System.Net.WebException]  
    {  
        Write-Host("Cannot download $url")  
        Write-Error "Error : $($_.Exception.Message)" 
    } 
} 

function Generate-GameObjectsFromHtml
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $cardList,
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $category,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$Price
)
	if($cardList -ne $Null)
	{
        $gameCardsList = $script:CategoryPriceCards.Get_Item(($Price+$category.Name))
		$cardList | ForEach-Object {
			[HtmlAgilityPack.HtmlNode] $card = $_
			
			$nodeXPath = $_.XPATH
			$hrefNode = $card.SelectSingleNode($nodeXPath + "/div/div[2]/h2/a")
			
			$gameTitle = $hrefNode.GetAttributeValue("title","")
			$gameUrl = ($global:googlePlayUrl + $hrefNode.GetAttributeValue("href",""))
			
			$queryParts = Get-UrlQueryParams -inputurl $gameUrl
			[string]$gameId = $queryParts.Get_Item("id")
			
			$Game = New-Object PSObject                                       
			   $Game | add-member -MemberType Noteproperty -Name Name -Value $gameTitle
			   $Game | add-member -MemberType Noteproperty -Name Url -Value $gameUrl
			   $Game | add-member -MemberType Noteproperty -Name Code -Value $gameId

			if($gameCardsList -ne $Null)
			{
				if($gameCardsList.ContainsKey($Game.Code) -eq $False)
				{
					$gameCardsList.Add($Game.Code,$Game)
				}
			}
		}
        $script:CategoryPriceCards.Set_Item($($Price+$category.Name), $gameCardsList)
	}
}

function Get-UrlQueryParams
{
[OutputType([System.Collections.Hashtable])]
param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$inputurl
)
	[System.Uri]$url = $inputurl
	$queryParts = @{}
	$queryPartsArray = ($url.Query.Substring(1)) -split "&"
	foreach ($queryPart in $queryPartsArray)
	{
		$element=@()
		$element= $queryPart.split("=")
		$queryParts.add($element[0], $element[1])
	}
	return $queryParts
}

function Get-CardsInfo  
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$url
)  
    try  
    {  
        Write-Host "Getting Cards for categories" -Foreground Green
        if($global:isDryRun -eq $False)
        {
			$switchPrice = @("Free","Paid")
			$switchPriceCount = 0
			foreach($category in $script:categories)
			{
                Write-Host "Getting Games for $($category.Name)" -Foreground Green
                Get-GamePages -category $category -Price $switchPrice[0]
                Get-GamePages -category $category -Price $switchPrice[1]
			}
            Clean-GameCards
			Write-Host "Total Game Cards Fetched :$($script:Cards.Count)"
        }
    }  
    catch [System.Net.WebException]  
    {  
        Write-Host("Cannot download $url")  
        Write-Error "Error : $($_.Exception.Message)" 
    } 
}

function Get-GamePages
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $category,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$Price
)	
	Get-GameCardsFirstPage -category $category -Price $Price
	Set-GameCardPageInfo -category $category -Price $Price
	Get-GameCardPageNumber -category $category -Price $Price
	if($script:pageNum -gt 0)
	{
		Get-MorePages -category $category -Price $Price
		Set-GameCardPageInfo -category $category -Price $Price
	}
}

function Set-GameCardPageInfo
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $category,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$Price
)
	try
	{
		$gameCardPageInfo = $script:gameCardPagesInfo.Get_Item($Price+$category.Name)
		$gameCardPageInfo.Page = $($gameCardPageInfo.Page + 1)
		$gameCardPageInfo.Offset = $gameCardPageInfo.Offset + $script:gameCardsCount
		$script:gameCardPagesInfo.Set_Item(($Price+$category.Name),$gameCardPageInfo)
	}
	catch [Exception]
	{
        Write-Error "Error : $($_.Exception.Message)" 
	}
	
}

function Clean-GameCards
{
	try
	{
		foreach($category in $script:categories)
        {
            $Price = "Free"
            $gameList = $script:CategoryPriceCards.Get_Item(($Price+$category.Name))
            foreach ($game in $gameList.Keys) 
            {
                if($script:Cards.Count -eq 0)
                {
                    $script:Cards.Add($game, $gameList[$game])
                }
                elseif($script:Cards.ContainsKey($game) -eq $False)
                {
                    $script:Cards.Add($game, $gameList[$game])
                }
            }
            $Price = "Paid"
            $gameList = $script:CategoryPriceCards.Get_Item(($Price+$category.Name))
            foreach ($game in $gameList.Keys) 
            {
                if($script:Cards.Count -eq 0)
                {
                    $script:Cards.Add($game, $gameList[$game])
                }
                elseif($script:Cards.ContainsKey($game) -eq $False)
                {
                    $script:Cards.Add($game, $gameList[$game])
                }
            }
        }
	}
	catch [Exception]
	{
        Write-Error "Error : $($_.Exception.StackTrace)" 
	}
	
}

function Get-GameCardPageNumber
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $category,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$Price
)
	$gameCardPageInfo = $script:gameCardPagesInfo.Get_Item($Price+$category.Name)
	$script:pageNum = $gameCardPageInfo.Page 
}

function Get-MorePages
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $category,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$Price
)	
	Write-Host  "Fetching pages for $($Price) Games" -Foreground Green
	try
	{
        $gameCardsList = $script:CategoryPriceCards.Get_Item($Price+$category.Name)
		While ($script:previousCardsCount -ne $gameCardsList.Count)
		{
			$script:previousCardsCount =  $script:CategoryPriceCards.Get_Item($Price+$category.Name).Count
			Get-MoreCards -category $category -Price $Price
            Set-GameCardPageInfo -category $category -Price $Price
		}
        $script:previousCardsCount = 0
		Write-Output ""
	}
	catch [Exception]
	{
        Write-Error "Error : $($_.Exception.StackTrace)" 
	}
}

function Generate-HttpPostPayload
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $category,
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$Price
)
	$nvc = @{}
	try
	{
		$gameCardPageInfo = $script:gameCardPagesInfo.Get_Item($Price+$category.Name)
		$nvc.Add("start", $gameCardPageInfo.Offset)
		$nvc.Add("num", $script:maxGameCardsToFetch)
		$nvc.Add("numChildren", 0)
		$nvc.Add("xhr", 1)
	}
	catch [Exception]
	{
		Write-Host("An Error occured generating post payload")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
	}
	
	return $nvc
}

function Get-MoreCards  
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $category,
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$Price
)  
    try  
    {  
		switch ($Price)
		{
			"Free" { $url=$global:topsellingFree }
			"Paid" { $url=$global:topsellingPaid }
		}
        if($global:isDryRun -eq $False)
        {
			$queryHash = Generate-HttpPostPayload -category $category -Price $Price
			$htmlDoc = Do-HttpPost -queryHash $queryHash -url ($category.Url + $url)
			$cardListXPath = "//div[@class=""card-list""]/*"
			$cardList = $htmlDoc.DocumentNode.SelectNodes($cardListXPath) 
            if($cardList -ne $Null)
            {
                $script:gameCardsCount = $cardList.Count
			    Generate-GameObjectsFromHtml -cardList $cardList -category $category -Price $Price
            }
        }
		Write-Host -NoNewline "$($script:gameCardPagesInfo.Get_Item($Price+$category.Name).Page)" -Foreground Blue
    }  
    catch [System.Net.WebException]  
    {  
        Write-Error "Error : $($_.Exception.StackTrace)" 
    } 
	catch [Exception]  
    {  
        Write-Error "Error : $($_.Exception.StackTrace)" 
    } 
}

function Save-SubCategories
{  
	$insertedId = $Null
    try  
    {  
        Write-Host "Saving Game subcategories" -Foreground Green
        if($global:isDryRun -eq $False)
        {
			foreach($category in $script:categories)
			{
				$insertedId = Save-SubCategory -category $category
			}
			Write-Output "Saved Game Categories : $insertedId"
        }
    }  
    catch [System.Net.WebException]  
    {  
        Write-Host("Cannot download $url")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
    } 
}

function Save-Cards
{  
	$insertedId = $Null
    try  
    {  
        Write-Host "Saving Game Cards" -Foreground Green
        if($global:isDryRun -eq $False)
        {
			foreach($card in $script:Cards.Keys)
			{
				$insertedId = Save-Card -card $script:Cards.Get_Item($card)
			}
			Write-Output "Saved game cards : $insertedId"
        }
    }  
    catch [System.Net.WebException]  
    {  
        Write-Host("Cannot download $url")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
    } 
}

function Save-Card
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $card
)  
	$insertedId = $Null
    try  
    {  
        #Write-Host "Saving Game card" -Foreground Green
        if($global:isDryRun -eq $False)
        {
			#insert into database
			$script:sqlCommand.CommandText = $script:queries.Get_Item("GameCard")
			
			$sqlParameter = New-Object System.Data.SqlClient.SqlParameter
			$sqlParameter.ParameterName = "@Name"
			$sqlParameter.Value = $card.Name
			$sqlParameter.SqlDbType = "NVarChar" 
			$sqlParameter.Size = 100
			
			$script:sqlCommand.Parameters.Add($sqlParameter) | Out-Null
			
			$sqlParameter = New-Object System.Data.SqlClient.SqlParameter
			$sqlParameter.ParameterName = "@Code"
			$sqlParameter.Value = $card.Code
			$sqlParameter.SqlDbType = "NVarChar" 
			$sqlParameter.Size = 100
			
			$script:sqlCommand.Parameters.Add($sqlParameter) | Out-Null
			
			$sqlParameter = New-Object System.Data.SqlClient.SqlParameter
			$sqlParameter.ParameterName = "@Url"
			$sqlParameter.Value = $card.Url
			$sqlParameter.SqlDbType = "NVarChar" 
			$sqlParameter.Size = 100
			
			$script:sqlCommand.Parameters.Add($sqlParameter) | Out-Null
			
			$insertedId = Execute-SQLQuery
        }
    }  
    catch [System.Data.SqlClient.SqlException]  
    {  
        Write-Host("Cannot Save Card ")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
    }
	Return $insertedId
} 

function Save-SubCategory
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $category
)  
	$insertedId = $Null
    try  
    {  
        #Write-Host "Saving Game subcategory" -Foreground Green
        if($global:isDryRun -eq $False)
        {
			#insert into database
			$script:sqlCommand.CommandText = $script:queries.Get_Item("GameCategory")
			
			$sqlParameter = New-Object System.Data.SqlClient.SqlParameter
			$sqlParameter.ParameterName = "@Name"
			$sqlParameter.Value = $category.Name
			$sqlParameter.SqlDbType = "NVarChar" 
			$sqlParameter.Size = 50
			
			$script:sqlCommand.Parameters.Add($sqlParameter) | Out-Null
			
			$sqlParameter = New-Object System.Data.SqlClient.SqlParameter
			$sqlParameter.ParameterName = "@Url"
			$sqlParameter.Value = $category.Url
			$sqlParameter.SqlDbType = "NVarChar" 
			$sqlParameter.Size = 150
			
			$script:sqlCommand.Parameters.Add($sqlParameter) | Out-Null
			
			$insertedId = Execute-SQLQuery
        }
    }  
    catch [System.Data.SqlClient.SqlException]  
    {  
        Write-Host("Cannot download $url")  
        Write-Error "Error : $($_.Exception.Message)" 
    } 
	Return $insertedId
} 

function Get-DBConnection
{
	$script:sqlConnection = New-Object System.Data.SqlClient.SqlConnection
	$script:sqlConnection.ConnectionString = "Server=$script:DBServer;Database=$script:DBName;Integrated Security=True;"
	try
	{
		$sqlConnection.Open()
	}
	catch [System.Data.SqlClient.InvalidOperationException]  
    {  
        Write-Host("The connection is already open.")  
        Write-Error "Error : $($_.Exception.Message)" 
    } 
	catch [System.Data.SqlClient.SqlException]
	{
		Write-Host("An Error occured while getting connection")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
}

function Execute-SQLQuery
{ 
	$script:sqlCommand.connection = $script:sqlConnection
		
	try
	{
        $InsertedID = $script:sqlCommand.ExecuteScalar()
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting connection")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
	finally
	{
		$script:sqlCommand.Parameters.Clear()
	}
	
	Return $InsertedID
}

function Execute-SQLQueryReader
{ 
	$script:sqlCommand.connection = $script:sqlConnection
    [System.Data.DataTable]$Datatable = New-Object System.Data.DataTable
    
	try
	{
        #Execute the Command
        $sqlReader = $script:sqlCommand.ExecuteReader()
        $DataTable.Load($SqlReader)
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting connection")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
	
	Return $DataTable
}

function Close-DBConnection
{
	try
	{
		if ($script:sqlConnection.State -eq [Data.ConnectionState]::Open) {
			$script:sqlConnection.Close()
		}
	}
	catch [System.Data.SqlClient.SqlException]
	{
		Write-Host("An Error occured while closing connection")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
}

function Setup-HtmlAgilityPack
{
	$invocationDir = Get-ScriptDirectory
	$htmlAgilityPackDir = Join-Path $invocationDir "dependencies/HtmlAgilityPack.dll"
	$assembly = [system.reflection.assembly]::LoadFile($htmlAgilityPackDir)
} 

function Setup-JsonLibrary
{
	$invocationDir = Get-ScriptDirectory
	$jsonLibDir = Join-Path $invocationDir "dependencies/Newtonsoft.Json.dll"
	$assembly = [system.reflection.assembly]::LoadFile($jsonLibDir)
}


function Get-ScriptDirectory
{
	$path = (split-path $SCRIPT:MyInvocation.MyCommand.Path -Parent)
	return $path 
}

function Do-HttpPost
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $queryHash,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$url
)
	try
	{
		[HtmlAgilityPack.HtmlDocument] $htmlDoc = New-object HtmlAgilityPack.HtmlDocument
		
		$payload = Assemble-PostPayload -queryHash $queryHash
		$bytesToPost = [System.Text.Encoding]::UTF8.GetBytes($payload)
		$webRequest = [System.Net.WebRequest]::Create($url)
		$webRequest.Method = "POST"
		$webRequest.ContentType = "application/x-www-form-urlencoded"
		$webRequest.ContentLength = $bytesToPost.Length
		$requestStream = $webRequest.GetRequestStream()
		$requestStream.Write($bytesToPost, 0, $bytesToPost.Length)
		$requestStream.Close()
		
		[System.Net.WebResponse]$response = $webRequest.GetResponse()
		$responseStream = $response.GetResponseStream()
		$responseStreamReader = New-Object System.IO.StreamReader -ArgumentList $responseStream
		[string]$responseString = $responseStreamReader.ReadToEnd()
		$htmlDoc.LoadHtml($responseString) | Out-Null
	}
	catch [Exception]
	{
		Write-Host("An Error occured during post operation")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
	}
	
	return $htmlDoc
}

function Assemble-PostPayload
{param(
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $queryHash
)
	[System.Text.StringBuilder] $sb = New-Object System.Text.StringBuilder
	try
	{
		foreach ($key in $queryHash.Keys) {
			$sb.Append("&" + $key + "=" + $queryHash[$key]) | Out-Null
		}
	}
	catch [Exception]
	{
		Write-Host("An Error occured assembling post payload")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
	}
	
	Return $sb.ToString().Substring(1)
}

function Get-DBSubCategories 
{
    $script:sqlCommand.CommandText = $script:queries.Get_Item("GameCategories")
		
	try
	{
        $script:DBCategories = Execute-SQLQueryReader
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting data")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
	}
}

function Get-DBGameCards
{
    $script:sqlCommand.CommandText = $script:queries.Get_Item("GameCards")
		
	try
	{
        $script:DBCards = Execute-SQLQueryReader
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting data")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
	}
}

function Get-GameCardDetails 
{
	try {
		foreach ($Row in $script:DBCards)
        { 
            $Game = New-Object PSObject                                       
			   $Game | add-member -MemberType Noteproperty -Name Name -Value $Row["Name"].ToString()
			   $Game | add-member -MemberType Noteproperty -Name Url -Value $Row["Url"].ToString()
			   $Game | add-member -MemberType Noteproperty -Name Code -Value $Row["Code"].ToString()
            Get-GameDetails -Game $Game
        }
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting game details")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
}

function Get-GameAdditionalDetails 
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[HtmlAgilityPack.HtmlNodeCollection]
		$additionalDetailNodes
	)
	try 
    {
		$additionalDetails = @{}
        foreach ($additionalDetailNode in $additionalDetailNodes) 
        {
            [string]$title = $Null
            [string]$data = $Null
            
            $nodeXPath = $additionalDetailNode.XPATH
            $titleNode = $additionalDetailNode.SelectSingleNode($nodeXPath + "/div[1]")
            $title = $titleNode.InnerHtml
            $exlcudedAdditionalDetails = @("Permissions","Report")
            if(($exlcudedAdditionalDetails -contains $title) -eq $False)
            {
                if($title -eq "Contact Developer")
                {
                    $developerDetails = @{}
                    $developerDetailsNodes = $additionalDetailNode.SelectNodes($nodeXPath + "/div[2]/*")
                    foreach ($developerDetailsNode in $developerDetailsNodes) 
                    {
                    	$devTitle = $developerDetailsNode.InnerHtml
                        $devGoogleUrl = $developerDetailsNode.GetAttributeValue("href","")
                        if($devTitle -eq "Email Developer")
                        {
                            $devUrl = $devGoogleUrl.Replace("mailto:","")
                        }
                        else
                        {
                            [System.Collections.Hashtable]$queryParts = Get-UrlQueryParams -inputurl $devGoogleUrl
                            $devUrl = $queryParts.Get_Item("q")
                        }
                        $developerDetails.Add($devTitle, $devUrl)
                    }
                    $additionalDetails.Add($title, $developerDetails)
                }
                else
                {
                    $dataNode = $additionalDetailNode.SelectSingleNode($nodeXPath + "/div[2]")
                    $data = $dataNode.InnerHtml 
                    $additionalDetails.Add($title, $data)
                }
            }
        }
	}
	catch 
    {
		Write-Host("An Error occured while getting game info additional details")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
	}
    Return $additionalDetails
}

function Get-GameDetailsRatings 
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[HtmlAgilityPack.HtmlNodeCollection]
		$ratingHistogramNodes
	)
    $ratingHistogram = @{}
	try 
    {
        foreach ($ratingHistogramNode in $ratingHistogramNodes) 
        {
            $nodeXPath = $ratingHistogramNode.XPATH
            $ratingNumberNode = $ratingHistogramNode.SelectSingleNode($nodeXPath + "/span[1]")
            $ratingNumber = $ratingNumberNode.InnerText
            $PeopleNode = $ratingHistogramNode.SelectSingleNode($nodeXPath + "/span[3]")
            [int]$People = $PeopleNode.InnerHtml
        	$ratingHistogram.Add($ratingNumber, $People)
        }
	}
	catch 
    {
		Write-Host("An Error occured while getting game info ratings details")  
        Write-Error "Error : $($_.Exception.StackTrace)"
	}
    Return $ratingHistogram
}

function Get-GameDetailsThumbnails
{
	[CmdletBinding()]
	[OutputType([System.Collections.ArrayList])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[HtmlAgilityPack.HtmlNodeCollection]
		$thumbnailsNodes
	)
    $thumbnails = @()
	try 
    {
        foreach ($thumnail in $thumbnailsNodes) {
        	if($thumnail.Name -eq "img")
            {
                $thumbnails += $thumnail.GetAttributeValue("src","")
            }
        }
	}
	catch 
    {
		Write-Host("An Error occured while getting game info thumbnails details")  
        Write-Error "Error : $($_.Exception.StackTrace)"
	}
    Return $thumbnails
}

function Get-GameDetailsChanges
{
	[CmdletBinding()]
    [OutputType([string])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[HtmlAgilityPack.HtmlNodeCollection]
		$whatsnewAndDetailsNodes
	)
    [string]$changes=''
	try 
    {
        foreach ($whatsnewAndDetailsNode in $whatsnewAndDetailsNodes) 
        {
        	$changes = $changes + $whatsnewAndDetailsNode.OuterHtml
        }
	}
	catch 
    {
		Write-Host("An Error occured while getting game info changes details")  
        Write-Error "Error : $($_.Exception.StackTrace)"
	}
    Return $changes
}

function Parse-GameDetailsParseInfo
{
	[CmdletBinding()]
	[OutputType([System.Collections.ArrayList])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[HtmlAgilityPack.HtmlDocument]
		$htmlDoc,
        
        [Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[System.Collections.ArrayList]
		$GameDetailsParseInfos
	)
    
    $GameDetails = @{}
	try 
    {
        foreach ($GameDetailsParseInfo in $GameDetailsParseInfos) 
        {
        	$Name = $GameDetailsParseInfo.Name
            $XPath = $GameDetailsParseInfo.XPath
            $OperationType = $GameDetailsParseInfo.OperationType
            $OperationValue = $GameDetailsParseInfo.OperationValue
            $node = $htmlDoc.DocumentNode.SelectSingleNode($XPath)
            if($OperationType -eq "InnerHtml")
            {
                if($node -ne $Null)
                {
                    $nodeHtml = $node.InnerHtml
                    if($Name -eq "Price")
                    {
                        if($nodeHtml -eq "Install")
                        {
                            $nodeHtml = 0
                        }
                        else
                        {
                            $nodeHtml = $nodeHtml.Replace("Rs.","").Replace(" Buy","")
                        }
                    }
                    
                    $GameDetails.Add($Name, $nodeHtml)    
                }
            }
            elseif($OperationType -eq "Attribute")
            {
                if($node -ne $Null)
                {
                    $nodeHtml = $node.GetAttributeValue($OperationValue,"")
                    if($Name -eq "AuthorUrl")
                    {
                        $nodeHtml = $global:googlePlayUrl + $nodeHtml
                    }
                    if($Name -eq "VideoImage")
                    {
                        $nodeHtml = "https:" + $nodeHtml
                    }
                    $GameDetails.Add($Name, $nodeHtml)
                }
            }
        }
	}
	catch 
    {
		Write-Host("An Error occured while getting game info changes details")  
        Write-Error "Error : $($_.Exception.Message)"
	}
    Return $GameDetails
}

function Generate-GameDetailsParseInfo
{
    [System.Collections.ArrayList]$GameDetailsParseInfos = @()
	try 
    {
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Category"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//div[@class=""info-container""]/div[3]/a[1]/span[1]"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Price"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//button[@class=""price buy""]/span[2]"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Logo"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//div[@class=""cover-container""]/img"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "Attribute"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value "src"
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "InAppPurchases"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//div[@class=""inapp-msg""]"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Author"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//a[@class=""document-subtitle primary""]/span"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
           
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Rating"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//div[@class=""score-container""]/div"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "RatedPeople"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//span[@class=""reviews-num""]"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Description"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//div[@class=""id-app-orig-desc""]"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "IsTopDeveloper"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//span[@class=""badge-title""]"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "AuthorUrl"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//div[@itemprop=""author""]/meta"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "Attribute"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value "content"
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "VideoImage"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//span[@class=""details-trailer""]/span/img"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "Attribute"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value "src"
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
        
        $GameDetailsParseInfo = New-Object PSObject                                       
            $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "VideoUrl"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "//span[@class=""details-trailer""]/span[2]"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "Attribute"
		    $GameDetailsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value "data-video-url"
            
        $GameDetailsParseInfos += $GameDetailsParseInfo
	}
	catch 
    {
		Write-Host("An Error occured while getting game info changes details")  
        Write-Error "Error : $($_.Exception.StackTrace)"
	}
    Return $GameDetailsParseInfos
}

function ParseItem($jsonItem) {
    if($jsonItem.Type -eq "Array") {
        return ParseJsonArray($jsonItem)
    }
    elseif($jsonItem.Type -eq "Object") {
        return ParseJsonObject($jsonItem)
    }
    else {
        return $jsonItem.ToString()
    }
}

function ParseJsonObject($jsonObj) {
    $result = @{}
    $jsonObj.Keys | ForEach-Object {
        $key = $_
        $item = $jsonObj[$key]
        $parsedItem = ParseItem $item
        $result.Add($key,$parsedItem)
    }
    return $result
}

function ParseJsonArray($jsonArray) {
    $result = @()
    $jsonArray | ForEach-Object {
        $parsedItem = ParseItem $_
        $result += $parsedItem
    }
    return $result
}

function ParseJsonString($json) {
    $config = [Newtonsoft.Json.Linq.JObject]::Parse($json)
    return ParseJsonObject($config)
}

function ParseJsonFile($fileName) {
    $json = (Get-Content $FileName | Out-String)
    return ParseJsonString $json
}
#todo
function Get-GamePermissions 
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[System.String]
		$Game
	)
	try 
    {
        [HtmlAgilityPack.HtmlDocument] $permissionsDoc = new-object HtmlAgilityPack.HtmlDocument
        $queryHash = @{}
        $queryHash.Add("ids","com.gameloft.android.ANMP.GloftGGHM")
        $queryHash.Add("xhr","1")
        $permissionsDoc = Do-HttpPost -queryHash $queryHash -url $script:googlePlayGamePermissionsUrl
        $permissionsJson = $permissionsDoc.DocumentNode.InnerHtml
        $permissionsJson = $permissionsJson.Substring(4)
        $permissionsJson = $permissionsJson.SubString($permissionsJson.IndexOf("{"), $permissionsJson.LastIndexOf("}") - $permissionsJson.IndexOf("{") + 1)
        $permissionsJson = $permissionsJson.replace("[,","["""",").replace(",,",",")
        $permissionsJson = $permissionsJson.replace("`n","").replace("`r","")
        
        $permissions = [Newtonsoft.Json.Linq.JObject]::Parse($permissionsJson)
        $mainPermissionsJson = $permissions.Root["42656262"][1][0]
        $otherPermissionsJson = $permissions.Root["42656262"][1][1]
        $otherPermissions1Json = $permissions.Root["42656262"][1][2]
        $permissions = ParseJsonArray -jsonArray $mainPermissionsJson
        $otherPermissions = ParseJsonArray -jsonArray $otherPermissionsJson
        $otherPermissions1 = ParseJsonArray -jsonArray $otherPermissions1Json
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting game permission details")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
}

function Save-GameDetails 
{
    [CmdletBinding()]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		$Game
	)
	try 
    {
        $script:sqlCommand.CommandText = "[dbo].[spGameDetailsImport]"
        $script:sqlCommand.CommandType = [System.Data.CommandType]::StoredProcedure
        
        $script:sqlCommand.Parameters.Clear()
        
        $value = $script:xmlData.OuterXml
        $value = $value.Replace("�","''")
        Out-File -filepath E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\test.txt -inputobject $value -encoding Unicode
        
        $script:sqlCommand.Parameters.Add("@inputxml",[system.data.SqlDbType]::XML) | Out-Null
        $script:sqlCommand.Parameters['@inputxml'].Direction = [system.data.ParameterDirection]::Input
        $script:sqlCommand.Parameters['@inputxml'].value = $value
         
        #$script:sqlCommand.ExecuteNonQuery()
        #[System.Data.SqlClient.SqlDataReader] $sqlReader = $script:sqlCommand.ExecuteReader()
        $rdr = $script:sqlCommand.ExecuteReader()
        While($rdr.Read()){
            $error = $script:dbErrorTemplate.PSObject.Copy()
        
            $error.ErrorNumber = $rdr['ErrorNumber']
            $error.ErrorSeverity = $rdr['ErrorSeverity']
            $error.ErrorState = $rdr['ErrorState']
            $error.ErrorProcedure = $rdr['ErrorProcedure']
            $error.ErrorLine = $rdr['ErrorLine']
            $error.ErrorMessage = $rdr['ErrorMessage']
            $error.Message = $rdr['Message']
            
            if($Script:DBNull.Equals($error.ErrorMessage))
            {
                #inserted properly
                $script:recordsInserted = $script:recordsInserted + 1
                Write-Output "$($Game.Code)"
                $Game.Code | Out-File -append -filepath E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\gamesFetched.txt
            }
            else
            {
                #error exists
                Write-Output "$($error.ErrorMessage) $($Game.Code)"
            }
        }
        
        $rdr.Close()
	}
	catch [Exception]
	{
		Write-Host("An Error occured while saving game info details")  
        Write-Error "Error : $($_.Exception.Message)" 
        #Write-Error "Error : $($_.Exception.StackTrace)"
	}
}

function Generate-GameDetailsObjectXmlDataTransport
{
	[CmdletBinding()]
	[OutputType([System.Xml.XmlDocument])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		$GameDetailsObject
	)
    # Create a new XML File with config root node
    $gameObjectXml = New-Object System.Xml.XmlDocument
	try 
    {
        $decl = $gameObjectXml.CreateXmlDeclaration("1.0", $null, $null)
        $gameObjectXml.InsertBefore($decl, $gameObjectXml.DocumentElement) | Out-Null
        
        # New Node
        [System.XML.XMLElement]$gameObjectRoot=$gameObjectXml.CreateElement("game") 
        # Append as child to an existing node
        $gameObjectXml.appendChild($gameObjectRoot) | Out-Null
        
        [System.XML.XMLElement]$details=$gameObjectRoot.appendChild($gameObjectXml.CreateElement("details")) 
        [System.XML.XMLElement]$category=$gameObjectRoot.appendChild($gameObjectXml.CreateElement("category")) 
        [System.XML.XMLElement]$author=$gameObjectRoot.appendChild($gameObjectXml.CreateElement("author")) 
        [System.XML.XMLElement]$video=$gameObjectRoot.appendChild($gameObjectXml.CreateElement("video")) 
        [System.XML.XMLElement]$ratings=$gameObjectRoot.appendChild($gameObjectXml.CreateElement("ratings")) 
        [System.XML.XMLElement]$thumbnails=$gameObjectRoot.appendChild($gameObjectXml.CreateElement("thumbnails")) 
        
        
        #populating details node
        foreach ($key in $GameDetailsObject.AdditionalDetails.Keys) {
            if($key -ne 'Contact Developer')
            {
                [System.XML.XMLElement]$paramElement = $gameObjectXml.CreateElement('param') 
                [System.XML.XMLElement]$nameElement = $gameObjectXml.CreateElement('name') 
                [System.XML.XMLElement]$valueElement = $gameObjectXml.CreateElement('value') 
                
                if($GameDetailsObject.AdditionalDetails[$key] -ne $Null)
                {
                    $nameElementTextNode = $gameObjectXml.CreateTextNode($key.Replace(' ',''))
                    if($($GameDetailsObject.AdditionalDetails[$key]).GetType().FullName -eq 'System.String')
                    {
                        $valueElementTextNode = $gameObjectXml.CreateTextNode(($GameDetailsObject.AdditionalDetails[$key]).Trim())
                    }
                    else
                    {
                        $valueElementTextNode = $gameObjectXml.CreateTextNode(($GameDetailsObject.AdditionalDetails[$key]))
                    }
                    
                    $nameElement.appendChild($nameElementTextNode) | Out-Null
                    $valueElement.appendChild($valueElementTextNode) | Out-Null
                    
                    $paramElement.appendChild($nameElement) | Out-Null
                    $paramElement.appendChild($valueElement) | Out-Null
                    
                    $details.appendChild($paramElement) | Out-Null
                }
            }
		}
        
        foreach ($detailObject in $GameDetailsObject.Details) 
        {
            [System.XML.XMLElement]$paramElement = $gameObjectXml.CreateElement('param') 
            [System.XML.XMLElement]$nameElement = $gameObjectXml.CreateElement('name') 
            [System.XML.XMLElement]$valueElement = $gameObjectXml.CreateElement('value') 
            
            if($($detailObject.value) -ne $Null)
            {
                $nameElementTextNode = $gameObjectXml.CreateTextNode($detailObject.name.Replace(' ',''))
                if($($detailObject.value).GetType().FullName -eq 'System.String')
                {
                    $valueElementTextNode = $gameObjectXml.CreateTextNode(($detailObject.value).Trim())
                }
                else
                {
                    $valueElementTextNode = $gameObjectXml.CreateTextNode($detailObject.value)
                }
                
                $nameElement.appendChild($nameElementTextNode)  | Out-Null
                $valueElement.appendChild($valueElementTextNode)  | Out-Null
                
                $paramElement.appendChild($nameElement)  | Out-Null
                $paramElement.appendChild($valueElement)  | Out-Null
                
                $details.appendChild($paramElement)  | Out-Null
            }
		}
        
        
        #populating category node
        $category.appendChild($gameObjectXml.CreateTextNode($GameDetailsObject.Category)) | out-Null
        
        #populating author node
        foreach ($authorOjb in $GameDetailsObject.Author) 
        {
            [System.XML.XMLElement]$paramElement = $gameObjectXml.CreateElement('param')
            [System.XML.XMLElement]$nameElement = $gameObjectXml.CreateElement('name')
            [System.XML.XMLElement]$valueElement = $gameObjectXml.CreateElement('value')
            
            if($($authorOjb.value) -ne $Null)
            {
                $nameElementTextNode = $gameObjectXml.CreateTextNode($authorOjb.name)
                if($($authorOjb.value).GetType().FullName -eq 'System.String')
                {
                    $valueElementTextNode = $gameObjectXml.CreateTextNode(($authorOjb.value).Trim())
                }
                else
                {
                    $valueElementTextNode = $gameObjectXml.CreateTextNode(($authorOjb.value))
                }
                
                $nameElement.appendChild($nameElementTextNode) | Out-Null
                $valueElement.appendChild($valueElementTextNode) | Out-Null
                
                $paramElement.appendChild($nameElement) | Out-Null
                $paramElement.appendChild($valueElement) | Out-Null
                
                $author.appendChild($paramElement) | Out-Null
            }
		}
        
        #populating video node
        foreach ($videoObj in $GameDetailsObject.Video) 
        {
            [System.XML.XMLElement]$paramElement = $gameObjectXml.CreateElement('param')
            [System.XML.XMLElement]$nameElement = $gameObjectXml.CreateElement('name')
            [System.XML.XMLElement]$valueElement = $gameObjectXml.CreateElement('value')
            
            if($($videoObj.value) -ne $Null)
            {
                $nameElementTextNode = $gameObjectXml.CreateTextNode($videoObj.name)
                if($($videoObj.value).GetType().FullName -eq 'System.String')
                {
                    $valueElementTextNode = $gameObjectXml.CreateTextNode(($videoObj.value).Trim())
                }
                else
                {
                    $valueElementTextNode = $gameObjectXml.CreateTextNode(($videoObj.value))
                }
                
                $nameElement.appendChild($nameElementTextNode) | Out-Null
                $valueElement.appendChild($valueElementTextNode) | Out-Null
                
                $paramElement.appendChild($nameElement) | Out-Null
                $paramElement.appendChild($valueElement) | Out-Null
                
                $video.appendChild($paramElement) | Out-Null
            }
		}
        
        #populating ratings node
        foreach ($ratingObj in $GameDetailsObject.RatingHistogram.Keys) 
        {
            [System.XML.XMLElement]$paramElement = $gameObjectXml.CreateElement('param')
            [System.XML.XMLElement]$nameElement = $gameObjectXml.CreateElement('name')
            [System.XML.XMLElement]$valueElement = $gameObjectXml.CreateElement('value')
            
            if($GameDetailsObject.RatingHistogram[$ratingObj] -ne $Null)
            {
                $nameElementTextNode = $gameObjectXml.CreateTextNode($ratingObj)
                $valueElementTextNode = $gameObjectXml.CreateTextNode($GameDetailsObject.RatingHistogram[$ratingObj])
                
                $nameElement.appendChild($nameElementTextNode) | Out-Null
                $valueElement.appendChild($valueElementTextNode) | Out-Null
                
                $paramElement.appendChild($nameElement) | Out-Null
                $paramElement.appendChild($valueElement) | Out-Null
                
                $ratings.appendChild($paramElement) | Out-Null
            }
		}
        
        
        #populating thumbnails node
        foreach ($thumbnailObj in $GameDetailsObject.Thumbnails) 
        {
            [System.XML.XMLElement]$thumbnailElement = $gameObjectXml.CreateElement('thumbnail')
            
            if($thumbnailObj -ne $Null)
            {
                $thumbnailElement.SetAttribute('type','thumbnail')
                $valueElementTextNode = $gameObjectXml.CreateTextNode($thumbnailObj)
                
                $thumbnailElement.appendChild($valueElementTextNode) | Out-Null
                
                $thumbnails.appendChild($thumbnailElement) | Out-Null
            }
		}
        
        #attaching logo in thumbnails
        [System.XML.XMLElement]$thumbnailElement = $gameObjectXml.CreateElement('thumbnail')
            
        if($GameDetailsObject.Logo -ne $Null)
        {
            $thumbnailElement.SetAttribute('type','logo')
            $valueElementTextNode = $gameObjectXml.CreateTextNode($GameDetailsObject.Logo)
            
            $thumbnailElement.appendChild($valueElementTextNode) | Out-Null
            
            $thumbnails.appendChild($thumbnailElement) | Out-Null
        }
        
        #Write-Output $gameObjectXml.InnerXml > E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\test.xml
        
        $script:xmlData = $gameObjectXml
	}
	catch [Exception]
	{
		Write-Host("An Error occured while generating game details object xml ")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
}

function Get-GameDetails 
{
	[CmdletBinding()]
	#[OutputType([System.Int32])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		$Game
	)
    $thumbnails = $Null
    $ratingHistogram = $Null
    $changes = $Null
    $additionalDetails = $Null
	try {
		[HtmlAgilityPack.HtmlDocument] $htmlDoc = new-object HtmlAgilityPack.HtmlDocument
		[HtmlAgilityPack.HtmlWeb] $webGet = new-object HtmlAgilityPack.HtmlWeb
        #$url = "https://play.google.com/store/apps/details?id=com.dvloper.slendrinacellarfree"
		$htmlDoc = $webGet.Load($Game.Url);
        #$htmlDoc = $webGet.Load($url);
        
        # complex logics and cannot be done by generic logic
        
		#todo
        #google plus info cant be fetched because iframe is not loading and its very dynamic
		#may be we should load it from iecplorer and get it by document.elementId - just research this
        
        $thumbnailsXPath = "//div[@class=""thumbnails""]/*"
        $ratingHistogramXPath = "//div[@class=""rating-histogram""]/*"
        $whatsnewAndDetailsXPath = "//div[@class=""recent-change""]"
        $additionalDetailsXPath = "//div[@class=""details-section metadata""]/div[2]/*"
        
        $GameDetailsParseInfos = Generate-GameDetailsParseInfo
        $GameDetails = Parse-GameDetailsParseInfo -htmlDoc $htmlDoc -GameDetailsParseInfos $GameDetailsParseInfos
        if($GameDetails.Keys.Count -eq 0)
        {
            #no game details fetched probably game deleted from google database
            #so return to other task from here
            Write-Output "$($Game.Code) deleted" | Out-File -append -filepath E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\gamesNotFetched.txt
            return
        }
        $topDeveloper = $null
        if ($GameDetails.ContainsKey("IsTopDeveloper")) 
        {
            $topDeveloper = $GameDetails["IsTopDeveloper"]
        } 
        
        $thumbnailsNodes = $htmlDoc.DocumentNode.SelectNodes($thumbnailsXPath)
        if($thumbnailsNodes -ne $Null)
        {
            $thumbnails = Get-GameDetailsThumbnails -thumbnailsNodes $thumbnailsNodes    
        }
        
        $ratingHistogramNodes = $htmlDoc.DocumentNode.SelectNodes($ratingHistogramXPath)
        if($ratingHistogramNodes -ne $Null)
        {
            $ratingHistogram = Get-GameDetailsRatings -ratingHistogramNodes $ratingHistogramNodes 
        }
        
        $whatsnewAndDetailsNodes = $htmlDoc.DocumentNode.SelectNodes($whatsnewAndDetailsXPath)
        if($whatsnewAndDetailsNodes -ne $Null)
        {
            $changes = Get-GameDetailsChanges -whatsnewAndDetailsNodes $whatsnewAndDetailsNodes
        }
        
        $additionalDetailNodes = $htmlDoc.DocumentNode.SelectNodes($additionalDetailsXPath)
        if($additionalDetailNodes -ne $Null)
        {
            $additionalDetails = Get-GameAdditionalDetails -additionalDetailNodes $additionalDetailNodes
        }
        
        $properties = @{name=''; value= '';}
        $detailsTemplate = New-Object -TypeName PSObject -Property $properties
        $detailsCollection=@()
        
        $detailsCurrent = $detailsTemplate.PSObject.Copy()
        $detailsCurrent.name = 'Name'
        $detailsCurrent.value = $($Game.Name)
        $detailsCollection +=$detailsCurrent
        
        $detailsCurrent = $detailsTemplate.PSObject.Copy()
        $detailsCurrent.name = 'Url'
        $detailsCurrent.value = $($Game.Url)
        $detailsCollection +=$detailsCurrent
        
        $detailsCurrent = $detailsTemplate.PSObject.Copy()
        $detailsCurrent.name = 'WhatsNew'
        $detailsCurrent.value = $changes
        $detailsCollection +=$detailsCurrent
        
        $detailsCurrent = $detailsTemplate.PSObject.Copy()
        $detailsCurrent.name = 'GameId'
        $detailsCurrent.value = $Game.Code
        $detailsCollection +=$detailsCurrent
        
        $exlcudedGameDetails = @("Author","AuthorUrl","VideoImage","VideoUrl","Category","Logo")
        foreach ($key in $GameDetails.Keys) {
            if(($exlcudedGameDetails -contains $key) -eq $False)
            {
                $value = ''
                switch ($key)
        		{
                    "InAppPurchases" 
                    {
                        if($($GameDetails[$key]) -eq "Offers in-app purchases")
                        {
                            $value = $True    
                        }
                        else
                        {
                            $value = $False
                        }
                    }
        			"IsTopDeveloper" 
                    { 
                        if($($GameDetails[$key]) -eq "Top Developer")
                        {
                            $value = $True    
                        }
                        else
                        {
                            $value = $False
                        }
                    }
                    "RatedPeople" 
                    { 
                        $value = $($GameDetails[$key]).Replace(',','')
                    }
                    default {$value = $($GameDetails[$key])}
        		}
                $detailsCurrent = $detailsTemplate.PSObject.Copy()
                $detailsCurrent.name = $key
                $detailsCurrent.value = $value
                $detailsCollection +=$detailsCurrent
            }
		}
        
        $category = $GameDetails['Category']
        
        $properties = @{name=''; value= '';}
        $authorTemplate = New-Object -TypeName PSObject -Property $properties
        $authorCollection=@()
        
        $authorCurrent = $detailsTemplate.PSObject.Copy()
        $authorCurrent.name = 'Name'
        $authorCurrent.value = $($GameDetails["Author"])
        $authorCollection +=$authorCurrent
        
        $authorCurrent = $detailsTemplate.PSObject.Copy()
        $authorCurrent.name = 'Url'
        $authorCurrent.value = $($GameDetails["AuthorUrl"])
        $authorCollection +=$authorCurrent
        
        $authorHash = $additionalDetails['Contact Developer']
        $authorCurrent = $detailsTemplate.PSObject.Copy()
        $authorCurrent.name = 'Website'
        $authorCurrent.value = $authorHash.Get_Item('Visit Developer''s Website')
        $authorCollection +=$authorCurrent
        
        $authorCurrent = $detailsTemplate.PSObject.Copy()
        $authorCurrent.name = 'Email'
        $authorCurrent.value = $authorHash.Get_Item('Email Developer')
        $authorCollection +=$authorCurrent
        
        $authorCurrent = $detailsTemplate.PSObject.Copy()
        $authorCurrent.name = 'PrivacyPolicy'
        $authorCurrent.value = $authorHash.Get_Item('Privacy Policy')
        $authorCollection +=$authorCurrent
        
        
        $properties = @{name=''; value= '';}
        $videoTemplate = New-Object -TypeName PSObject -Property $properties
        $videoCollection=@()
        
        $videoCurrent = $detailsTemplate.PSObject.Copy()
        $videoCurrent.name = 'Image'
        $videoCurrent.value = $($GameDetails["VideoImage"])
        $videoCollection +=$videoCurrent
        
        $videoCurrent = $detailsTemplate.PSObject.Copy()
        $videoCurrent.name = 'Url'
        $videoCurrent.value = $($GameDetails["VideoUrl"])
        $videoCollection +=$videoCurrent
        
        
        $GameDetailsObject = New-Object PSObject                                  
            $GameDetailsObject | add-member -MemberType Noteproperty -Name Details -Value $detailsCollection
            $GameDetailsObject | add-member -MemberType Noteproperty -Name Category -Value $category
            $GameDetailsObject | add-member -MemberType Noteproperty -Name Logo -Value $GameDetails["Logo"]
		    $GameDetailsObject | add-member -MemberType Noteproperty -Name Author -Value $authorCollection
            $GameDetailsObject | add-member -MemberType Noteproperty -Name Video -Value $videoCollection
            $GameDetailsObject | add-member -MemberType Noteproperty -Name Thumbnails -Value $thumbnails
            $GameDetailsObject | add-member -MemberType Noteproperty -Name RatingHistogram -Value $ratingHistogram
            $GameDetailsObject | add-member -MemberType Noteproperty -Name AdditionalDetails -Value $additionalDetails
                
        Generate-GameDetailsObjectXmlDataTransport -GameDetailsObject $GameDetailsObject
        Save-GameDetails -Game $Game
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting game info details")  
        Write-Error "Error : $($_.Exception.Message)" 
        #Throw $_
        #Return
	}
}

function Get-PageComments
{
	[CmdletBinding()]
    [OutputType([System.Collections.ArrayList])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		$Game
	)
    $Comments = @()
	try 
    {
        $queryHash = Generate-HttpPostPayloadComments -Game $Game
		$htmlDoc = Do-HttpPost -queryHash $queryHash -url "https://play.google.com/store/getreviews"
		
        #htmlDoc contains json so we need to parse before getting comments
        $commentsHtml = $htmlDoc.DocumentNode.OuterHtml
        $commentsHtml = $commentsHtml.SubString(4)
        $commentsHtml = $commentsHtml.replace("`n","").replace("`r","")
        $commentsHtml = $commentsHtml.replace("\u003d","=")
        $commentsHtml = $commentsHtml.replace("\u003c","<")
        $commentsHtml = $commentsHtml.replace("\u003e",">")
        $commentsHtml = $commentsHtml.replace("\u0026amp;","&")
        $commentsHtml = "{""comments"":"+$commentsHtml+"}"
        Out-File -filepath E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\comments.txt -inputobject $commentsHtml -encoding Unicode
        $commentsJson = [Newtonsoft.Json.Linq.JObject]::Parse($commentsHtml)
        $commentsJson = $commentsJson.Root["comments"]
        $commentsArray = ParseJsonArray -jsonArray $commentsJson 
        $commentsJsonHtml = $commentsArray[2]
		
        #for comment pages where there are no comments then the value will be zero
        if($commentsJsonHtml.Length -gt 0)
        {
            [HtmlAgilityPack.HtmlDocument] $htmlDoc = new-object HtmlAgilityPack.HtmlDocument
    		[HtmlAgilityPack.HtmlWeb] $webGet = new-object HtmlAgilityPack.HtmlWeb
    		$htmlDoc.LoadHtml($commentsJsonHtml);
            $commentsXPath="/div"
            $commentNodes = $htmlDoc.DocumentNode.SelectNodes($commentsXPath)
            
            $GameCommentsParseInfos = Generate-GameCommentsParseInfo
            foreach ($commentNode in $commentNodes) 
            {
                $Comment = Parse-GameCommentsParseInfo -htmlNode $commentNode -GameCommentsParseInfos $GameCommentsParseInfos
                $Comments += $Comment
            }
            $script:currentPageCommentsFetched=$Comments.Count
            $script:totalPageCommentsFetched +=$script:currentPageCommentsFetched
        }
        
        #wait for some time to let google think that we are not a bot
        Start-Sleep -s 3
        if(($script:totalPageCommentsFetched -eq 2000) -or ($script:totalPageCommentsFetched -gt 2000))
        {
            Start-Sleep -s 1300
            $script:totalPageCommentsFetched = 0
        }
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting game comments ")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
    Return $Comments
}

function Get-Comments 
{
	try {
		foreach ($Row in $script:DBCards)
        { 
            $Game = New-Object PSObject                                       
			   $Game | add-member -MemberType Noteproperty -Name Id -Value $Row["Id"].ToString()
			   $Game | add-member -MemberType Noteproperty -Name Url -Value $Row["Url"].ToString()
			   $Game | add-member -MemberType Noteproperty -Name Code -Value $Row["Code"].ToString()
            
            $Game.Code | Out-File -append -filepath E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\commentsFetched.txt
            Write-Output $Game.Code
            Get-GameAllComments -Game $Game
            
            #resetting comments fetched for next game comments
            $script:previouPageCommentsFetched = 0
            $script:currentPageCommentsFetched = 0
        }
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting game get comments")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
}

function Get-GameAllComments 
{
    [CmdletBinding()]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		$Game
	)
	try 
    {
        #$Game.Url = "https://play.google.com/store/apps/details?id=com.artfactory.af_gbe1"
        #$Game.Code = "com.artfactory.af_gbe1"
        #$Game.Id = 13
        
        #getting zero page and next pages are set by increasing count
        $script:gameCommentsPagesInfo[$Game.Id] = 0
        
		$GameComments = Get-PageComments -Game $Game
        
        if($GameComments -ne $Null)
        {
            Generate-GameCommentsObjectXmlDataTransport -GameComments $GameComments
            Save-PageComments -Game $Game
        
            #setting page number for further iterations
            $pageNum = $script:gameCommentsPagesInfo[$Game.Id]
            $pageNum = $pageNum + 1
            $script:gameCommentsPagesInfo[$Game.Id] = $pageNum
        }
        
        #if cur fetch = 20 and curr fetch = previous fetch continue = middle pages 
        #if cur fetch = 20 and previous fetch = 0 continue = second page
        #if cur fetch = 0 then 224 pages are fetched so exit
        While (
                (
                    ($script:currentPageCommentsFetched -eq 20) -and 
                    ($script:currentPageCommentsFetched -eq $script:previouPageCommentsFetched)
                ) -or 
                (
                    ($script:currentPageCommentsFetched -eq 20) -and 
                    ($script:previouPageCommentsFetched -eq 0)
                )
            )
		{
            if($script:gameCommentsPagesInfo[$Game.Id] -eq 223)
            {
                break
            }
            $script:previouPageCommentsFetched = $script:currentPageCommentsFetched
            $GameComments = Get-PageComments -Game $Game
            
            if($GameComments -ne $Null)
            {
                Generate-GameCommentsObjectXmlDataTransport -GameComments $GameComments
                Save-PageComments -Game $Game
            
                #setting page number for further iterations
                $pageNum = $script:gameCommentsPagesInfo[$Game.Id]
                $pageNum = $pageNum + 1
                $script:gameCommentsPagesInfo[$Game.Id] = $pageNum
            }
		}
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting game all comments")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
}

function Get-DBGameCardsForComments 
{
	$script:sqlCommand.CommandText = $script:queries.Get_Item("GameCardsComments")
		
	try
	{
        $script:DBCards = Execute-SQLQueryReader
	}
	catch [Exception]
	{
		Write-Host("An Error occured while getting data")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
}

function Generate-HttpPostPayloadComments
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] $Game
)
	$nvc = @{}
	try
	{
        $nvc.Add("id", $Game.Code)
		$nvc.Add("pageNum", $script:gameCommentsPagesInfo.Get_Item($Game.Id))
        $nvc.Add("reviewSortOrder", 2)
		$nvc.Add("reviewType", 0)
		$nvc.Add("xhr", 1)
	}
	catch [Exception]
	{
		Write-Host("An Error occured generating post payload")  
        Write-Error "Error : $($_.Exception.StackTrace)" 
	}
	
	return $nvc
}

function Generate-GameCommentsParseInfo
{
    [System.Collections.ArrayList]$GameCommentsParseInfos = @()
	try 
    {
        $GameCommentsParseInfo = New-Object PSObject                                       
            $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "AuthorImage"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "/a[1]/img[1]"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "Attribute"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value "src"
            
        $GameCommentsParseInfos += $GameCommentsParseInfo
        
        $GameCommentsParseInfo = New-Object PSObject                                       
            $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "AuthorUrl"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "/div[1]/div[1]/span[1]/a[1]"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "Attribute"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value "href"
            
        $GameCommentsParseInfos += $GameCommentsParseInfo
        
        $GameCommentsParseInfo = New-Object PSObject                                       
            $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Author"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "/div[1]/div[1]/span[1]/a[1]"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameCommentsParseInfos += $GameCommentsParseInfo
        
        $GameCommentsParseInfo = New-Object PSObject                                       
            $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "ReviewDate"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "/div[1]/div[1]/span[2]"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameCommentsParseInfos += $GameCommentsParseInfo
        
        $GameCommentsParseInfo = New-Object PSObject                                       
            $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Rating"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "/div[1]/div[1]/div[2]/div[1]"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "Attribute"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value "aria-label"
            
        $GameCommentsParseInfos += $GameCommentsParseInfo
           
        $GameCommentsParseInfo = New-Object PSObject                                       
            $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Title"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "/div[2]/span[1]"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameCommentsParseInfos += $GameCommentsParseInfo
           
        $GameCommentsParseInfo = New-Object PSObject                                       
            $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name Name -Value "Description"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name XPath -Value "/div[2]/span[1]"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationType -Value "InnerHtml"
		    $GameCommentsParseInfo | add-member -MemberType Noteproperty -Name OperationValue -Value $Null
            
        $GameCommentsParseInfos += $GameCommentsParseInfo
	}
	catch 
    {
		Write-Host("An Error occured while getting game comments")  
        Write-Error "Error : $($_.Exception.StackTrace)"
	}
    Return $GameCommentsParseInfos
}

function Parse-GameCommentsParseInfo
{
	[CmdletBinding()]
	[OutputType([System.Collections.HashTable])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[HtmlAgilityPack.HtmlNode]
		$htmlNode,
        
        [Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[System.Collections.ArrayList]
		$GameCommentsParseInfos
	)
    
    $GameComment = @{}
	try 
    {
        $anonymousUserXPath = @{}
        $anonymousUserXPath.Add("AuthorImage","/span[1]/img[1]")
        $anonymousUserXPath.Add("AuthorUrl","/div[1]/div[1]/span[1]/a[1]")
        $anonymousUserXPath.Add("Author","/div[1]/div[1]/span[1]")
        foreach ($GameCommentsParseInfo in $GameCommentsParseInfos) 
        {
        	$Name = $GameCommentsParseInfo.Name
            $XPath = $GameCommentsParseInfo.XPath
            $OperationType = $GameCommentsParseInfo.OperationType
            $OperationValue = $GameCommentsParseInfo.OperationValue
            $nodeXPath = $htmlNode.XPath
            
            #for a regular user html is good but for anonymous user xpath are different
            #authorurl is empty for anonymous user
            #author is "A Google User" for anonymous user
            if($anonymousUserXPath.ContainsKey($Name) -eq $True)
            {
                $node = $htmlNode.SelectSingleNode($nodeXPath + $XPath)
                if($node -eq $Null)
                {
                    $node = $htmlNode.SelectSingleNode($nodeXPath + $anonymousUserXPath[$Name])
                }
            }
            else
            {
                $node = $htmlNode.SelectSingleNode($nodeXPath + $XPath)
            }
            if($OperationType -eq "InnerHtml")
            {
                if($node -ne $Null)
                {
                    if($Name -eq "Description")
                    {
                        $nodeHtml = $node.NextSibling.InnerText    
                    }
                    elseif($Name -eq "Author")
                    {
                        $nodeHtml = $($node.InnerText).Trim()
                    }
                    else
                    {
                        $nodeHtml = $node.InnerHtml
                    }
                    $GameComment.Add($Name, $nodeHtml)    
                }
            }
            elseif($OperationType -eq "Attribute")
            {
                if($node -ne $Null)
                {
                    $nodeHtml = $node.GetAttributeValue($OperationValue,"")
                    if($Name -eq "AuthorUrl")
                    {
                        $nodeHtml = $global:googlePlayUrl + $nodeHtml
                    }
                    if($Name -eq "Rating")
                    {
                        $matches = Get-RegexMatches -inputString $nodeHtml -regexPattern '[0-9]'
                        if($matches -ne $null)
		                {
                            $nodeHtml = $matches[0].Get_Item(0)
                        }
                    }
                    $GameComment.Add($Name, $nodeHtml)
                }
            }
        }
	}
	catch 
    {
		Write-Host("An Error occured while getting game info changes details")  
        Write-Error "Error : $($_.Exception.Message)"
	}
    Return $GameComment
}

function Generate-GameCommentsObjectXmlDataTransport
{
	[CmdletBinding()]
	[OutputType([System.Xml.XmlDocument])]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
        [System.Collections.ArrayList]
		$GameComments
	)
    # Create a new XML File with config root node
    $gameObjectXml = New-Object System.Xml.XmlDocument
	try 
    {
        $decl = $gameObjectXml.CreateXmlDeclaration("1.0", $null, $null)
        $gameObjectXml.InsertBefore($decl, $gameObjectXml.DocumentElement) | Out-Null
        
        # New Node
        [System.XML.XMLElement]$gameObjectRoot=$gameObjectXml.CreateElement("comments") 
        # Append as child to an existing node
        $gameObjectXml.appendChild($gameObjectRoot) | Out-Null
        
        $count = 0
        
        #populating comment node
        foreach ($GameComment in $GameComments) 
        {
            $count = $count + 1
            [System.XML.XMLElement]$comment=$gameObjectRoot.appendChild($gameObjectXml.CreateElement("comment")) 
            $comment.SetAttribute("num",$count)
            
            foreach ($key in $GameComment.Keys) 
            {    
                [System.XML.XMLElement]$paramElement = $gameObjectXml.CreateElement('param') 
                [System.XML.XMLElement]$nameElement = $gameObjectXml.CreateElement('name') 
                [System.XML.XMLElement]$valueElement = $gameObjectXml.CreateElement('value') 
                
                if($GameComment[$key] -ne $Null)
                {
                    $nameElementTextNode = $gameObjectXml.CreateTextNode($key)
                    if($($GameComment[$key]).GetType().FullName -eq 'System.String')
                    {
                        $valueElementTextNode = $gameObjectXml.CreateTextNode(($GameComment[$key]).Trim())
                    }
                    else
                    {
                        $valueElementTextNode = $gameObjectXml.CreateTextNode($GameComment[$key])
                    }
                    
                    $nameElement.appendChild($nameElementTextNode)  | Out-Null
                    $valueElement.appendChild($valueElementTextNode)  | Out-Null
                    
                    $paramElement.appendChild($nameElement)  | Out-Null
                    $paramElement.appendChild($valueElement)  | Out-Null
                    
                    $comment.appendChild($paramElement)  | Out-Null
                }
            }
            $gameObjectRoot.appendChild($comment) | Out-Null
		}
        
        #Write-Output $gameObjectXml.InnerXml > E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\comments.xml
        
        $script:xmlData = $gameObjectXml
	}
	catch [Exception]
	{
		Write-Host("An Error occured while generating game details object xml ")  
        Write-Error "Error : $($_.Exception.Message)" 
	}
}

function Get-RegexMatches
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$inputString,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$regexPattern
)
	[System.Array]$matches=$null
	$found = $inputString -Match $regexPattern
	if($found -eq $True)
	{
		$matches
	}
	Return $matches
}

function Save-PageComments
{
    [CmdletBinding()]
	param(
		[Parameter(Position=0, Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		$Game
	)
	try 
    {
        $script:sqlCommand.CommandText = "[dbo].[spGameCommentsImport]"
        $script:sqlCommand.CommandType = [System.Data.CommandType]::StoredProcedure
        
        $script:sqlCommand.Parameters.Clear()
        
        $value = $script:xmlData.OuterXml
        $value = $value.Replace("'","''")
        Out-File -filepath E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\comments.xml -inputobject $value -encoding Unicode
        
        $script:sqlCommand.Parameters.Add("@gameId",[system.data.SqlDbType]::BIGINT) | Out-Null
        $script:sqlCommand.Parameters['@gameId'].Direction = [system.data.ParameterDirection]::Input
        $script:sqlCommand.Parameters['@gameId'].value = $Game.Id
        
        $script:sqlCommand.Parameters.Add("@inputxml",[system.data.SqlDbType]::XML) | Out-Null
        $script:sqlCommand.Parameters['@inputxml'].Direction = [system.data.ParameterDirection]::Input
        $script:sqlCommand.Parameters['@inputxml'].value = $value
         
        $rdr = $script:sqlCommand.ExecuteReader()
        While($rdr.Read()){
            $error = $script:dbErrorTemplate.PSObject.Copy()
        
            $error.ErrorNumber = $rdr['ErrorNumber']
            $error.ErrorSeverity = $rdr['ErrorSeverity']
            $error.ErrorState = $rdr['ErrorState']
            $error.ErrorProcedure = $rdr['ErrorProcedure']
            $error.ErrorLine = $rdr['ErrorLine']
            $error.ErrorMessage = $rdr['ErrorMessage']
            $error.Message = $rdr['Message']
            
            if($Script:DBNull.Equals($error.ErrorMessage))
            {
                #inserted properly
                $pageNum = $script:gameCommentsPagesInfo[$Game.Id]
                Write-Output $pageNum
                $pageNum | Out-File -append -filepath E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\commentsFetched.txt
            }
            else
            {
                #error exists
                Write-Output "$($error.ErrorMessage) $($Game.Code)"
                $($Game.Code + $pageNum) | Out-File -append -filepath E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\Utilities\GooglePlayGames\commentsNotFetched.txt
            }
        }
        
        $rdr.Close()
	}
	catch [Exception]
	{
		Write-Host("An Error occured while saving game info details")  
        Write-Error "Error : $($_.Exception.Message)" 
        #Write-Error "Error : $($_.Exception.StackTrace)"
	}
}

Main