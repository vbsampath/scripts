#----------------------------------------------------------------------------
#Variables
#----------------------------------------------------------------------------

[string]$userHomeDir=$env:userprofile
[xml]$global:rssFeed=$null
[xml]$global:xmlDoc=$null
[System.XML.XMLElement]$global:xmlRoot=$null
[bool]$global:filterBetas=$false

[string]$global:fileName="updates.xml"
[string]$global:tempSortedFileName="updates_sorted_unique.xml"
[string]$global:tempFileName="updates_temp.xml"

[string]$global:dirName=Join-Path $userHomeDir "Downloads\Firefox Downloads\FilehippoSoftwares"
[string]$global:storageDir=Join-Path $userHomeDir "\Downloads\Firefox Downloads\FilehippoSoftwares\Updates\"

[string[]]$global:softwaresAvailable = $()
[System.Array]$global:sortedXmlArray=$null
[System.Array]$global:programNamesTitles=$null
[System.Array]$global:installedProgramNamesTitles=$null
[System.Array]$global:upgradeMatches= $null
$global:softwares=@{}
$global:installedSoftwareDetails=@{}
$global:softwareUpdateDetails=@{}
$global:consolidatedDetails=@{}
[string]$global:userAgent="Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2;)"
[System.Array]$global:registryPropertiesToExtract = "DisplayVersion","UninstallString"
[bool]$isDryRun=$False


#----------------------------------------------------------------------------
#Functions
#----------------------------------------------------------------------------

function Main
{
	if($isDryRun -eq $True)
	{
		$file = Join-Path $global:dirName  $global:fileName
		$tempPath = Join-Path $global:dirName "temp.txt"

		#checking if updates_temp.xml file exists
		if((Test-Path -path $file) -eq $False)
		{
			Copy-Item $tempPath $file 
		}
	}
		
	CheckFileExists
	
	CreateTempXML
	
	LoadTempUpdatesXmlFile
	
	FilterTempUpdatesXmlFile
	
	LoadSortedUpdatesFile
	
	if($global:filterBetas)
	{
		FilterBetas
	}
	
	GetInstalledSoftwaresInstalledOnMachine
	
	ApplyPreProcesses
	
	DownloadPotentialUpgrades
	
	RemoveUnnecessaryFiles
}

#Usage
function Get-Help
{
	Write-Host "Usage:`n"
	Write-Host 'Additional Information : 
	This script downloads software updates from filehippo.com'
}

#Create temporary xml to process
function CreateTempXML
{
	$file = Join-Path $global:dirName  $global:fileName
	$tempPath = Join-Path $global:dirName $global:tempFileName
	
	#checking if updates_temp.xml file exists
	if((Test-Path -path $tempPath) -eq $False)
	{
		Copy-Item $file $tempPath
	}
}

#Check if filehippo software updates xml is existing
function CheckFileExists
{
	$path = Join-Path $global:dirName $global:fileName 
	
	#checking if updates.xml file exists or else throw error
	if((Test-Path -path $path) -eq $False)
	{
		$errorMessage = "Error : Updates.xml file doesn't exists at location "+ $path
		Write-Error $errorMessage
	}
}

#Load FileHippo Software updates XML file
function LoadTempUpdatesXmlFile
{
	$path = Join-Path $global:dirName $global:tempFileName 
	
	#create XML document to hold the next set of updates
	[System.XML.XMLDocument]$global:xmlDoc=New-Object System.XML.XMLDocument
	
	# load it into an XML object:
	$global:xmlDoc.Load($path)
}

#Filter redundant entries in updates_temp.xml file
function FilterTempUpdatesXmlFile
{
	$path = Join-Path $global:dirName $global:tempSortedFileName 
	
	#sort the xml and remove duplicates
	$global:xmlDoc.softwares.software | Sort-Object -Property key , pubdate -Unique | Export-CliXML $path
}

#Load up sorted unique software updates from tempSorted xml file
function LoadSortedUpdatesFile
{
	$path = Join-Path $global:dirName $global:tempSortedFileName 
	$global:sortedXmlArray = Import-Clixml $path
}

#Extract registry key properties which we get from $key.GetSubKeyNames() method
function Extract-RegistryKeyProperties
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [Microsoft.Win32.RegistryKey]$key,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [System.Array]$propertiesToExtract
)
	$valueNameDetails=@{}
	foreach($valueName in $key.GetValueNames())
	{
		if($propertiesToExtract -Contains $valueName)
		{
			$valueNameDetails.Add($valueName, $key.GetValue($valueName).ToString())
		}
	}
	
	Return $valueNameDetails
}

#Get softwares installed on the machine
function GetInstalledSoftwaresInstalledOnMachine
{
	#Define the variable to hold the location of Currently Installed Programs

	$UninstallKey="SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall" 

	#Create an instance of the Registry Object and open the HKLM base key

	$reg=[microsoft.win32.registrykey]::OpenRemoteBaseKey('LocalMachine',$env:computername)

	#Drill down into the Uninstall key using the OpenSubKey Method

	$regkey=$reg.OpenSubKey($UninstallKey) 

	#Retrieve an array of string that contain all the subkey names

	$subkeys=$regkey.GetSubKeyNames() 
	
	#Open each Subkey and use GetValue Method to return the required values for each
	foreach($key in $subkeys){
		$valueNamesHash = @{}
		$thisKey=$UninstallKey+"\\"+$key 
		
		$thisSubKey=$reg.OpenSubKey($thisKey) 
		
		$ProgramName =  $thisSubKey.getValue("DisplayName")
		
		if($ProgramName -ne $null)
		{
			if($global:softwares.ContainsKey($ProgramName) -ne $True)
			{
				$valueNamesHash = Extract-RegistryKeyProperties -key $thisSubKey -propertiesToExtract $global:registryPropertiesToExtract
				$global:softwares.Add($ProgramName, $valueNamesHash)
			}
		}
	}
}

#Process file names into seperate array to match exactly with softwares finded in target machine
#seperate out versions and titles and may be betas 
function ApplyPreProcesses
{
	for ($i=0;$i -lt $global:sortedXmlArray.Length; $i++) 
	{
		$details=@{}
		$tokens = $global:sortedXmlArray[$i].title -Split " "
		$details.Add("guid", $global:sortedXmlArray[$i].guid)
		$details.Add("key", $global:sortedXmlArray[$i].key)
		$details.Add("pubdate", $global:sortedXmlArray[$i].pubdate)
		$details.Add("img", $global:sortedXmlArray[$i].img)
		$details.Add("title", $global:sortedXmlArray[$i].title)
		$details.Add("name", $global:sortedXmlArray[$i].name)
		$global:softwareUpdateDetails.Add($global:sortedXmlArray[$i].title, $details)
		$global:programNamesTitles += $global:sortedXmlArray[$i].name
	}
	
	
	foreach($installedProgram in $global:softwares.Keys)
	{
		foreach($update in $global:programNamesTitles)
		{
			if(($update.Contains($installedProgram) -or $installedProgram.Contains($update)) -and ($installedProgram -ne ''))
			{
				$global:upgradeMatches += $update
				$global:installedSoftwareDetails.Add($installedProgram, $global:softwares.Get_Item($installedProgram))
			}
		}
	}
	
	#Write-Output $global:installedSoftwareDetails
	#Write-Output $global:softwareUpdateDetails
	
	
	#create a hash table such that all information is consolidated
	foreach($detail in $global:installedSoftwareDetails.Keys)
	{	
		$regex = "\d+\.\d+|\d+"
		$splittedTitle = [regex]::Split($detail, $regex)
		$programNameConsolidated = $splittedTitle[0].Trim()
		#$programNameConsolidated + "detail" | Out-String
		
		foreach($updateDetails in $global:softwareUpdateDetails.Keys)
		{
			$splittedTitle1 = [regex]::Split($updateDetails, $regex)
			$updateConsolidated = $splittedTitle1[0].Trim()
			
			if(($programNameConsolidated.Contains($updateConsolidated)) -or ($updateConsolidated.Contains($programNameConsolidated)))
			{
				$consolidatedHash=@{}
				$consolidatedHash.Add("guid",$global:softwareUpdateDetails.Get_Item($updateDetails).guid)
				$consolidatedHash.Add("key",$global:softwareUpdateDetails.Get_Item($updateDetails).key)
				$consolidatedHash.Add("title",$global:softwareUpdateDetails.Get_Item($updateDetails).title)
				$consolidatedHash.Add("img",$global:softwareUpdateDetails.Get_Item($updateDetails).img)
				$consolidatedHash.Add("pubdate",$global:softwareUpdateDetails.Get_Item($updateDetails).pubdate)
				$consolidatedHash.Add("displayversion",$global:installedSoftwareDetails.Get_Item($detail).DisplayVersion)
				$consolidatedHash.Add("uninstallstring",$global:installedSoftwareDetails.Get_Item($detail).UninstallString)
				$consolidatedHash.Add("installed",$detail)
				$global:consolidatedDetails.Add($programNameConsolidated, $consolidatedHash)
				break
			}
		}
	}
	
	#Write-Output $global:consolidatedDetails
	
}

Function Remove-InvalidFileNameChars { 
    <# 
    .SYNOPSIS 
    This is a PowerShell function to remove invalid characters from strings to be used as file names. 
 
    .DESCRIPTION 
    The function takes a string parameter called Name and returns a string that has been stripped of invalid file name characters, i.e. *, :, \, /.  The Name parameter will also receive input from the pipeline. 
 
    .PARAMETER Name 
    Specifies the file name to strip of invalid characters. 
 
    .INPUTS 
    Parameter Name accepts System.String objects from the pipeline. 
 
    .OUTPUTS 
    System.String.  Outpus a string object 
 
    .EXAMPLE 
    Remove-InvalidFileNameChars -Name "<This/name\is*an:illegal?filename>" 
    PS C:\>Thisnameisanillegalfilename 
 
    .NOTES 
    It would be easiest to copy the function from the script file and place it in your profile.  However, you may also dot-source the script to load the function into PowerShell: 
    i.e. PS C:\>. .\Remove-InvalidFileNameChars.ps1 
    #> 
 
    [CmdletBinding()] 
 
    param(
		[Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)] [String]$Name,
		[Parameter(Mandatory=$true, Position=1, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)] [String]$ReplaceWith 
    ) 
 
    return [RegEx]::Replace($Name, "[{0}]" -f ([RegEx]::Escape([String][System.IO.Path]::GetInvalidFileNameChars())), $ReplaceWith) 
}

#Download softwares to preset location from the updates available
function DownloadString
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$url
)
	[string]$reply=$null
	$webclient = New-Object System.Net.WebClient
	$webclient.Headers.Add("user-agent", $global:userAgent)
	$reply = $webclient.DownloadString($url)
	Return $reply
}

#Download files
function DownloadFile  
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$directory,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$file,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$url
)  
	$webclient = New-Object System.Net.WebClient  
	$webclient.Headers.Add("user-agent", $global:userAgent)
	$file = $file + ".exe"
	$path = Join-Path $directory $file
	
	try  
	{  
		Write-Output "Downloading $file"
		if($isDryRun -eq $False)
		{
			$webclient.DownloadFileAsync($url, $path)  
		}
	}  
	catch [System.Net.WebException]  
	{  
		Write-Host("Cannot download $url")  
		Write-Error "Error : $($_.Exception.Message)" 
	}   
	finally  
	{    
		$webclient.Dispose()  
	}  
}  

#Return matches after matching a string with regex pattern
function Get-RegexMatches
{param(
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$inputString,
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string]$regexPattern
)
	[System.Array]$matches=$null
	$found = $inputString -Match $regexPattern
	if($found -eq $True)
	{
		$matches
	}
	Return $matches
}

#Download softwares to preset location from the updates available
function DownloadPotentialUpgrades
{
	foreach($updateSoftware in $global:consolidatedDetails.Keys)
	{
		$updateObj = $global:consolidatedDetails.Get_Item($updateSoftware)
		$validFileName = Remove-InvalidFileNameChars -Name $updateObj.title -ReplaceWith " "
		
		#"/download_firefox/download/9e2058a79da99fdc10c946165860cdca/
		$htmlOutput = DownloadString -url $updateObj.guid
		$regexPattern = 'download_' + $updateObj.key +'\/download\/\w+'
		$matches = Get-RegexMatches -inputString $htmlOutput -regexPattern $regexPattern
		if($matches -ne $null)
		{
			$downloadString = $matches[0].Get_Item(0)
			$finalString = "http://filehippo.com/" + $downloadString
			$htmlOutput1 = DownloadString -url $finalString
			
			#/download/file/42039b2987844e3b10ba7526a064c4d96b149adfbd72c8cce17e844269f05488/
			$regexPattern1 = 'download\/file\/\w+'
			$matches = Get-RegexMatches -inputString $htmlOutput1 -regexPattern $regexPattern1
			if($matches -ne $null)
			{
				$downloadString1 = $matches[0].Get_Item(0)
				$finalString1 = "http://filehippo.com/" + $downloadString1
				DownloadFile -directory $global:storageDir -file $validFileName -url $finalString1
			}
		}
	}
}

#Removes unnecessary files created by this script
function RemoveUnnecessaryFiles
{
	Remove-Item (Join-Path $global:dirName $global:fileName)
	Remove-Item (Join-Path $global:dirName $global:tempSortedFileName)
	Remove-Item (Join-Path $global:dirName $global:tempFileName)
}


Try
{	
	Main
}
Catch
{
	#Write-Error "Error : $($_.Exception.Message)" 
	Write-Host "Error : $($_.Exception.Message)"  -ForegroundColor red
	Get-Help
}





<#
function GenerateForm {
[void][reflection.assembly]::loadwithpartialname("System.Windows.Forms") 

$form1 = New-Object System.Windows.Forms.Form
$form1.Text = "FileHippo Software Updates"
$form1.StartPosition = 4
$form1.ClientSize = "550,450"
$form1.ShowDialog()
}
#>
<#
Add-Type -Language CSharp@"
public class Update{
	public string guid;
	public string img;
	public string key;
	public System.DateTime pubdate;
	public string title;
}
public class Updates{
	public Update[] updates;
}
"@
#>