/****** Object:  StoredProcedure [dbo].[spTblDisplayColumnsRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDisplayColumnsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblDisplayColumnsRead](
    		@DisplayColumnId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@DisplayColumnId IS NULL)
    		SELECT * FROM [dbo].[TblDisplayColumns] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblDisplayColumns] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND DisplayColumnId = @DisplayColumnId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblEditableObjectsRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblEditableObjectsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblEditableObjectsRead](
    		@EditableObjectId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@EditableObjectId IS NULL)
    		SELECT * FROM [dbo].[TblEditableObjects] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblEditableObjects] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND EditableObjectId = @EditableObjectId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartInventoryRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartInventoryRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblPartInventoryRead](
    		@PartInventoryId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@PartInventoryId IS NULL)
    		SELECT * FROM [dbo].[TblPartInventory] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblPartInventory] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND PartInventoryId = @PartInventoryId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartsRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblPartsRead](
    		@PartId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@PartId IS NULL)
    		SELECT * FROM [dbo].[TblParts] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblParts] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND PartId = @PartId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartStatusTypesRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartStatusTypesRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblPartStatusTypesRead](
    		@PartStatusTypeId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@PartStatusTypeId IS NULL)
    		SELECT * FROM [dbo].[TblPartStatusTypes] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblPartStatusTypes] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND PartStatusTypeId = @PartStatusTypeId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartsUsageRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartsUsageRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblPartsUsageRead](
    		@PartsUsageId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@PartsUsageId IS NULL)
    		SELECT * FROM [dbo].[TblPartsUsage] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblPartsUsage] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND PartsUsageId = @PartsUsageId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartTransactionsRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartTransactionsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblPartTransactionsRead](
    		@PartTransactionId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@PartTransactionId IS NULL)
    		SELECT * FROM [dbo].[TblPartTransactions] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblPartTransactions] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND PartTransactionId = @PartTransactionId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartTransactionTypesRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartTransactionTypesRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblPartTransactionTypesRead](
    		@PartTransactionTypeId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@PartTransactionTypeId IS NULL)
    		SELECT * FROM [dbo].[TblPartTransactionTypes] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblPartTransactionTypes] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND PartTransactionTypeId = @PartTransactionT
ypeId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartTypesRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartTypesRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblPartTypesRead](
    		@PartTypeId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@PartTypeId IS NULL)
    		SELECT * FROM [dbo].[TblPartTypes] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblPartTypes] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND PartTypeId = @PartTypeId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSavedSearchRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSavedSearchRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblSavedSearchRead](
    		@SavedSearchId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@SavedSearchId IS NULL)
    		SELECT * FROM [dbo].[TblSavedSearch] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblSavedSearch] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND [Saved Search Id] = @SavedSearchId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSearchColumnsRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchColumnsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblSearchColumnsRead](
    		@SearchColumnId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@SearchColumnId IS NULL)
    		SELECT * FROM [dbo].[TblSearchColumns] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblSearchColumns] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND SearchColumnId = @SearchColumnId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSearchObjectsRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchObjectsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblSearchObjectsRead](
    		@SearchObjectId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@SearchObjectId IS NULL)
    		SELECT * FROM [dbo].[TblSearchObjects] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblSearchObjects] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND SearchObjectId = @SearchObjectId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblStaticObjectsRead]    Script Date: 2012-03-12 12:43:26******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblStaticObjectsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblStaticObjectsRead](
    		@StaticObjectId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@StaticObjectId IS NULL)
    		SELECT * FROM [dbo].[TblStaticObjects] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblStaticObjects] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND StaticObjectId = @StaticObjectId;
            
    END' 
END
GO

