/****** Object:  StoredProcedure [dbo].[spTblSavedSearchInsert]    Script Date: 02/20/2012 01:55:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSavedSearchInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSavedSearchInsert](
		@SearchTitle VARCHAR(50),
		@SearchObject VARCHAR(50),
		@SearchColumn VARCHAR(50),
		@SearchVariable VARCHAR(50),
		@SearchDescription VARCHAR(50),
		@SearchDateTime DATETIME,
		@SearchQuery VARCHAR(250),
		@InsertedDate DATETIME,
		@ModifiedDate DATETIME,
		@Inserted VARCHAR(20),
		@Modified VARCHAR(20),
		@Deleted BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO [dbo].[TblSavedSearch] 
	(
		[Search Title],
		[Search Object],
		[Search Column],
		[Search Variable],
		[Search Description],
		[Search DateTime],
		[Search Query],
		InsertedDate, 
		ModifiedDate, 
		Inserted, 
		Modified, 
		Deleted
	)
	VALUES 
	(
		@SearchTitle,
		@SearchObject,
		@SearchColumn,
		@SearchVariable,
		@SearchDescription, 
		@SearchDateTime,
		@SearchQuery, 
		@InsertedDate, 
		@ModifiedDate, 
		@Inserted, 
		@Modified, 
		@Deleted
	);

END' 
END
GO
