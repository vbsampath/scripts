


 --READ STORED PROCEDURE 



/****** Object:  StoredProcedure [dbo].[spTblAppInfoRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAppInfoRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblAppInfoRead](
    		@AppInfoId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@AppInfoId IS NULL)
    		SELECT * FROM [dbo].[TblAppInfo] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblAppInfo] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND AppInfoId = @AppInfoId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblAuditInformationRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAuditInformationRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblAuditInformationRead](
    		@AuditInformationId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@AuditInformationId IS NULL)
    		SELECT * FROM [dbo].[TblAuditInformation] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblAuditInformation] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND AuditInformationId = @AuditInformationId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblBanksRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblBanksRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblBanksRead](
    		@BankId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@BankId IS NULL)
    		SELECT * FROM [dbo].[TblBanks] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblBanks] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND BankId = @BankId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblBillValidatorsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblBillValidatorsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblBillValidatorsRead](
    		@BillValidatorId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@BillValidatorId IS NULL)
    		SELECT * FROM [dbo].[TblBillValidators] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblBillValidators] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND BillValidatorId = @BillValidatorId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblButtonPanelsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblButtonPanelsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblButtonPanelsRead](
    		@ButtonPanelId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@ButtonPanelId IS NULL)
    		SELECT * FROM [dbo].[TblButtonPanels] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblButtonPanels] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND ButtonPanelId = @ButtonPanelId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblChassisRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblChassisRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblChassisRead](
    		@ChassisId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@ChassisId IS NULL)
    		SELECT * FROM [dbo].[TblChassis] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblChassis] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND ChassisId = @ChassisId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblCPUsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblCPUsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblCPUsRead](
    		@CPUId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@CPUId IS NULL)
    		SELECT * FROM [dbo].[TblCPUs] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblCPUs] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND CPUId = @CPUId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblDenominationsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDenominationsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblDenominationsRead](
    		@DenominationId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@DenominationId IS NULL)
    		SELECT * FROM [dbo].[TblDenominations] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblDenominations] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND DenominationId = @DenominationId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblDisplayColumnsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDisplayColumnsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblDisplayColumnsRead](
    		@DisplayColumnId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@DisplayColumnId IS NULL)
    		SELECT * FROM [dbo].[TblDisplayColumns] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblDisplayColumns] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND [Display Column Id] = @DisplayColumnId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblEditableObjectsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblEditableObjectsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblEditableObjectsRead](
    		@EditableObjectId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@EditableObjectId IS NULL)
    		SELECT * FROM [dbo].[TblEditableObjects] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblEditableObjects] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND EditableObjectId = @EditableObjectId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblGamingControllersRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblGamingControllersRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblGamingControllersRead](
    		@GamingControllerId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@GamingControllerId IS NULL)
    		SELECT * FROM [dbo].[TblGamingControllers] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblGamingControllers] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND GamingControllerId = @GamingControllerId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblHistoryObjectsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblHistoryObjectsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblHistoryObjectsRead](
    		@HistoryObjectId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@HistoryObjectId IS NULL)
    		SELECT * FROM [dbo].[TblHistoryObjects] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblHistoryObjects] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND [History Object Id] = @HistoryObjectId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentAuditInformationRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentAuditInformationRead]') AND type in (N'P', N'PC
'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblIncidentAuditInformationRead](
    		@IncidentAuditInformationId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@IncidentAuditInformationId IS NULL)
    		SELECT * FROM [dbo].[TblIncidentAuditInformation] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblIncidentAuditInformation] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND IncidentAuditInformationId = @Inci
dentAuditInformationId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentForRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentForRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblIncidentForRead](
    		@IncidentForId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@IncidentForId IS NULL)
    		SELECT * FROM [dbo].[TblIncidentFor] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblIncidentFor] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND IncidentForId = @IncidentForId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblIncidentsRead](
    		@IncidentId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@IncidentId IS NULL)
    		SELECT * FROM [dbo].[TblIncidents] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblIncidents] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND IncidentId = @IncidentId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentStatusRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentStatusRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblIncidentStatusRead](
    		@IncidentStatusId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@IncidentStatusId IS NULL)
    		SELECT * FROM [dbo].[TblIncidentStatus] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblIncidentStatus] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND IncidentStatusId = @IncidentStatusId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentTypesRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentTypesRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblIncidentTypesRead](
    		@IncidentTypeId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@IncidentTypeId IS NULL)
    		SELECT * FROM [dbo].[TblIncidentTypes] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblIncidentTypes] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND IncidentTypeId = @IncidentTypeId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblLocationsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblLocationsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblLocationsRead](
    		@LocationId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@LocationId IS NULL)
    		SELECT * FROM [dbo].[TblLocations] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblLocations] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND LocationId = @LocationId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachineOperationsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineOperationsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblMachineOperationsRead](
    		@MachineOperationsId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@MachineOperationsId IS NULL)
    		SELECT * FROM [dbo].[TblMachineOperations] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblMachineOperations] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND MachineOperationsId = @MachineOperationsI
d;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachinesRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachinesRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblMachinesRead](
    		@MachineId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@MachineId IS NULL)
    		SELECT * FROM [dbo].[TblMachines] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblMachines] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND MachineId = @MachineId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachineSnapshotRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineSnapshotRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblMachineSnapshotRead](
    		@MachineSnapshotId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@MachineSnapshotId IS NULL)
    		SELECT * FROM [dbo].[TblMachineSnapshot] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblMachineSnapshot] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND MachineSnapshotId = @MachineSnapshotId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachineStatesRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineStatesRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblMachineStatesRead](
    		@MachineStateId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@MachineStateId IS NULL)
    		SELECT * FROM [dbo].[TblMachineStates] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblMachineStates] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND MachineStateId = @MachineStateId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachineTypesRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineTypesRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblMachineTypesRead](
    		@MachineTypeId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@MachineTypeId IS NULL)
    		SELECT * FROM [dbo].[TblMachineTypes] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblMachineTypes] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND MachineTypeId = @MachineTypeId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblModificationTypeRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblModificationTypeRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblModificationTypeRead](
    		@ModificationCategoryId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@ModificationCategoryId IS NULL)
    		SELECT * FROM [dbo].[TblModificationType] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblModificationType] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND ModificationCategoryId = @ModificationCate
goryId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblNOCHistoryRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblNOCHistoryRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblNOCHistoryRead](
    		@NOCHistoryId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@NOCHistoryId IS NULL)
    		SELECT * FROM [dbo].[TblNOCHistory] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblNOCHistory] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND NOCHistoryId = @NOCHistoryId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblPartsRead](
    		@PartId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@PartId IS NULL)
    		SELECT * FROM [dbo].[TblParts] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblParts] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND PartId = @PartId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblReportColumnsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportColumnsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblReportColumnsRead](
    		@ReportColumnId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@ReportColumnId IS NULL)
    		SELECT * FROM [dbo].[TblReportColumns] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblReportColumns] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND [Report Column Id] = @ReportColumnId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblReportObjectsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportObjectsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblReportObjectsRead](
    		@ReportObjectId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@ReportObjectId IS NULL)
    		SELECT * FROM [dbo].[TblReportObjects] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblReportObjects] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND [Report Object Id] = @ReportObjectId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[sptblSavedSearchRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sptblSavedSearchRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[sptblSavedSearchRead](
    		@SavedSearchId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@SavedSearchId IS NULL)
    		SELECT * FROM [dbo].[tblSavedSearch] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[tblSavedSearch] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND [Saved Search Id] = @SavedSearchId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSearchColumnsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchColumnsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblSearchColumnsRead](
    		@SearchColumnId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@SearchColumnId IS NULL)
    		SELECT * FROM [dbo].[TblSearchColumns] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblSearchColumns] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND [Search Column Id] = @SearchColumnId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSearchObjectsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchObjectsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblSearchObjectsRead](
    		@SearchObjectId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@SearchObjectId IS NULL)
    		SELECT * FROM [dbo].[TblSearchObjects] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblSearchObjects] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND [Search Object Id] = @SearchObjectId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblStaticObjectsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblStaticObjectsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblStaticObjectsRead](
    		@StaticObjectId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@StaticObjectId IS NULL)
    		SELECT * FROM [dbo].[TblStaticObjects] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblStaticObjects] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND [Static Object Id] = @StaticObjectId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblThemesRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblThemesRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblThemesRead](
    		@ThemeId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@ThemeId IS NULL)
    		SELECT * FROM [dbo].[TblThemes] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblThemes] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND ThemeId = @ThemeId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblTicketPrintersRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblTicketPrintersRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblTicketPrintersRead](
    		@TicketPrinterId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@TicketPrinterId IS NULL)
    		SELECT * FROM [dbo].[TblTicketPrinters] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblTicketPrinters] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND TicketPrinterId = @TicketPrinterId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblTopBoxesRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblTopBoxesRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblTopBoxesRead](
    		@TopBoxId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@TopBoxId IS NULL)
    		SELECT * FROM [dbo].[TblTopBoxes] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblTopBoxes] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND TopBoxId = @TopBoxId;
            
    END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblVendorsRead]    Script Date: 2012-03-14 19:28:41******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblVendorsRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE [dbo].[spTblVendorsRead](
    		@VendorId BIGINT = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@VendorId IS NULL)
    		SELECT * FROM [dbo].[TblVendors] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[TblVendors] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND VendorId = @VendorId;
            
    END' 
END
GO





 --DELETE STORED PROCEDURE



/****** Object:  StoredProcedure [dbo].[spTblAppInfoDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAppInfoDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblAppInfoDelete](
			@AppInfoId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblAppInfo] SET Deleted = 1 WHERE AppInfoId  = @AppInfoId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblAuditInformationDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAuditInformationDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblAuditInformationDelete](
			@AuditInformationId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblAuditInformation] SET Deleted = 1 WHERE AuditInformationId  = @AuditInformationId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblBanksDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblBanksDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblBanksDelete](
			@BankId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblBanks] SET Deleted = 1 WHERE BankId  = @BankId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblBillValidatorsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblBillValidatorsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblBillValidatorsDelete](
			@BillValidatorId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblBillValidators] SET Deleted = 1 WHERE BillValidatorId  = @BillValidatorId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblButtonPanelsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblButtonPanelsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblButtonPanelsDelete](
			@ButtonPanelId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblButtonPanels] SET Deleted = 1 WHERE ButtonPanelId  = @ButtonPanelId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblChassisDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblChassisDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblChassisDelete](
			@ChassisId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblChassis] SET Deleted = 1 WHERE ChassisId  = @ChassisId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblCPUsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblCPUsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblCPUsDelete](
			@CPUId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblCPUs] SET Deleted = 1 WHERE CPUId  = @CPUId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblDenominationsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDenominationsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblDenominationsDelete](
			@DenominationId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblDenominations] SET Deleted = 1 WHERE DenominationId  = @DenominationId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblDisplayColumnsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDisplayColumnsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblDisplayColumnsDelete](
			@DisplayColumnId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblDisplayColumns] SET Deleted = 1 WHERE [Display Column Id]  = @DisplayColumnId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblEditableObjectsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblEditableObjectsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblEditableObjectsDelete](
			@EditableObjectId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblEditableObjects] SET Deleted = 1 WHERE EditableObjectId  = @EditableObjectId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblGamingControllersDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblGamingControllersDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblGamingControllersDelete](
			@GamingControllerId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblGamingControllers] SET Deleted = 1 WHERE GamingControllerId  = @GamingControllerId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblHistoryObjectsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblHistoryObjectsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblHistoryObjectsDelete](
			@HistoryObjectId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblHistoryObjects] SET Deleted = 1 WHERE [History Object Id]  = @HistoryObjectId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentAuditInformationDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentAuditInformationDelete]') AND TYPE IN (N'P', N'
PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblIncidentAuditInformationDelete](
			@IncidentAuditInformationId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblIncidentAuditInformation] SET Deleted = 1 WHERE IncidentAuditInformationId  = @IncidentAuditInformationId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentForDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentForDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblIncidentForDelete](
			@IncidentForId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblIncidentFor] SET Deleted = 1 WHERE IncidentForId  = @IncidentForId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblIncidentsDelete](
			@IncidentId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblIncidents] SET Deleted = 1 WHERE IncidentId  = @IncidentId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentStatusDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentStatusDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblIncidentStatusDelete](
			@IncidentStatusId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblIncidentStatus] SET Deleted = 1 WHERE IncidentStatusId  = @IncidentStatusId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblIncidentTypesDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentTypesDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblIncidentTypesDelete](
			@IncidentTypeId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblIncidentTypes] SET Deleted = 1 WHERE IncidentTypeId  = @IncidentTypeId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblLocationsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblLocationsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblLocationsDelete](
			@LocationId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblLocations] SET Deleted = 1 WHERE LocationId  = @LocationId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachineOperationsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineOperationsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblMachineOperationsDelete](
			@MachineOperationsId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblMachineOperations] SET Deleted = 1 WHERE MachineOperationsId  = @MachineOperationsId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachinesDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachinesDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblMachinesDelete](
			@MachineId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblMachines] SET Deleted = 1 WHERE MachineId  = @MachineId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachineSnapshotDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineSnapshotDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblMachineSnapshotDelete](
			@MachineSnapshotId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblMachineSnapshot] SET Deleted = 1 WHERE MachineSnapshotId  = @MachineSnapshotId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachineStatesDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineStatesDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblMachineStatesDelete](
			@MachineStateId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblMachineStates] SET Deleted = 1 WHERE MachineStateId  = @MachineStateId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblMachineTypesDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineTypesDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblMachineTypesDelete](
			@MachineTypeId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblMachineTypes] SET Deleted = 1 WHERE MachineTypeId  = @MachineTypeId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblModificationTypeDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblModificationTypeDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblModificationTypeDelete](
			@ModificationCategoryId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblModificationType] SET Deleted = 1 WHERE ModificationCategoryId  = @ModificationCategoryId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblNOCHistoryDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblNOCHistoryDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblNOCHistoryDelete](
			@NOCHistoryId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblNOCHistory] SET Deleted = 1 WHERE NOCHistoryId  = @NOCHistoryId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblPartsDelete](
			@PartId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblParts] SET Deleted = 1 WHERE PartId  = @PartId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblReportColumnsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportColumnsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblReportColumnsDelete](
			@ReportColumnId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblReportColumns] SET Deleted = 1 WHERE [Report Column Id]  = @ReportColumnId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblReportObjectsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportObjectsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblReportObjectsDelete](
			@ReportObjectId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblReportObjects] SET Deleted = 1 WHERE [Report Object Id]  = @ReportObjectId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[sptblSavedSearchDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sptblSavedSearchDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[sptblSavedSearchDelete](
			@SavedSearchId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[tblSavedSearch] SET Deleted = 1 WHERE [Saved Search Id]  = @SavedSearchId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSearchColumnsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchColumnsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblSearchColumnsDelete](
			@SearchColumnId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblSearchColumns] SET Deleted = 1 WHERE [Search Column Id]  = @SearchColumnId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSearchObjectsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchObjectsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblSearchObjectsDelete](
			@SearchObjectId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblSearchObjects] SET Deleted = 1 WHERE [Search Object Id]  = @SearchObjectId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblStaticObjectsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblStaticObjectsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblStaticObjectsDelete](
			@StaticObjectId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblStaticObjects] SET Deleted = 1 WHERE [Static Object Id]  = @StaticObjectId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblThemesDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblThemesDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblThemesDelete](
			@ThemeId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblThemes] SET Deleted = 1 WHERE ThemeId  = @ThemeId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblTicketPrintersDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblTicketPrintersDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblTicketPrintersDelete](
			@TicketPrinterId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblTicketPrinters] SET Deleted = 1 WHERE TicketPrinterId  = @TicketPrinterId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblTopBoxesDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblTopBoxesDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblTopBoxesDelete](
			@TopBoxId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblTopBoxes] SET Deleted = 1 WHERE TopBoxId  = @TopBoxId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblVendorsDelete]    Script Date: 2012-03-14 19:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblVendorsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblVendorsDelete](
			@VendorId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblVendors] SET Deleted = 1 WHERE VendorId  = @VendorId;

	END' 
END
GO





 --INSERT STORED PROCEDURE


/****** Object:  StoredProcedure [dbo].[spTblIncidentsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentsInsert](
		@IncidentType		nvarchar(100),
		@IncidentDetailedDescription		nvarchar(2000),
		@PartId		bigint,
		@PartType		nvarchar(100),
		@BillValidatorId		bigint,
		@BillValidatorType		nvarchar(100),
		@IncidentStatusId		bigint,
		@IncidentStatusType		nvarchar(100),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit,
		@DescriptionHistory		xml,
		@IncidentFor		nvarchar(30),
		@IncidentForId		bigint,
		@SerialNumber		nvarchar(50),
		@SerialNumberId		bigint,
		@MachineId		bigint
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblIncidents 
	(
		[Incident Type],
		[IncidentDetailedDescription],
		[PartId],
		[Part Type],
		[BillValidatorId],
		[Bill Validator Type],
		[IncidentStatusId],
		[Incident Status Type],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted],
		[DescriptionHistory],
		[Incident For],
		[IncidentForId],
		[Serial Number],
		[SerialNumberId],
		[MachineId]
	)
	VALUES 
	(
		@IncidentType,
		@IncidentDetailedDescription,
		@PartId,
		@PartType,
		@BillValidatorId,
		@BillValidatorType,
		@IncidentStatusId,
		@IncidentStatusType,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted,
		@DescriptionHistory,
		@IncidentFor,
		@IncidentForId,
		@SerialNumber,
		@SerialNumberId,
		@MachineId
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblVendorsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblVendorsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblVendorsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@VendorCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblVendors 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Vendor Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@VendorCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblDisplayColumnsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDisplayColumnsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblDisplayColumnsInsert](
		@DisplayGroup		nchar,
		@DisplayColumn		nchar,
		@DisplayColumnTable		nchar,
		@StoredProcedure		nvarchar(50)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblDisplayColumns 
	(
		[Display Group],
		[Display Column],
		[Display Column Table],
		[StoredProcedure]
	)
	VALUES 
	(
		@DisplayGroup,
		@DisplayColumn,
		@DisplayColumnTable,
		@StoredProcedure
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblCPUsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblCPUsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblCPUsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@CPUTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblCPUs 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[CPU Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@CPUTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblIncidentAuditInformationInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentAuditInformationInsert]') AND TYPE IN (N'P', N'
PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentAuditInformationInsert](
		@AfterXml		xml,
		@IncidentId		bigint,
		@MachineId		bigint,
		@IncidentTypeId		bigint,
		@IncidentType		nvarchar(100),
		@IncidentDetailedDescription		nvarchar(2000),
		@PartId		bigint,
		@PartType		nvarchar(250),
		@BillValidatorId		bigint,
		@BillValidatorType		nvarchar(250),
		@IncidentStatusId		bigint,
		@IncidentStatusType		nvarchar(250),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(50),
		@Modified		nvarchar(50),
		@Deleted		bit,
		@SerialNumberId		bigint,
		@SerialNumber		nvarchar(50)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblIncidentAuditInformation 
	(
		[AfterXml],
		[IncidentId],
		[MachineId],
		[IncidentTypeId],
		[Incident Type],
		[IncidentDetailedDescription],
		[PartId],
		[Part Type],
		[BillValidatorId],
		[Bill Validator Type],
		[IncidentStatusId],
		[Incident Status Type],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted],
		[SerialNumberId],
		[Serial Number]
	)
	VALUES 
	(
		@AfterXml,
		@IncidentId,
		@MachineId,
		@IncidentTypeId,
		@IncidentType,
		@IncidentDetailedDescription,
		@PartId,
		@PartType,
		@BillValidatorId,
		@BillValidatorType,
		@IncidentStatusId,
		@IncidentStatusType,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted,
		@SerialNumberId,
		@SerialNumber
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachinesInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachinesInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachinesInsert](
		@AssetNumber		nvarchar(30),
		@Bank		nvarchar(20),
		@HouseNumber		nvarchar(20),
		@Registration		nvarchar(20),
		@IPAddress		nvarchar(20),
		@Port		nvarchar(20),
		@ThemeNameID		nvarchar(30),
		@ThemeName		nvarchar(100),
		@ThemeInstallDate		datetime,
		@Template		nvarchar(30),
		@DenominationID		nvarchar(10),
		@Denomination		nvarchar(10),
		@LocationID		nvarchar(20),
		@Location		nvarchar(20),
		@PlayerTerminalTypeID		nvarchar(20),
		@PlayerTerminalType		nvarchar(20),
		@VendorID		nvarchar(30),
		@VendorName		nvarchar(30),
		@ChassisTypeID		nvarchar(30),
		@ChassisType		nvarchar(30),
		@TicketPrinterTypeID		nvarchar(30),
		@TicketPrinterType		nvarchar(30),
		@ButtonPanelTypeID		nvarchar(30),
		@ButtonPanelType		nvarchar(30),
		@CPUTypeID		nvarchar(30),
		@CPUType		nvarchar(30),
		@TopBoxTypeID		nvarchar(30),
		@TopBoxType		nvarchar(30),
		@MachineTypeID		nvarchar(30),
		@MachineType		nvarchar(30),
		@MachineStateID		nvarchar(30),
		@MachineState		nvarchar(30),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(50),
		@Modified		nvarchar(50),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblMachines 
	(
		[Asset Number],
		[Bank],
		[House Number],
		[Registration],
		[IP Address],
		[Port],
		[ThemeNameID],
		[Theme Name],
		[Theme Install Date],
		[Template],
		[DenominationID],
		[Denomination],
		[LocationID],
		[Location],
		[PlayerTerminalTypeID],
		[Player Terminal Type],
		[VendorID],
		[Vendor Name],
		[ChassisTypeID],
		[Chassis Type],
		[TicketPrinterTypeID],
		[Ticket Printer Type],
		[ButtonPanelTypeID],
		[Button Panel Type],
		[CPUTypeID],
		[CPU Type],
		[TopBoxTypeID],
		[Top Box Type],
		[MachineTypeID],
		[Machine Type],
		[MachineStateID],
		[Machine State],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted]
	)
	VALUES 
	(
		@AssetNumber,
		@Bank,
		@HouseNumber,
		@Registration,
		@IPAddress,
		@Port,
		@ThemeNameID,
		@ThemeName,
		@ThemeInstallDate,
		@Template,
		@DenominationID,
		@Denomination,
		@LocationID,
		@Location,
		@PlayerTerminalTypeID,
		@PlayerTerminalType,
		@VendorID,
		@VendorName,
		@ChassisTypeID,
		@ChassisType,
		@TicketPrinterTypeID,
		@TicketPrinterType,
		@ButtonPanelTypeID,
		@ButtonPanelType,
		@CPUTypeID,
		@CPUType,
		@TopBoxTypeID,
		@TopBoxType,
		@MachineTypeID,
		@MachineType,
		@MachineStateID,
		@MachineState,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblTicketPrintersInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblTicketPrintersInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblTicketPrintersInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@TicketPrinterTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblTicketPrinters 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Ticket Printer Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@TicketPrinterTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblStaticObjectsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblStaticObjectsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblStaticObjectsInsert](
		@StaticObjectType		nvarchar(30),
		@StaticObjectTable		nvarchar(30),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@StaticObjectDisplayField		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblStaticObjects 
	(
		[Static Object Type],
		[Static Object Table],
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Static Object Display Field]
	)
	VALUES 
	(
		@StaticObjectType,
		@StaticObjectTable,
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@StaticObjectDisplayField
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblIncidentForInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentForInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentForInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@IncidentForCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblIncidentFor 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Incident For Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@IncidentForCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachineOperationsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineOperationsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachineOperationsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@MachineOperationsTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblMachineOperations 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Machine Operations Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@MachineOperationsTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblReportObjectsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportObjectsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblReportObjectsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nchar,
		@ModifiedBy		nchar,
		@Deleted		bit,
		@ExtraField		nchar,
		@ExtraFieldSP		nchar,
		@ReportStoredProcedure		nchar
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblReportObjects 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Extra Field],
		[Extra Field SP],
		[Report Stored Procedure]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@ExtraField,
		@ExtraFieldSP,
		@ReportStoredProcedure
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblDenominationsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDenominationsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblDenominationsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@DenominationCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblDenominations 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Denomination Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@DenominationCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblModificationTypeInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblModificationTypeInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblModificationTypeInsert](
		@Code		nchar,
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblModificationType 
	(
		[Code],
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted]
	)
	VALUES 
	(
		@Code,
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblGamingControllersInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblGamingControllersInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblGamingControllersInsert](
		@GamingControllerNumber		nvarchar(10),
		@PGC		bit,
		@AssetNumber		nvarchar(50),
		@Location		nvarchar(50),
		@LocationID		nvarchar(50),
		@Online		bit,
		@EPRom		nvarchar(50),
		@Port		nvarchar(50),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(50),
		@Modified		nvarchar(50),
		@Deleted		nvarchar(10)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblGamingControllers 
	(
		[Gaming Controller Number],
		[PGC],
		[Asset Number],
		[Location],
		[LocationID],
		[Online],
		[EPRom],
		[Port],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted]
	)
	VALUES 
	(
		@GamingControllerNumber,
		@PGC,
		@AssetNumber,
		@Location,
		@LocationID,
		@Online,
		@EPRom,
		@Port,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblBanksInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblBanksInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblBanksInsert](
		@BankCode		nvarchar(15),
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblBanks 
	(
		[Bank Code],
		[InsertedBy],
		[ModifiedBy],
		[InsertedDate],
		[ModifiedDate],
		[Deleted]
	)
	VALUES 
	(
		@BankCode,
		@InsertedBy,
		@ModifiedBy,
		@InsertedDate,
		@ModifiedDate,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblSearchColumnsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchColumnsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSearchColumnsInsert](
		@SearchColumnName		nchar,
		@SearchTable		nchar,
		@SearchColumnAlias		nchar,
		@SearchOperator		nchar,
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nchar,
		@ModifiedBy		nchar,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblSearchColumns 
	(
		[Search Column Name],
		[Search Table],
		[Search Column Alias],
		[Search Operator],
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted]
	)
	VALUES 
	(
		@SearchColumnName,
		@SearchTable,
		@SearchColumnAlias,
		@SearchOperator,
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblChassisInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblChassisInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblChassisInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@ChassisTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblChassis 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Chassis Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@ChassisTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblButtonPanelsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblButtonPanelsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblButtonPanelsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@ButtonPanelTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblButtonPanels 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Button Panel Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@ButtonPanelTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblAppInfoInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAppInfoInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblAppInfoInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblAppInfo 
	(
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblHistoryObjectsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblHistoryObjectsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblHistoryObjectsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nchar,
		@ModifiedBy		nchar,
		@Deleted		bit,
		@HistoryStoredProcedure		nchar
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblHistoryObjects 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[History Stored Procedure]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@HistoryStoredProcedure
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblEditableObjectsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblEditableObjectsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblEditableObjectsInsert](
		@TableName		nvarchar(30),
		@TableIDField		nvarchar(30),
		@SelectStoredProcedure		nvarchar(50),
		@InsertStoredProcedure		nvarchar(50),
		@UpdateStoredProcedure		nvarchar(50),
		@DeleteStoredProcedure		nvarchar(50),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nchar,
		@Modified		nchar,
		@Deleted		nchar,
		@ExtraField		nchar,
		@ExtraFieldSP		nchar,
		@ReportStoredProcedure		nchar
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblEditableObjects 
	(
		[Table Name],
		[Table ID Field],
		[Select Stored Procedure],
		[Insert Stored Procedure],
		[Update Stored Procedure],
		[Delete Stored Procedure],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted],
		[Extra Field],
		[Extra Field SP],
		[Report Stored Procedure]
	)
	VALUES 
	(
		@TableName,
		@TableIDField,
		@SelectStoredProcedure,
		@InsertStoredProcedure,
		@UpdateStoredProcedure,
		@DeleteStoredProcedure,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted,
		@ExtraField,
		@ExtraFieldSP,
		@ReportStoredProcedure
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblAuditInformationInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAuditInformationInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblAuditInformationInsert](
		@AfterXml		xml,
		@Comment		nvarchar(250),
		@AuditType		nvarchar(25),
		@Object		nvarchar(25),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(50),
		@Modified		nvarchar(50),
		@Deleted		bit,
		@MachineId		bigint,
		@ModificationCategoryId		bigint,
		@ModificationDescription		nchar,
		@ModificationDetailedDescription		nchar
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblAuditInformation 
	(
		[After Xml],
		[Comment],
		[AuditType],
		[Object],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted],
		[MachineId],
		[ModificationCategoryId],
		[ModificationDescription],
		[ModificationDetailedDescription]
	)
	VALUES 
	(
		@AfterXml,
		@Comment,
		@AuditType,
		@Object,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted,
		@MachineId,
		@ModificationCategoryId,
		@ModificationDescription,
		@ModificationDetailedDescription
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@PartTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblParts 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Part Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@PartTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachineSnapshotInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineSnapshotInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachineSnapshotInsert](
		@Comment		nvarchar(250),
		@InsertedDate		datetime,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblMachineSnapshot 
	(
		[Comment],
		[InsertedDate],
		[Deleted]
	)
	VALUES 
	(
		@Comment,
		@InsertedDate,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachineTypesInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineTypesInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachineTypesInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@MachineTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblMachineTypes 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Machine Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@MachineTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblSearchObjectsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchObjectsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSearchObjectsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nchar,
		@ModifiedBy		nchar,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblSearchObjects 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblNOCHistoryInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblNOCHistoryInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblNOCHistoryInsert](
		@TaskDate		datetime,
		@TicketNumber		nvarchar(50),
		@ContactPerson		nvarchar(50),
		@Technician		nvarchar(50),
		@Reason		nvarchar(100),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblNOCHistory 
	(
		[Task Date],
		[Ticket Number],
		[Contact Person],
		[Technician],
		[Reason],
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted]
	)
	VALUES 
	(
		@TaskDate,
		@TicketNumber,
		@ContactPerson,
		@Technician,
		@Reason,
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblIncidentStatusInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentStatusInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentStatusInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@IncidentStatusTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblIncidentStatus 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Incident Status Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@IncidentStatusTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblTopBoxesInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblTopBoxesInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblTopBoxesInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@TopBoxTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblTopBoxes 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Top Box Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@TopBoxTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblIncidentTypesInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentTypesInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentTypesInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@IncidentTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblIncidentTypes 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Incident Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@IncidentTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblBillValidatorsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblBillValidatorsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblBillValidatorsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@BillValidatorTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblBillValidators 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Bill Validator Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@BillValidatorTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblLocationsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblLocationsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblLocationsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@LocationCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblLocations 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Location Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@LocationCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachineStatesInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineStatesInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachineStatesInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@MachineStateTypeCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblMachineStates 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Machine State Type Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@MachineStateTypeCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblThemesInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblThemesInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblThemesInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit,
		@ThemeNameCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblThemes 
	(
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Theme Name Code]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@ThemeNameCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblReportColumnsInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportColumnsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblReportColumnsInsert](
		@ReportObjectName		nchar,
		@ReportColumnName		nchar,
		@ReportTable		nchar,
		@ReportColumnAlias		nchar,
		@ReportOperator		nchar,
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nchar,
		@ModifiedBy		nchar,
		@Deleted		bit,
		@ExtraField		nchar,
		@ExtraFieldSP		nchar,
		@ReportStoredProcedure		nchar
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblReportColumns 
	(
		[Report Object Name],
		[Report Column Name],
		[Report Table],
		[Report Column Alias],
		[Report Operator],
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted],
		[Extra Field],
		[Extra Field SP],
		[Report Stored Procedure]
	)
	VALUES 
	(
		@ReportObjectName,
		@ReportColumnName,
		@ReportTable,
		@ReportColumnAlias,
		@ReportOperator,
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted,
		@ExtraField,
		@ExtraFieldSP,
		@ReportStoredProcedure
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[sptblSavedSearchInsert]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sptblSavedSearchInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[sptblSavedSearchInsert](
		@SearchObject		varchar(50),
		@SearchColumn		varchar(50),
		@SearchVariable		varchar(50),
		@SearchDescription		varchar(50),
		@SearchDateTime		datetime,
		@SearchQuery		varchar(250),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		varchar(20),
		@Modified		varchar(20),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO tblSavedSearch 
	(
		[Search Object],
		[Search Column],
		[Search Variable],
		[Search Description],
		[Search DateTime],
		[Search Query],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted]
	)
	VALUES 
	(
		@SearchObject,
		@SearchColumn,
		@SearchVariable,
		@SearchDescription,
		@SearchDateTime,
		@SearchQuery,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted
	);

END' 
END
GO





 --UPDATE STORED PROCEDURE


/****** Object:  StoredProcedure [dbo].[spTblIncidentsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentsUpdate](
		@IncidentId		BIGINT,
		@IncidentType				NVARCHAR(100),
		@IncidentDetailedDescription				NVARCHAR(2000),
		@PartId				BIGINT,
		@PartType				NVARCHAR(100),
		@BillValidatorId				BIGINT,
		@BillValidatorType				NVARCHAR(100),
		@IncidentStatusId				BIGINT,
		@IncidentStatusType				NVARCHAR(100),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT,
		@DescriptionHistory				XML,
		@IncidentFor				NVARCHAR(30),
		@IncidentForId				BIGINT,
		@SerialNumber				NVARCHAR(50),
		@SerialNumberId				BIGINT,
		@MachineId				BIGINT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblIncidents 
		SET 
			[Incident Type] = @IncidentType,
			[IncidentDetailedDescription] = @IncidentDetailedDescription,
			[PartId] = @PartId,
			[Part Type] = @PartType,
			[BillValidatorId] = @BillValidatorId,
			[Bill Validator Type] = @BillValidatorType,
			[IncidentStatusId] = @IncidentStatusId,
			[Incident Status Type] = @IncidentStatusType,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted,
			[DescriptionHistory] = @DescriptionHistory,
			[Incident For] = @IncidentFor,
			[IncidentForId] = @IncidentForId,
			[Serial Number] = @SerialNumber,
			[SerialNumberId] = @SerialNumberId,
			[MachineId] = @MachineId
		where 
			[IncidentId] = @IncidentId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblVendorsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblVendorsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblVendorsUpdate](
		@VendorId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@VendorCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblVendors 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Vendor Code] = @VendorCode
		where 
			[VendorId] = @VendorId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblDisplayColumnsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDisplayColumnsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblDisplayColumnsUpdate](
		@DisplayColumnId		BIGINT,
		@DisplayGroup				NCHAR,
		@DisplayColumn				NCHAR,
		@DisplayColumnTable				NCHAR,
		@StoredProcedure				NVARCHAR(50)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblDisplayColumns 
		SET 
			[Display Group] = @DisplayGroup,
			[Display Column] = @DisplayColumn,
			[Display Column Table] = @DisplayColumnTable,
			[StoredProcedure] = @StoredProcedure
		where 
			[Display Column Id] = @DisplayColumnId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblCPUsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblCPUsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblCPUsUpdate](
		@CPUId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@CPUTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblCPUs 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[CPU Type Code] = @CPUTypeCode
		where 
			[CPUId] = @CPUId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblIncidentAuditInformationUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentAuditInformationUpdate]') AND type in (N'P', N'
PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentAuditInformationUpdate](
		@IncidentAuditInformationId		BIGINT,
		@AfterXml				XML,
		@IncidentId				BIGINT,
		@MachineId				BIGINT,
		@IncidentTypeId				BIGINT,
		@IncidentType				NVARCHAR(100),
		@IncidentDetailedDescription				NVARCHAR(2000),
		@PartId				BIGINT,
		@PartType				NVARCHAR(250),
		@BillValidatorId				BIGINT,
		@BillValidatorType				NVARCHAR(250),
		@IncidentStatusId				BIGINT,
		@IncidentStatusType				NVARCHAR(250),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(50),
		@Modified				NVARCHAR(50),
		@Deleted				BIT,
		@SerialNumberId				BIGINT,
		@SerialNumber				NVARCHAR(50)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblIncidentAuditInformation 
		SET 
			[AfterXml] = @AfterXml,
			[IncidentId] = @IncidentId,
			[MachineId] = @MachineId,
			[IncidentTypeId] = @IncidentTypeId,
			[Incident Type] = @IncidentType,
			[IncidentDetailedDescription] = @IncidentDetailedDescription,
			[PartId] = @PartId,
			[Part Type] = @PartType,
			[BillValidatorId] = @BillValidatorId,
			[Bill Validator Type] = @BillValidatorType,
			[IncidentStatusId] = @IncidentStatusId,
			[Incident Status Type] = @IncidentStatusType,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted,
			[SerialNumberId] = @SerialNumberId,
			[Serial Number] = @SerialNumber
		where 
			[IncidentAuditInformationId] = @IncidentAuditInformationId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachinesUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachinesUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachinesUpdate](
		@MachineId		BIGINT,
		@AssetNumber				NVARCHAR(30),
		@Bank				NVARCHAR(20),
		@HouseNumber				NVARCHAR(20),
		@Registration				NVARCHAR(20),
		@IPAddress				NVARCHAR(20),
		@Port				NVARCHAR(20),
		@ThemeNameID				NVARCHAR(30),
		@ThemeName				NVARCHAR(100),
		@ThemeInstallDate				DATETIME,
		@Template				NVARCHAR(30),
		@DenominationID				NVARCHAR(10),
		@Denomination				NVARCHAR(10),
		@LocationID				NVARCHAR(20),
		@Location				NVARCHAR(20),
		@PlayerTerminalTypeID				NVARCHAR(20),
		@PlayerTerminalType				NVARCHAR(20),
		@VendorID				NVARCHAR(30),
		@VendorName				NVARCHAR(30),
		@ChassisTypeID				NVARCHAR(30),
		@ChassisType				NVARCHAR(30),
		@TicketPrinterTypeID				NVARCHAR(30),
		@TicketPrinterType				NVARCHAR(30),
		@ButtonPanelTypeID				NVARCHAR(30),
		@ButtonPanelType				NVARCHAR(30),
		@CPUTypeID				NVARCHAR(30),
		@CPUType				NVARCHAR(30),
		@TopBoxTypeID				NVARCHAR(30),
		@TopBoxType				NVARCHAR(30),
		@MachineTypeID				NVARCHAR(30),
		@MachineType				NVARCHAR(30),
		@MachineStateID				NVARCHAR(30),
		@MachineState				NVARCHAR(30),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(50),
		@Modified				NVARCHAR(50),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblMachines 
		SET 
			[Asset Number] = @AssetNumber,
			[Bank] = @Bank,
			[House Number] = @HouseNumber,
			[Registration] = @Registration,
			[IP Address] = @IPAddress,
			[Port] = @Port,
			[ThemeNameID] = @ThemeNameID,
			[Theme Name] = @ThemeName,
			[Theme Install Date] = @ThemeInstallDate,
			[Template] = @Template,
			[DenominationID] = @DenominationID,
			[Denomination] = @Denomination,
			[LocationID] = @LocationID,
			[Location] = @Location,
			[PlayerTerminalTypeID] = @PlayerTerminalTypeID,
			[Player Terminal Type] = @PlayerTerminalType,
			[VendorID] = @VendorID,
			[Vendor Name] = @VendorName,
			[ChassisTypeID] = @ChassisTypeID,
			[Chassis Type] = @ChassisType,
			[TicketPrinterTypeID] = @TicketPrinterTypeID,
			[Ticket Printer Type] = @TicketPrinterType,
			[ButtonPanelTypeID] = @ButtonPanelTypeID,
			[Button Panel Type] = @ButtonPanelType,
			[CPUTypeID] = @CPUTypeID,
			[CPU Type] = @CPUType,
			[TopBoxTypeID] = @TopBoxTypeID,
			[Top Box Type] = @TopBoxType,
			[MachineTypeID] = @MachineTypeID,
			[Machine Type] = @MachineType,
			[MachineStateID] = @MachineStateID,
			[Machine State] = @MachineState,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted
		where 
			[MachineId] = @MachineId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblTicketPrintersUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblTicketPrintersUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblTicketPrintersUpdate](
		@TicketPrinterId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@TicketPrinterTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblTicketPrinters 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Ticket Printer Type Code] = @TicketPrinterTypeCode
		where 
			[TicketPrinterId] = @TicketPrinterId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblStaticObjectsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblStaticObjectsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblStaticObjectsUpdate](
		@StaticObjectId		BIGINT,
		@StaticObjectType				NVARCHAR(30),
		@StaticObjectTable				NVARCHAR(30),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@StaticObjectDisplayField				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblStaticObjects 
		SET 
			[Static Object Type] = @StaticObjectType,
			[Static Object Table] = @StaticObjectTable,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Static Object Display Field] = @StaticObjectDisplayField
		where 
			[Static Object Id] = @StaticObjectId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblIncidentForUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentForUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentForUpdate](
		@IncidentForId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@IncidentForCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblIncidentFor 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Incident For Code] = @IncidentForCode
		where 
			[IncidentForId] = @IncidentForId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachineOperationsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineOperationsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachineOperationsUpdate](
		@MachineOperationsId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@MachineOperationsTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblMachineOperations 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Machine Operations Type Code] = @MachineOperationsTypeCode
		where 
			[MachineOperationsId] = @MachineOperationsId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblReportObjectsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportObjectsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblReportObjectsUpdate](
		@ReportObjectId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NCHAR,
		@ModifiedBy				NCHAR,
		@Deleted				BIT,
		@ExtraField				NCHAR,
		@ExtraFieldSP				NCHAR,
		@ReportStoredProcedure				NCHAR
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblReportObjects 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Extra Field] = @ExtraField,
			[Extra Field SP] = @ExtraFieldSP,
			[Report Stored Procedure] = @ReportStoredProcedure
		where 
			[Report Object Id] = @ReportObjectId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblDenominationsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDenominationsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblDenominationsUpdate](
		@DenominationId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@DenominationCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblDenominations 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Denomination Code] = @DenominationCode
		where 
			[DenominationId] = @DenominationId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblModificationTypeUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblModificationTypeUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblModificationTypeUpdate](
		@ModificationCategoryId		BIGINT,
		@Code				NCHAR,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblModificationType 
		SET 
			[Code] = @Code,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted
		where 
			[ModificationCategoryId] = @ModificationCategoryId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblGamingControllersUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblGamingControllersUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblGamingControllersUpdate](
		@GamingControllerId		BIGINT,
		@GamingControllerNumber				NVARCHAR(10),
		@PGC				BIT,
		@AssetNumber				NVARCHAR(50),
		@Location				NVARCHAR(50),
		@LocationID				NVARCHAR(50),
		@Online				BIT,
		@EPRom				NVARCHAR(50),
		@Port				NVARCHAR(50),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(50),
		@Modified				NVARCHAR(50),
		@Deleted				NVARCHAR(10)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblGamingControllers 
		SET 
			[Gaming Controller Number] = @GamingControllerNumber,
			[PGC] = @PGC,
			[Asset Number] = @AssetNumber,
			[Location] = @Location,
			[LocationID] = @LocationID,
			[Online] = @Online,
			[EPRom] = @EPRom,
			[Port] = @Port,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted
		where 
			[GamingControllerId] = @GamingControllerId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblBanksUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblBanksUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblBanksUpdate](
		@BankId		BIGINT,
		@BankCode				NVARCHAR(15),
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblBanks 
		SET 
			[Bank Code] = @BankCode,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Deleted] = @Deleted
		where 
			[BankId] = @BankId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblSearchColumnsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchColumnsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSearchColumnsUpdate](
		@SearchColumnId		BIGINT,
		@SearchColumnName				NCHAR,
		@SearchTable				NCHAR,
		@SearchColumnAlias				NCHAR,
		@SearchOperator				NCHAR,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NCHAR,
		@ModifiedBy				NCHAR,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblSearchColumns 
		SET 
			[Search Column Name] = @SearchColumnName,
			[Search Table] = @SearchTable,
			[Search Column Alias] = @SearchColumnAlias,
			[Search Operator] = @SearchOperator,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted
		where 
			[Search Column Id] = @SearchColumnId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblChassisUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblChassisUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblChassisUpdate](
		@ChassisId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@ChassisTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblChassis 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Chassis Type Code] = @ChassisTypeCode
		where 
			[ChassisId] = @ChassisId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblButtonPanelsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblButtonPanelsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblButtonPanelsUpdate](
		@ButtonPanelId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@ButtonPanelTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblButtonPanels 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Button Panel Type Code] = @ButtonPanelTypeCode
		where 
			[ButtonPanelId] = @ButtonPanelId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblAppInfoUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAppInfoUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblAppInfoUpdate](
		@AppInfoId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblAppInfo 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted
		where 
			[AppInfoId] = @AppInfoId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblHistoryObjectsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblHistoryObjectsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblHistoryObjectsUpdate](
		@HistoryObjectId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NCHAR,
		@ModifiedBy				NCHAR,
		@Deleted				BIT,
		@HistoryStoredProcedure				NCHAR
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblHistoryObjects 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[History Stored Procedure] = @HistoryStoredProcedure
		where 
			[History Object Id] = @HistoryObjectId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblEditableObjectsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblEditableObjectsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblEditableObjectsUpdate](
		@EditableObjectId		BIGINT,
		@TableName				NVARCHAR(30),
		@TableIDField				NVARCHAR(30),
		@SelectStoredProcedure				NVARCHAR(50),
		@InsertStoredProcedure				NVARCHAR(50),
		@UpdateStoredProcedure				NVARCHAR(50),
		@DeleteStoredProcedure				NVARCHAR(50),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NCHAR,
		@Modified				NCHAR,
		@Deleted				NCHAR,
		@ExtraField				NCHAR,
		@ExtraFieldSP				NCHAR,
		@ReportStoredProcedure				NCHAR
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblEditableObjects 
		SET 
			[Table Name] = @TableName,
			[Table ID Field] = @TableIDField,
			[Select Stored Procedure] = @SelectStoredProcedure,
			[Insert Stored Procedure] = @InsertStoredProcedure,
			[Update Stored Procedure] = @UpdateStoredProcedure,
			[Delete Stored Procedure] = @DeleteStoredProcedure,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted,
			[Extra Field] = @ExtraField,
			[Extra Field SP] = @ExtraFieldSP,
			[Report Stored Procedure] = @ReportStoredProcedure
		where 
			[EditableObjectId] = @EditableObjectId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblAuditInformationUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAuditInformationUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblAuditInformationUpdate](
		@AuditInformationId		BIGINT,
		@AfterXml				XML,
		@Comment				NVARCHAR(250),
		@AuditType				NVARCHAR(25),
		@Object				NVARCHAR(25),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(50),
		@Modified				NVARCHAR(50),
		@Deleted				BIT,
		@MachineId				BIGINT,
		@ModificationCategoryId				BIGINT,
		@ModificationDescription				NCHAR,
		@ModificationDetailedDescription				NCHAR
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblAuditInformation 
		SET 
			[After Xml] = @AfterXml,
			[Comment] = @Comment,
			[AuditType] = @AuditType,
			[Object] = @Object,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted,
			[MachineId] = @MachineId,
			[ModificationCategoryId] = @ModificationCategoryId,
			[ModificationDescription] = @ModificationDescription,
			[ModificationDetailedDescription] = @ModificationDetailedDescription
		where 
			[AuditInformationId] = @AuditInformationId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartsUpdate](
		@PartId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@PartTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblParts 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Part Type Code] = @PartTypeCode
		where 
			[PartId] = @PartId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachineSnapshotUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineSnapshotUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachineSnapshotUpdate](
		@MachineSnapshotId		BIGINT,
		@Comment				NVARCHAR(250),
		@InsertedDate				DATETIME,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblMachineSnapshot 
		SET 
			[Comment] = @Comment,
			[InsertedDate] = @InsertedDate,
			[Deleted] = @Deleted
		where 
			[MachineSnapshotId] = @MachineSnapshotId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachineTypesUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineTypesUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachineTypesUpdate](
		@MachineTypeId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@MachineTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblMachineTypes 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Machine Type Code] = @MachineTypeCode
		where 
			[MachineTypeId] = @MachineTypeId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblSearchObjectsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchObjectsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSearchObjectsUpdate](
		@SearchObjectId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NCHAR,
		@ModifiedBy				NCHAR,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblSearchObjects 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted
		where 
			[Search Object Id] = @SearchObjectId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblNOCHistoryUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblNOCHistoryUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblNOCHistoryUpdate](
		@NOCHistoryId		BIGINT,
		@TaskDate				DATETIME,
		@TicketNumber				NVARCHAR(50),
		@ContactPerson				NVARCHAR(50),
		@Technician				NVARCHAR(50),
		@Reason				NVARCHAR(100),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblNOCHistory 
		SET 
			[Task Date] = @TaskDate,
			[Ticket Number] = @TicketNumber,
			[Contact Person] = @ContactPerson,
			[Technician] = @Technician,
			[Reason] = @Reason,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted
		where 
			[NOCHistoryId] = @NOCHistoryId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblIncidentStatusUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentStatusUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentStatusUpdate](
		@IncidentStatusId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@IncidentStatusTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblIncidentStatus 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Incident Status Type Code] = @IncidentStatusTypeCode
		where 
			[IncidentStatusId] = @IncidentStatusId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblTopBoxesUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblTopBoxesUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblTopBoxesUpdate](
		@TopBoxId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@TopBoxTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblTopBoxes 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Top Box Type Code] = @TopBoxTypeCode
		where 
			[TopBoxId] = @TopBoxId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblIncidentTypesUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblIncidentTypesUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblIncidentTypesUpdate](
		@IncidentTypeId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@IncidentTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblIncidentTypes 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Incident Type Code] = @IncidentTypeCode
		where 
			[IncidentTypeId] = @IncidentTypeId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblBillValidatorsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblBillValidatorsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblBillValidatorsUpdate](
		@BillValidatorId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@BillValidatorTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblBillValidators 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Bill Validator Type Code] = @BillValidatorTypeCode
		where 
			[BillValidatorId] = @BillValidatorId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblLocationsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblLocationsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblLocationsUpdate](
		@LocationId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@LocationCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblLocations 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Location Code] = @LocationCode
		where 
			[LocationId] = @LocationId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblMachineStatesUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblMachineStatesUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblMachineStatesUpdate](
		@MachineStateId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@MachineStateTypeCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblMachineStates 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Machine State Type Code] = @MachineStateTypeCode
		where 
			[MachineStateId] = @MachineStateId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblThemesUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblThemesUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblThemesUpdate](
		@ThemeId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT,
		@ThemeNameCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblThemes 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Theme Name Code] = @ThemeNameCode
		where 
			[ThemeId] = @ThemeId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblReportColumnsUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportColumnsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblReportColumnsUpdate](
		@ReportColumnId		BIGINT,
		@ReportObjectName				NCHAR,
		@ReportColumnName				NCHAR,
		@ReportTable				NCHAR,
		@ReportColumnAlias				NCHAR,
		@ReportOperator				NCHAR,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NCHAR,
		@ModifiedBy				NCHAR,
		@Deleted				BIT,
		@ExtraField				NCHAR,
		@ExtraFieldSP				NCHAR,
		@ReportStoredProcedure				NCHAR
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblReportColumns 
		SET 
			[Report Object Name] = @ReportObjectName,
			[Report Column Name] = @ReportColumnName,
			[Report Table] = @ReportTable,
			[Report Column Alias] = @ReportColumnAlias,
			[Report Operator] = @ReportOperator,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted,
			[Extra Field] = @ExtraField,
			[Extra Field SP] = @ExtraFieldSP,
			[Report Stored Procedure] = @ReportStoredProcedure
		where 
			[Report Column Id] = @ReportColumnId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[sptblSavedSearchUpdate]    Script Date: 2012-03-14 19:28:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sptblSavedSearchUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[sptblSavedSearchUpdate](
		@SavedSearchId		BIGINT,
		@SearchObject				VARCHAR(50),
		@SearchColumn				VARCHAR(50),
		@SearchVariable				VARCHAR(50),
		@SearchDescription				VARCHAR(50),
		@SearchDateTime				DATETIME,
		@SearchQuery				VARCHAR(250),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				VARCHAR(20),
		@Modified				VARCHAR(20),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE tblSavedSearch 
		SET 
			[Search Object] = @SearchObject,
			[Search Column] = @SearchColumn,
			[Search Variable] = @SearchVariable,
			[Search Description] = @SearchDescription,
			[Search DateTime] = @SearchDateTime,
			[Search Query] = @SearchQuery,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted
		where 
			[Saved Search Id] = @SavedSearchId;

END' 
END
GO
