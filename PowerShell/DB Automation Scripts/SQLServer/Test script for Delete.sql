/****** Object:  StoredProcedure [dbo].[spTblStaticObjectsDelete]    Script Date: 02/20/2012 01:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblStaticObjectsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblStaticObjectsDelete](
			@StaticObjectId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblStaticObjects] SET Deleted = 1 WHERE [Static Object Id]  = @StaticObjectId;

	END' 
END
GO