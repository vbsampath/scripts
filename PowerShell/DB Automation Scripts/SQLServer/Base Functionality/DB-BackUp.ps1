#Loading required assemblies
[void][System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.ConnectionInfo');            
[void][System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.Management.Sdk.Sfc');            
[void][System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.SMO');            
[void][System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.SMOExtended');            


#If arguments are insufficient
if($args.count -ne 3)
{
	write-output "Usage:"
	write-output 'Syntax : "ServerName" "Dbname" "Destination dir"'
	write-output 'Example : "SQLPOC" "QCCDB" "C:\DatabaseBackups\"'
	write-output 'Additional Information : This script creates backup of the database in the directory provided'
	return;
};


#Setting up arguments to variables
$Server = $args[0];                 
$Database = $args[1];
$Dest = $args[2]; 

#Validating parameters
if($Server -eq '')
{	write-output "Server name cannot be empty. Please provide a valid name..."
	return;
}
if($Database -eq '')
{
	write-output "Database name cannot be empty. Please provide a valid name..."
	return;
}
if($Dest -eq '')
{
	$Dest = $server.Settings.BackupDirectory + "\" 
}


#Setting up servername
$srv = New-Object Microsoft.SqlServer.Management.Smo.Server $Server;            


Write-Output ("Started at: " + (Get-Date -format yyyy-MM-dd-HH:mm:ss));            

#Setting up database
$db = $srv.Databases.Item($Database) 


if($db)
  {
	$timestamp = Get-Date -format yyyy-MM-dd-HH-mm-ss;            
	$backup = New-Object ("Microsoft.SqlServer.Management.Smo.Backup");            
	$backup.Action = "Database";            
	$backup.Database = $db.Name;            
	$backup.Devices.AddDevice($Dest + $db.Name + "_full_" + $timestamp + ".bak", "File");            
	$backup.BackupSetDescription = "Full backup of " + $db.Name + " " + $timestamp;            
	$backup.Incremental = 0;            
	
	# Starting full backup process.            
	$backup.SqlBackup($srv);     
	
	# For db with recovery mode <> simple: Log backup.            
	If ($db.RecoveryModel -ne 3)            
	{            
		$timestamp = Get-Date -format yyyy-MM-dd-HH-mm-ss;            
		$backup = New-Object ("Microsoft.SqlServer.Management.Smo.Backup");            
		$backup.Action = "Log";            
		$backup.Database = $db.Name;            
		$backup.Devices.AddDevice($Dest + $db.Name + "_log_" + $timestamp + ".trn", "File");            
		$backup.BackupSetDescription = "Log backup of " + $db.Name + " " + $timestamp;            
		
		#Specify that the log must be truncated after the backup is complete.            
		$backup.LogTruncation = "Truncate";
		
		# Starting log backup process            
		$backup.SqlBackup($srv);            
	};            
  }
else
{
	write-output "No Database present!!!"
	return;
}      

Write-Output ("Finished at: " + (Get-Date -format  yyyy-MM-dd-HH:mm:ss));


#Full-backup for every database            
<#
foreach ($db in $srv.Databases)            
{            
    If($db.Name -ne "tempdb")  # Non need to backup TempDB            
    {            
        $timestamp = Get-Date -format yyyy-MM-dd-HH-mm-ss;            
        $backup = New-Object ("Microsoft.SqlServer.Management.Smo.Backup");            
        $backup.Action = "Database";            
        $backup.Database = $db.Name;            
        $backup.Devices.AddDevice($Dest + $db.Name + "_full_" + $timestamp + ".bak", "File");            
        $backup.BackupSetDescription = "Full backup of " + $db.Name + " " + $timestamp;            
        $backup.Incremental = 0;            
        # Starting full backup process.            
        $backup.SqlBackup($srv);     
        # For db with recovery mode <> simple: Log backup.            
        If ($db.RecoveryModel -ne 3)            
        {            
            $timestamp = Get-Date -format yyyy-MM-dd-HH-mm-ss;            
            $backup = New-Object ("Microsoft.SqlServer.Management.Smo.Backup");            
            $backup.Action = "Log";            
            $backup.Database = $db.Name;            
            $backup.Devices.AddDevice($Dest + $db.Name + "_log_" + $timestamp + ".trn", "File");            
            $backup.BackupSetDescription = "Log backup of " + $db.Name + " " + $timestamp;            
            #Specify that the log must be truncated after the backup is complete.            
            $backup.LogTruncation = "Truncate";
            # Starting log backup process            
            $backup.SqlBackup($srv);            
        };            
    };            
};
#>            


