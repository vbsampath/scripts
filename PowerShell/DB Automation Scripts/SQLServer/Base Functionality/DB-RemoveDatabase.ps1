#Loading required assemblies
[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo")

#If arguments are insufficient
if($args.count -ne 2)
{
	write-output "Usage:"
	write-output 'Syntax : "ServerName" "Database name"'
	write-output 'Example : "SQLPOC" "QCCDB"'
	write-output 'Additional Information : This script deletes the database'
};

if($args.count -eq 2)
{
	#Setting up arguments to variables
	$Server = $args[0];
	$dbName = $args[1];
	$continue = $True


	#Validating parameters
	if($Server -eq '')
	{
		write-output "Server name cannot be empty. Please provide a valid name..."
		$continue = $False;
	}
	if($dbName -eq '')
	{
		write-output "Database name cannot be empty. Please provide a valid name..."
		$continue = $False;
	}
	
	if($continue)
	{
		#Declaring variables
		$dbServer = new-object Microsoft.SqlServer.Management.Smo.Server ($Server)
		$dbNew = new-object Microsoft.SqlServer.Management.Smo.Database
		$found = $False
		
		#Search databases on the server
		foreach ($db in $dbServer.Databases)
		{
			if ($db.Name -eq $dbName)
			{
				$dbNew = $db
				$found = $True
			}
		}

		#If the database is found
		if ($found)
		{
			write-output "Deleting database $dbName..."
			$dbServer.KillAllProcesses($dbNew.Name)
			$dbServer.KillDatabase($dbNew.Name)
		}
		else
		{
			write-output "Database $dbName not found..."
		}
	}
};
