
#Loading required assemblies   
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoEnum") | Out-Null
 
 
 #If arguments are insufficient
if($args.count -ne 3)
{
	write-output "Usage:"
	write-output 'Syntax : "ServerName" "Backup file path to use" "Text to append"'
	write-output 'Example : "SQLPOC" "C:\BackUp\QCCDB_full_2011-10-06-04-04-37.bak" "Copy"'
	write-output 'Additional Information : This script restores a backup with additional text appended to database if provided'
	return;
};
 


$Server = $args[0]; 
$BackupFile = $args[1];
$TextToAppend = $args[2];


#Validating parameters
if($Server -eq '')
{	write-output "Server name cannot be empty. Please provide a valid name..."
	return;
}
if($BackupFile -eq '')
{
	write-output "Backup file path cannot be empty. Please provide a valid path..."
	return;
}
if($TextToAppend -eq '')
{
	$TextToAppend = 'Copy'
	write-output "Text to append is not provided so defaulting to 'Copy'"
}


 

$server = New-Object Microsoft.SqlServer.Management.Smo.Server($Server)
$backupDevice = New-Object Microsoft.SqlServer.Management.Smo.BackupDeviceItem($BackupFile, "File")
$smoRestore = new-object Microsoft.SqlServer.Management.Smo.Restore
 

$smoRestore.NoRecovery = $false;
$smoRestore.Devices.Add($backupDevice)


$smoRestoreDetails = $smoRestore.ReadBackupHeader($server) 
 
"Database Name Before from Backup Header : " +$smoRestoreDetails.Rows[0]["DatabaseName"]
 

$smoRestore.Database =$smoRestoreDetails.Rows[0]["DatabaseName"] + $TextToAppend
"Database Name After from Backup Header : " + $smoRestore.Database


$smoRestoreFile = New-Object Microsoft.SqlServer.Management.Smo.RelocateFile
$smoRestoreLog = New-Object Microsoft.SqlServer.Management.Smo.RelocateFile
 

$smoRestoreFile.LogicalFileName = $smoRestoreDetails.Rows[0]["DatabaseName"] 
$smoRestoreFile.PhysicalFileName = $server.Information.MasterDBPath + "\" + $smoRestore.Database + ".mdf"
Write-OutPut $smoRestoreFile.LogicalFileName 
Write-OutPut $smoRestoreFile.PhysicalFileName

$smoRestoreLog.LogicalFileName = $smoRestoreDetails.Rows[0]["DatabaseName"] + "_Log"
$smoRestoreLog.PhysicalFileName = $server.Information.MasterDBLogPath  + "\" + $smoRestore.Database + "_Log.ldf"
Write-OutPut $smoRestoreLog.LogicalFileName 
Write-OutPut $smoRestoreLog.PhysicalFileName 

$smoRestore.RelocateFiles.Add($smoRestoreFile)
$smoRestore.RelocateFiles.Add($smoRestoreLog)

 
#restore database
$smoRestore.SqlRestore($server)





 
<#
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoEnum") | Out-Null
 

$server = new-object Microsoft.SqlServer.Management.Smo.Server($args[0])
$smoRestore = new-object Microsoft.SqlServer.Management.Smo.Restore
 

$smoRestore.NoRecovery = $FALSE;
$smoRestore.Database = $args[1]
$smoRestore.Devices.AddDevice($args[2], [Microsoft.SqlServer.Management.Smo.DeviceType]::File)
 

$smoRestore.SqlRestore($server)
#>
