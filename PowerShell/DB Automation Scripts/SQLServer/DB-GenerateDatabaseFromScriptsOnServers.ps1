#If arguments are insufficient
if($args.count -ne 4)
{
	write-output "Usage:"
	write-output 'Syntax : "ServerName" "Database name" "Option" "Path of generated scripts"'
	write-output 'Example : "SQLPOC" "QCCDB" "SchemaOnly" "C:\Users\sampathv\Documents\PIMS\January\27-jan-2012\1230PM\"'
	write-output 'Additional Information : This script deletes previously existing database on remote server and creates database from scripts which are already generated'
	return;
}

$server = $args[0];
$dbname = $args[1]; 
$option = $args[2];
$source = $args[3];


#Validating parameters
if($server -eq '')
{	write-output "Server name cannot be empty. Please provide a valid name..."
	return;
}
if($dbname -eq '')
{
	write-output "Database name cannot be empty. Please provide a valid name..."
	return;
}
if($option -eq '')
{
	$option = 'SchemaOnly'
}
if($source -eq '')
{	write-output "Source path cannot be empty. Please provide a valid path to scripts..."
	return;
}


#Adding required snapins
Write-Host "Adding Snapins..." -ForegroundColor gray

Add-PSSnapin -Name sqlserverprovidersnapin100 -ErrorAction SilentlyCOntinue -ErrorVariable err
if ($err){
    Write-Host "sqlserverprovidersnapin100 snapin already exists!!! Proceding to next snapin" -ForegroundColor green
}else{
    Write-Host "sqlserverprovidersnapin100 Snapin installed" -ForegroundColor green
}


Add-PSSnapin -Name sqlservercmdletsnapin100 -ErrorAction SilentlyCOntinue -ErrorVariable err
if ($err){
    Write-Host "sqlservercmdletsnapin100 snapin already exists!!! Proceding to next snapin" -ForegroundColor green
}else{
    Write-Host "sqlservercmdletsnapin100 Snapin installed" -ForegroundColor green
}







#Getting current directory to access dependency scripts
[Environment]::CurrentDirectory=(Get-Location -PSProvider FileSystem).ProviderPath
$curr_dir = [IO.Directory]::GetCurrentDirectory();


#removing database on remote server if any
Write-Host "Removing Database $dbname if Any...."  -ForegroundColor gray
Start-Job -Name 'RemoveDB' -FilePath $curr_dir'\Base Functionality\DB-RemoveDatabase.ps1' -ArgumentList $server,$dbname
Wait-Job -Name 'RemoveDB'
Remove-Job -Name 'RemoveDB'


#creating database on remote server if any
Write-Host "Creating Database $dbname..."  -ForegroundColor gray
Start-Job -Name 'CreateDB' -FilePath $curr_dir'\Base Functionality\DB-CreateDatabase.ps1' -ArgumentList $server,$dbname
Wait-Job -Name 'CreateDB'
Remove-Job -Name 'CreateDB'

#--------------------------------------------------------------------------

Write-Host "Creating Tables with Data...."  -ForegroundColor gray

If($option -eq 'SchemaOnly')
{
$na = $source + "*_Schema.sql"
}

If($option -eq 'SchemaAndData')
{
$na = $source + "*_SchemaAndData.sql"
}

$a = Get-ChildItem -Path $na

$actualFile = $source + $a.name
Invoke-Sqlcmd -Database $dbname -InputFile $actualFile -serverinstance $server

Write-Host "Completed Tables Creation..."   -ForegroundColor green

#--------------------------------------------------------------------------


Write-Host "Creating Stored Procedures...."  -ForegroundColor gray

$na = $source + "*_StoredProcedures.sql"
$a = Get-ChildItem -Path $na

$actualFile = $source + $a.name
Invoke-Sqlcmd -Database $dbname -InputFile $actualFile -serverinstance $server

Write-Host "Completed Stored Procedures Creation..."   -ForegroundColor green

#--------------------------------------------------------------------------


Write-Host "Creating User Defined Functions...."  -ForegroundColor gray

$na = $source + "*_UserDefinedFunctions.sql"
$a = Get-ChildItem -Path $na

$actualFile = $source + $a.name
Invoke-Sqlcmd -Database $dbname -InputFile $actualFile -serverinstance $server

Write-Host "Completed Stored Procedures Creation..."   -ForegroundColor green


#--------------------------------------------------------------------------


Write-Host "Creating Views...."  -ForegroundColor gray

$na = $source + "*_Views.sql"
$a = Get-ChildItem -Path $na

$actualFile = $source + $a.name
Invoke-Sqlcmd -Database $dbname -InputFile $actualFile -serverinstance $server

Write-Host "Completed Views Creation..."   -ForegroundColor green

#---------------------------------------------------------------------------


Write-Host "Creating Table Triggers...."  -ForegroundColor gray

$na = $source + "*_TableTriggers.sql"
$a = Get-ChildItem -Path $na

$actualFile = $source + $a.name
Invoke-Sqlcmd -Database $dbname -InputFile $actualFile -serverinstance $server

Write-Host "Completed Table Triggers Creation..."   -ForegroundColor green

#----------------------------------------------------------------------------

Write-Host "Creating Database Triggers...."  -ForegroundColor gray

$na = $source + "*_DatabaseTriggers.sql"
$a = Get-ChildItem -Path $na

$actualFile = $source + $a.name
Invoke-Sqlcmd -Database $dbname -InputFile $actualFile -serverinstance $server

Write-Host "Completed Database Triggers Creation..."   -ForegroundColor green

#-----------------------------------------------------------------------------