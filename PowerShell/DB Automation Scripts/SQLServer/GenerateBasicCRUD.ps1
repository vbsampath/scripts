[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo")

#If arguments are insufficient
if($args.count -ne 3)
{
	write-output "Usage:"
	write-output 'Syntax : "ServerName" "Database name" "Destination Directory" '
	write-output 'Example : "SQLPOC" "QCCDB" "C:\dbCRUDscripts\"'
	write-output 'Additional Information : This script generates basic database CRUD. Its limitations are 1)Not supported when table has more than two primary keys 2)Assumes the tables are having its primary keys as auto increment'
    return;
};


#Setting up arguments to variables
$Server = $args[0];
$dbName = $args[1];
$Path = $args[2];


#Check if directory path exists otherwise create one
if((Test-Path -path $Path) -ne $True)
{
    New-Item $Path -type directory | Out-Null
    $Path = $Path + '\'
	write-output "Folder created $Path"
}  
	
#Making the path a correct directory
elseif(!$Path.EndsWith("\"))
{
	$Path = $Path + "\"
}



#Declaring variables
$dbServer = new-object Microsoft.SqlServer.Management.Smo.Server ($Server)

$dbName = $dbServer.Databases[$dbName]

function GetAllColumns
{param($Database)
    $Col = @()
    foreach ($table in $Database.Tables)
    {
    	foreach($column in $table.Columns)
    	{
            $Columns = $column | SELECT Parent,Name,DataType,InPrimaryKey   
            $Col += @($Columns)
    	}
    }
    return $Col
}

function GetColumnsForRead
{param($Database)
    $hash = @{}
    [int]$CountCol = 0
    foreach ($table in $Database.Tables)
    {
    	foreach($column in $table.Columns)
    	{
            $Columns = $column | Where-Object {$_.InPrimaryKey -eq $true} | SELECT Parent,Name,DataType
            if(![string]::IsNullOrEmpty($Columns.Parent.Name))
            {
                $temp = New-Object 'object[,]' 1,3
                $temp = ($Columns.Parent.Name,$Columns.Name,$Columns.DataType.Name)
                $hash.add($CountCol,$temp)
                $CountCol = $CountCol + 1    
            }
    	}
    }
    return $hash
}

function GetColumnsOfTableForRead
{param($Database, [string]$TableName)
    $Col = @()
    $Table = New-Object ("Microsoft.SqlServer.Management.SMO.Table")
    $Table.Name = $TableName
    $table = $Database.Tables[$Table]
	foreach($column in $Table.Columns)
	{
        
        $Columns = $column | Where-Object {$_.InPrimaryKey -eq $true} | SELECT Name,DataType
        $Col += @($Columns)
	}
    return $Col
}

function GetColumnsForDelete
{param($Database)
    $hash = @{}
    [int]$CountCol = 0
    foreach ($table in $Database.Tables)
    {
    	foreach($column in $table.Columns)
    	{
            $Columns = $column | Where-Object {$_.InPrimaryKey -eq $true} | SELECT Parent,Name,DataType
            if(![string]::IsNullOrEmpty($Columns.Parent.Name))
            {
                $temp = New-Object 'object[,]' 1,3
                $temp = ($Columns.Parent.Name,$Columns.Name,$Columns.DataType.Name)
                $hash.add($CountCol,$temp)
                $CountCol = $CountCol + 1    
            }
    	}
    }
    return $hash
}

function GetConsolidatedColumns
{param($Col)
    
}

function GetColumnsForInsertForTable
{param($Database, [string]$TableName)
    $Col = @()
    $Table = New-Object ("Microsoft.SqlServer.Management.SMO.Table")
    $Table.Name = $TableName
    $table = $Database.Tables[$Table]
    foreach($column in $table.Columns)
    {
        $Columns = $column  | SELECT Parent,Name,DataType,InPrimaryKey
        $Col += @($Columns)

    }
    return $Col
}

function GetColumnsForInsert
{param($Database)
    $temp = @()
    [hashtable]$hash = @{}
    $Parent = ""
    $Columns = @{}
    $DataType = @{}
    $Variables = @{}
    foreach ($table in $Database.Tables)
    {
        #Get Table Name
        $Parent = $table.Name
        [int]$Count = 0
        foreach($column in $table.Columns)
        {

            $temp = $column |  Where-Object{$_.InPrimaryKey -eq $false} |
            SELECT Parent, 
                   Name, 
                   @{Name='DataType';Expression = 
                        {
                            [string]$DataType = $_.Properties['DataType'].Value 
                            [string]$Length = $_.Properties['Length'].Value 
                            if($DataType -Contains "nvarchar" -or $DataType -Contains "varchar")
                            {
                                $DataType = $DataType +"("+$Length+")"
                            }
                            write-output $DataType
                        }
                    },
                   @{Name='Variable';Expression = 
                        {
                            [string]$col = $_.name
                            $col = $col.replace(" ","")
                            write-output $col
                        }
                    }
           
            
            if(![string]::IsNullOrEmpty($temp.Name))
            {
                $Columns.Add($Count,$temp.Name)
                $DataType.Add($Count,$temp.DataType)
                $Variables.Add($Count,$temp.Variable)
                $Count = $Count + 1
            }
            
        }
        
        $Consolidated = @{}
        $Consolidated = @{"Columns"=$Columns;"DataType"=$DataType;"Variables"=$Variables}
        
        $Columns = @{}
        $DataType = @{}
        $Variables = @{}
        
        #Add each columns, datatypes, variables to hash table
        $hash.Add($Parent,$Consolidated) 
    }
    #write-output $hash 
    return $hash 
}


function GetColumnsForUpdate
{param($Database)
    $temp = @()
    [hashtable]$hash = @{}
    $Parent = ""
    $Columns = @{}
    $DataType = @{}
    $Variables = @{}
    [string]$DataTypeString = ""
    foreach ($table in $Database.Tables)
    {
        #Get Table Name
        $Parent = $table.Name
        [int]$Count = 0
        foreach($column in $table.Columns)
        {

            $temp = $column |  Where-Object{$_.InPrimaryKey -eq $false} |
            SELECT Parent, 
                   Name, 
                   @{Name='DataType';Expression = 
                        {
                            [string]$DataType = $_.Properties['DataType'].Value 
                            [string]$Length = $_.Properties['Length'].Value 
                            if($DataType -Contains "nvarchar" -or $DataType -Contains "varchar")
                            {
                                $DataType = $DataType +"("+$Length+")"
                            }
                            write-output $DataType
                        }
                    },
                   @{Name='Variable';Expression = 
                        {
                            [string]$col = $_.name
                            $col = $col.replace(" ","")
                            write-output $col
                        }
                    }
           
            
            if(![string]::IsNullOrEmpty($temp.Name))
            {
                $Columns.Add($Count,$temp.Name)
                $DataTypeString = $temp.DataType
                $DataType.Add($Count,$DataTypeString.ToUpper())
                $Variables.Add($Count,$temp.Variable)
                $Count = $Count + 1
            }
            
        }
        
        $Consolidated = @{}
        $Consolidated = @{"Columns"=$Columns;"DataType"=$DataType;"Variables"=$Variables}
        
        $Columns = @{}
        $DataType = @{}
        $Variables = @{}
        
        #Add each columns, datatypes, variables to hash table
        $hash.Add($Parent,$Consolidated) 
    }
    #write-output $hash 
    return $hash 
}


function GetColumnsForUpdateForTable
{param($Database, [string]$TableName)
    $Col = @()
    $Table = New-Object ("Microsoft.SqlServer.Management.SMO.Table")
    $Table.Name = $TableName
    $table = $Database.Tables[$Table]
    foreach($column in $table.Columns)
    {
        $Columns = $column  | SELECT Parent,Name,DataType,InPrimaryKey
        $Col += @($Columns)
    }
    return $Col
}

function ExtractTableName
{param([string]$TableName)
    $SchemaPresent = $TableName.Contains("[dbo].")
    if($SchemaPresent)
    {
        $TableName = $TableName.substring(6);
        if($TableName.StartsWith("[") -and $TableName.EndsWith("]"))
        {
            $length = $TableName.length
            $TableName = $TableName.substring(1,($length-2));
        }
    }
    return $TableName
}

function GetSPName
{param([string]$TableName,[string]$Operation)
    
    [string]$SPName = "["+$TableName+"]"
    $SPName = $SPName.Replace("[", "[sp")
    $SPName = $SPName.Replace("]", $Operation+"]")
    $SPName = $SPName.Replace("[", "[dbo].[")
    return $SPName
    
}


function RemoveEndSpaces
{param([string]$String)
    
    if($String.EndsWith(",`r`n`t`t"))
    {
        $String = $String.Substring(0,($String.Length-5))
    }
    
    if($String.EndsWith(",`r`n`t`t`t"))
    {
        $String = $String.Substring(0,($String.Length-6))
    }
    return $String
}


function GetUpdateWhereClause
{param($Database)
    
    [string]$Parent = ""
    $WhereClause = @{}
    foreach ($table in $Database.Tables)
    {
        #Get Table Name
        $Parent = $table.Name

        foreach($column in $table.Columns)
        {

            $temp = $column |  Where-Object{$_.InPrimaryKey -eq $true} |
            SELECT Name
           
            if(![string]::IsNullOrEmpty($temp.Name))
            {
                [string]$PrimaryKey = $temp.Name
                [string]$PrimaryKeyNoSpaces = "@"+$PrimaryKey.replace(" ","")
                [string]$WhereClauseValue = "["+$PrimaryKey + "] = " +$PrimaryKeyNoSpaces
                $WhereClause.Add($Parent,$WhereClauseValue)
            }
        }
    }
    return $WhereClause
}


function GetPrimaryKeyVariables
{param($Database)
    
    $PrimaryKeys = @{}
    foreach ($table in $Database.Tables)
    {
        #Get Table Name
        $Parent = $table.Name

        foreach($column in $table.Columns)
        {

            $temp = $column |  Where-Object{$_.InPrimaryKey -eq $true} |
            SELECT Name,DataType
           
            if(![string]::IsNullOrEmpty($temp.Name))
            {
                $Data = @()
                [string]$DataType =$temp.DataType
                [string]$Name = $temp.Name
                $Data += "@"+$Name.replace(" ","")
                $Data +=$DataType.ToUpper()
                
                $PrimaryKeys.Add($Parent,$Data)
            }
        }
    }
    return $PrimaryKeys
}


function GenerateReadForTable
{param($Database, [string]$TableName)
    [string]$DatabaseName = $Database.Name
    
    #Get Table Name for Read SP
    $TrimmedTableName = ExtractTableName -TableName $TableName
    
    #Get Cols for Read SP
    $ColsRead = GetColumnsOfTableForRead -Database $Database -Table $TrimmedTableName
    
    #Get Primary Key and its datatype
    $PrimaryKey = $ColsRead | SELECT Name
    $PrimaryKeyName = $PrimaryKey.Name
    $PrimaryKeyNameNoSpaces = $PrimaryKeyName.replace(" ","")
    $Space = $PrimaryKeyName.IndexOf(" ")
    if($Space -gt 0)
    {
        $PrimaryKeyName = "["+$PrimaryKeyName+"]"
    }
    $PrimaryKeyDataType = $ColsRead | SELECT DataType
    $PrimaryKeyDataTypeName = $PrimaryKeyDataType.DataType.Name
    
    #Get SP Name
    [string]$TableNameTemp = $TableName
    [string]$SchemaName = ""
    [string]$SPName = ""
    $SchemaPresent = $TableNameTemp.Contains("[dbo].")
    if($SchemaPresent)
    {
        $SchemaName = $TableNameTemp.substring(0,5);
        $TableNameTemp = $TableNameTemp.substring(6);
    }
    $TableNameTemp = $TableNameTemp.Replace("[", "[sp")
    $TableNameTemp = $TableNameTemp.Replace("]", "Read]")
    $SPName = $SchemaName +"."+ $TableNameTemp
    
    #Date for logging
    $date = Get-Date –f "yyyy-MM-dd HH:mm:ss"
    
    #Build StoredProcedure
    [string]$Query = "
    /****** Object:  StoredProcedure {0}    Script Date: {5}******/
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'{0}') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE {0}(
		@{3} {2} = NULL
)

AS
BEGIN
	SET NOCOUNT ON;
	
	IF(@{3} IS NULL)
		SELECT * FROM {4} WHERE (Deleted IS NULL) OR (Deleted != 1);
	ELSE
		SELECT * FROM {4} WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND {1} = @{3};
	

END' 
END
GO"


    #Fill the StoredProcedure with values
    [string]$ReadWithValues = $Query -f $SPName, $PrimaryKeyName, $PrimaryKeyDataTypeName, $PrimaryKeyNameNoSpaces,$TableName, $date
    return $ReadWithValues
}


function GenerateRead
{param($Database)
    
    #Get All Columns as Hash Table to create SPReads
    $ReadCol = GetColumnsForRead -Database $Database
    [string]$Parent = ""
    [string]$PrimaryKey = ""
    [string]$PrimaryKeyDataType = ""
    [int]$row = 0
    [string]$FinalReadSQL = ""
    [string]$FinalDeleteSQL = ""
    
    #Build StoredProcedure for Read
    [string]$ReadQuery = "
/****** Object:  StoredProcedure {0}    Script Date: {5}******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'{0}') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
    CREATE PROCEDURE {0}(
    		@{3} {2} = NULL
    )

    AS
    BEGIN
    	SET NOCOUNT ON;
    	
    	IF(@{3} IS NULL)
    		SELECT * FROM [dbo].[{4}] WHERE (Deleted IS NULL) OR (Deleted != 1);
    	ELSE
    		SELECT * FROM [dbo].[{4}] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND {1} = @{3};
            
    END' 
END
GO


"
  
    
    for($row = 0; $row -lt $ReadCol.Count;$row ++)
    {
        $test = $ReadCol.Get_Item($row)
        
        #Getting values from each Hash table row
        $Parent = $test[0]
        $PrimaryKey = $test[1]
        $PrimaryKeyDataType = $test[2]
        $PrimaryKeyDataType = $PrimaryKeyDataType.ToUpper()
        
        #Stored Procedure Name for Read
        [string]$SPRead = GetSPName -TableName $Parent -Operation "Read"
                       
        
        #PrimaryKey for variables
        $PrimaryKeyNoSpaces = $PrimaryKey.replace(" ","")
        $Space = $PrimaryKey.IndexOf(" ")
        if($Space -gt 0)
        {
            $PrimaryKey = "["+$PrimaryKey+"]"
        }
        
        #Date for logging
        $date = Get-Date –f "yyyy-MM-dd HH:mm:ss"
    
        
        
        
        #Fill the StoredProcedure with values for Read
        [string]$ReadWithValues = $ReadQuery -f $SPRead, $PrimaryKey, $PrimaryKeyDataType, $PrimaryKeyNoSpaces,$Parent, $date
        
        #Building final scripts
        $FinalReadSQL = $FinalReadSQL  + $ReadWithValues
    }
    return $FinalReadSQL
}


function GenerateDelete
{param($Database)
    
    #Get All Columns as Hash Table to create SPReads
    $DeleteCol = GetColumnsForDelete -Database $Database
    [string]$Parent = ""
    [string]$PrimaryKey = ""
    [string]$PrimaryKeyDataType = ""
    [int]$row = 0
    [string]$FinalDeleteSQL = ""
    

    #Build StoredProcedure for Delete
    [string]$DeleteQuery = "
/****** Object:  StoredProcedure {0}    Script Date: {5} ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'{0}') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE {0}(
			@{3}   {2}
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[{4}] SET Deleted = 1 WHERE {1}  = @{3};

	END' 
END
GO


"

    
    for($row = 0; $row -lt $DeleteCol.Count;$row ++)
    {
        $test = $DeleteCol.Get_Item($row)
        
        #Getting values from each Hash table row
        $Parent = $test[0]
        $PrimaryKey = $test[1]
        $PrimaryKeyDataType = $test[2]
        $PrimaryKeyDataType = $PrimaryKeyDataType.ToUpper()
        
        
        #Stored Procedure Name for Delete
        [string]$SPDelete = GetSPName -TableName $Parent -Operation "Delete"
                
        
        #PrimaryKey for variables
        $PrimaryKeyNoSpaces = $PrimaryKey.replace(" ","")
        $Space = $PrimaryKey.IndexOf(" ")
        if($Space -gt 0)
        {
            $PrimaryKey = "["+$PrimaryKey+"]"
        }
        
        #Date for logging
        $date = Get-Date –f "yyyy-MM-dd HH:mm:ss"
    
        
        
        #Fill the StoredProcedure with values for Delete
        [string]$DeleteWithValues = $DeleteQuery -f $SPDelete, $PrimaryKey, $PrimaryKeyDataType, $PrimaryKeyNoSpaces,$Parent, $date
        
        #Building final scripts
        $FinalDeleteSQL = $FinalDeleteSQL + $DeleteWithValues
    }
    return $FinalDeleteSQL
}


function GenerateInsert
{param($Database)
    
    #Variables Declaration
    [hashtable]$InsertCol = @{}
    [string]$FinalInsertSQL = ""
    [string]$SPVariables = ""
    [string]$SPColumns = ""
    [string]$SPValues = ""
    
    #Get All Columns as Hash Table to create SPReads
    $InsertCol = GetColumnsForInsert -Database $Database
    
    
    #Build StoredProcedure for Insert
    [string]$InsertQuery = "/****** Object:  StoredProcedure {0}    Script Date: {5} ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'{0}') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE {0}(
		{2}
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO {1} 
	(
		{3}
	)
	VALUES 
	(
		{4}
	);

END' 
END
GO


"


    foreach($key in $($InsertCol.keys))
    {
        $TableDetails = $InsertCol.Get_Item($key)
        $TableColumns = $TableDetails["Columns"]
        $TableDataTypes = $TableDetails["DataType"]
        $TableVariables = $TableDetails["Variables"]
        
        
        for([int]$i = 1;$i -lt $TableColumns.Count;$i++)
        {
            $SPColumns = $SPColumns + "["+$TableColumns.Get_Item($i) +"],`r`n`t`t"
            $SPVariables = $SPVariables +"@"+ $TableVariables.Get_Item($i)+"`t`t"+$TableDataTypes.Get_Item($i)+",`r`n`t`t"
            $SPValues = $SPValues +"@"+ $TableVariables.Get_Item($i)+",`r`n`t`t"
        }
        
        $SPColumns = RemoveEndSpaces -String $SPColumns
        $SPVariables = RemoveEndSpaces -String $SPVariables
        $SPValues = RemoveEndSpaces -String $SPValues
        
        #Stored Procedure Name for Insert
        [string]$SPInsert = GetSPName -TableName $Key -Operation "Insert"
        
        
        #Date for logging
        $date = Get-Date –f "yyyy-MM-dd HH:mm:ss"
        
        #Fill the StoredProcedure with values for Delete
        [string]$InsertWithValues = $InsertQuery -f $SPInsert, $Key, $SPVariables, $SPColumns,$SPValues, $date
        
        #Building final scripts
        $FinalInsertSQL = $FinalInsertSQL + $InsertWithValues
        
        #Resetting table details so that we can use for another iteration
        $SPColumns = ""
        $SPVariables = ""
        $SPValues = ""
    }
    return $FinalInsertSQL
}



function GenerateUpdate
{param($Database)
    
    #Variables Declaration
    [hashtable]$UpdateCol = @{}
    [string]$FinalUpdateSQL = ""
    [string]$SPVariables = ""
    [string]$SPColumns = ""
    [string]$SPValues = ""
    
    #Get All Columns as Hash Table to create SPUpdate
    $UpdateCol = GetColumnsForUpdate -Database $Database
    
    
    #Build StoredProcedure for Update
    [string]$UpdateQuery = "/****** Object:  StoredProcedure {0}    Script Date: {5} ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'{0}') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE {0}(
		{2}
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE {1} 
		SET 
			{3}
		where 
			{4};

END' 
END
GO


"

    $WhereClause = GetUpdateWhereClause -Database $Database
    $PrimaryKeys = GetPrimaryKeyVariables -Database $Database

    foreach($key in $($UpdateCol.keys))
    {
        $TableDetails = $UpdateCol.Get_Item($key)
        $TableColumns = $TableDetails["Columns"]
        $TableDataTypes = $TableDetails["DataType"]
        $TableVariables = $TableDetails["Variables"]
        
        
        for([int]$i = 1;$i -lt $TableColumns.Count;$i++)
        {
            $SPVariables = $SPVariables +"@"+ $TableVariables.Get_Item($i)+"`t`t`t`t"+$TableDataTypes.Get_Item($i)+",`r`n`t`t"
            $SPValues = $SPValues  +"["+ $TableColumns.Get_Item($i)+"] = @"+ $TableVariables.Get_Item($i)+",`r`n`t`t`t"
        }
        
        $SPColumns = RemoveEndSpaces -String $SPColumns
        $SPVariables = RemoveEndSpaces -String $SPVariables
        $SPValues = RemoveEndSpaces -String $SPValues
        
        #Adding Primary Key in Stored Procedure Variables
        $PrimaryKeysData = $PrimaryKeys.Get_Item($key)
        
        
        $SPVariables = $PrimaryKeysData[0]+"`t`t"+$PrimaryKeysData[1] +",`r`n`t`t"+ $SPVariables
        
        #Stored Procedure Name for Insert
        [string]$SPUpdate = GetSPName -TableName $Key -Operation "Update"
        
        
        #Date for logging
        $date = Get-Date –f "yyyy-MM-dd HH:mm:ss"
        
        #Where Clause for Update Stored Procedure
        $WhereClauseString = $WhereClause.Get_Item($key)
        
        #Fill the StoredProcedure with values for Delete
        [string]$UpdateWithValues = $UpdateQuery -f $SPUpdate, $Key, $SPVariables, $SPValues,$WhereClauseString, $date
        
        #Building final scripts
        $FinalUpdateSQL = $FinalUpdateSQL + $UpdateWithValues
        
        #Resetting table details so that we can use for another iteration
        $SPColumns = ""
        $SPVariables = ""
        $SPValues = ""
    }
    return $FinalUpdateSQL
}


#$AllColumns = GetAllColumns -Database $dbName
#write-output $AllColumns

#$ReadCol = GetColumnsForRead -Database $dbName
#write-output $ReadCol

#$DeleteCol = GetColumnsForDelete -Database $dbName
#write-output $DeleteCol

#$InsertForTableCol = GetColumnsForInsertForTable -Database $dbName -Table "TblSavedSearch"
#write-output $InsertForTableCol

#GetPrimaryKeys -Database $dbName
#GetUpdateWhereClause -Database $dbName

#GetColumnsForInsert -Database $dbName
#write-output $InsertCol

#$UpdateColForTable = GetColumnsForUpdateForTable -Database $dbName -Table "TblEmployee"
#write-output $UpdateColForTable

#$SPRead = GenerateReadForTable -Database $dbName -Table "[dbo].[TblReportObjects]"
#write-output $SPRead

$Read = GenerateRead -Database $dbName
$Delete = GenerateDelete -Database $dbName
$Insert = GenerateInsert -Database $dbName
$Update = GenerateUpdate -Database $dbName

$Script = "--READ STORED PROCEDURES `r`n`r`n`r`n"+$Read +"`r`n`r`n`r`n --DELETE STORED PROCEDURES`r`n`r`n`r`n"+$Delete +"`r`n`r`n`r`n --INSERT STORED PROCEDURES`r`n`r`n`r`n"+$Insert+"`r`n`r`n`r`n --UPDATE STORED PROCEDURES`r`n`r`n`r`n"+$Update
#write-output $Script


[string]$date = Get-Date –f "yyyy-MM-dd HH:mm:ss"
$date = $date.replace(":","_")
$File = $Path + $dbName+"-Basic-CRUD-"+$date+".sql"
write-output $File

New-Item $File -type file -force -value $Script | out-null






