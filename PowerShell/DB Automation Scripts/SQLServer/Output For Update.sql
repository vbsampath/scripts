/****** Object:  StoredProcedure [dbo].[spTblSearchColumnsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchColumnsUpdate]') AND type in (N'P', N'PC
'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSearchColumnsUpdate](
		@SearchColumnId		BIGINT,
		@SearchColumnName				NVARCHAR(30),
		@SearchTable				NVARCHAR(30),
		@SearchColumnAlias				NVARCHAR(30),
		@SearchOperator				NVARCHAR(10),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblSearchColumns 
		SET 
			[Search Column Name] = @SearchColumnName,
			[Search Table] = @SearchTable,
			[Search Column Alias] = @SearchColumnAlias,
			[Search Operator] = @SearchOperator,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted
		where 
			[Search Column Id] = @SearchColumnId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblReportObjectsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportObjectsUpdate]') AND type in (N'P', N'PC
'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblReportObjectsUpdate](
		@ReportObjectId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT,
		@ExtraField				NVARCHAR(30),
		@ExtraFieldSP				NVARCHAR(30),
		@ReportStoredProcedure				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblReportObjects 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted,
			[Extra Field] = @ExtraField,
			[Extra Field SP] = @ExtraFieldSP,
			[Report Stored Procedure] = @ReportStoredProcedure
		where 
			[Report Object Id] = @ReportObjectId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartsUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartsUpdate](
		@PartId		BIGINT,
		@PartTypeName				NVARCHAR(50),
		@PartName				NVARCHAR(1000),
		@Barcode				NVARCHAR(30),
		@UPC				NVARCHAR(30),
		@SKU				NVARCHAR(50),
		@Manufacturer				NVARCHAR(30),
		@Spec				NVARCHAR(-1),
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Deleted				BIT,
		@PartCode				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblParts 
		SET 
			[Part Type Name] = @PartTypeName,
			[Part Name] = @PartName,
			[Barcode] = @Barcode,
			[UPC] = @UPC,
			[SKU] = @SKU,
			[Manufacturer] = @Manufacturer,
			[Spec] = @Spec,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Deleted] = @Deleted,
			[Part Code] = @PartCode
		where 
			[PartId] = @PartId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblReportColumnsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblReportColumnsUpdate]') AND type in (N'P', N'PC
'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblReportColumnsUpdate](
		@ReportColumnId		BIGINT,
		@ReportObjectName				NVARCHAR(30),
		@ReportColumnName				NVARCHAR(30),
		@ReportTable				NVARCHAR(30),
		@ReportColumnAlias				NVARCHAR(30),
		@ReportOperator				NVARCHAR(10),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT,
		@ExtraField				NVARCHAR(30),
		@ExtraFieldSP				NVARCHAR(30),
		@ReportStoredProcedure				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblReportColumns 
		SET 
			[Report Object Name] = @ReportObjectName,
			[Report Column Name] = @ReportColumnName,
			[Report Table] = @ReportTable,
			[Report Column Alias] = @ReportColumnAlias,
			[Report Operator] = @ReportOperator,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted,
			[Extra Field] = @ExtraField,
			[Extra Field SP] = @ExtraFieldSP,
			[Report Stored Procedure] = @ReportStoredProcedure
		where 
			[Report Column Id] = @ReportColumnId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartInventorySummaryUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartInventorySummaryUpdate]') AND type in (N'P
', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartInventorySummaryUpdate](
		@PartTypeName				NVARCHAR(50),
		@Barcode				NVARCHAR(30),
		@Manufacturer				NVARCHAR(30),
		@Spec				NVARCHAR(-1),
		@StockLevel				BIGINT,
		@Threshold				INT,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblPartInventorySummary 
		SET 
			[Part Type Name] = @PartTypeName,
			[Barcode] = @Barcode,
			[Manufacturer] = @Manufacturer,
			[Spec] = @Spec,
			[Stock Level] = @StockLevel,
			[Threshold] = @Threshold,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Deleted] = @Deleted
		where 
			;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartInventoryUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartInventoryUpdate]') AND type in (N'P', N'PC
'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartInventoryUpdate](
		@PartInventoryId		BIGINT,
		@PartTypeId				BIGINT,
		@PartTypeName				NVARCHAR(50),
		@SerialNumber				NVARCHAR(20),
		@AssetNumber				NVARCHAR(20),
		@PartStatusId				BIGINT,
		@PartStatus				NVARCHAR(20),
		@PartName				NVARCHAR(1000),
		@Barcode				NVARCHAR(30),
		@UPC				NVARCHAR(30),
		@SKU				NVARCHAR(50),
		@Manufacturer				NVARCHAR(30),
		@Spec				NVARCHAR(-1),
		@PartCode				NVARCHAR(30),
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblPartInventory 
		SET 
			[PartTypeId] = @PartTypeId,
			[Part Type Name] = @PartTypeName,
			[Serial Number] = @SerialNumber,
			[Asset Number] = @AssetNumber,
			[PartStatusId] = @PartStatusId,
			[Part Status] = @PartStatus,
			[Part Name] = @PartName,
			[Barcode] = @Barcode,
			[UPC] = @UPC,
			[SKU] = @SKU,
			[Manufacturer] = @Manufacturer,
			[Spec] = @Spec,
			[Part Code] = @PartCode,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Deleted] = @Deleted
		where 
			[PartInventoryId] = @PartInventoryId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblModificationTypeUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblModificationTypeUpdate]') AND type in (N'P', N
'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblModificationTypeUpdate](
		@ModificationCategoryId		BIGINT,
		@Code				NVARCHAR(250),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@InsertedBy				NVARCHAR(15),
		@ModifiedBy				NVARCHAR(15),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblModificationType 
		SET 
			[Code] = @Code,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[InsertedBy] = @InsertedBy,
			[ModifiedBy] = @ModifiedBy,
			[Deleted] = @Deleted
		where 
			[ModificationCategoryId] = @ModificationCategoryId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblAuditInformationUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAuditInformationUpdate]') AND type in (N'P', N
'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblAuditInformationUpdate](
		@AuditInformationId		BIGINT,
		@Comment				NVARCHAR(250),
		@PartInventoryId				BIGINT,
		@AuditType				NVARCHAR(20),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblAuditInformation 
		SET 
			[Comment] = @Comment,
			[PartInventoryId] = @PartInventoryId,
			[Audit Type] = @AuditType,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted
		where 
			[AuditInformationId] = @AuditInformationId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartCategoriesUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartCategoriesUpdate]') AND type in (N'P', N'P
C'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartCategoriesUpdate](
		@PartTypeId		BIGINT,
		@PartTypeCode				NVARCHAR(30),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblPartCategories 
		SET 
			[Part Type Code] = @PartTypeCode,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted
		where 
			[PartTypeId] = @PartTypeId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartTransactionTypesUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartTransactionTypesUpdate]') AND type in (N'P
', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartTransactionTypesUpdate](
		@PartTransactionTypeId		BIGINT,
		@TransactionTypeCode				NVARCHAR(30),
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblPartTransactionTypes 
		SET 
			[Transaction Type Code] = @TransactionTypeCode,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Deleted] = @Deleted
		where 
			[PartTransactionTypeId] = @PartTransactionTypeId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblStaticObjectsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblStaticObjectsUpdate]') AND type in (N'P', N'PC
'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblStaticObjectsUpdate](
		@StaticObjectId		BIGINT,
		@StaticObjectType				NVARCHAR(30),
		@StaticObjectTable				NVARCHAR(30),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT,
		@StaticObjectDisplayField				NVARCHAR(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblStaticObjects 
		SET 
			[Static Object Type] = @StaticObjectType,
			[Static Object Table] = @StaticObjectTable,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted,
			[Static Object Display Field] = @StaticObjectDisplayField
		where 
			[Static Object Id] = @StaticObjectId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartTransactionsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartTransactionsUpdate]') AND type in (N'P', N
'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartTransactionsUpdate](
		@PartTransactionId		BIGINT,
		@TransactionType				NVARCHAR(30),
		@BeforeXml				XML,
		@AfterXml				XML,
		@Comments				NVARCHAR(500),
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblPartTransactions 
		SET 
			[Transaction Type] = @TransactionType,
			[Before Xml] = @BeforeXml,
			[After Xml] = @AfterXml,
			[Comments] = @Comments,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Deleted] = @Deleted
		where 
			[PartTransactionId] = @PartTransactionId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblDisplayColumnsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDisplayColumnsUpdate]') AND type in (N'P', N'P
C'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblDisplayColumnsUpdate](
		@DisplayColumnId		BIGINT,
		@DisplayGroup				NVARCHAR(50),
		@DisplayColumn				NVARCHAR(50),
		@DisplayColumnTable				NVARCHAR(50),
		@StoredProcedure				NVARCHAR(50)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblDisplayColumns 
		SET 
			[Display Group] = @DisplayGroup,
			[Display Column] = @DisplayColumn,
			[Display Column Table] = @DisplayColumnTable,
			[StoredProcedure] = @StoredProcedure
		where 
			[Display Column Id] = @DisplayColumnId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblSearchObjectsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchObjectsUpdate]') AND type in (N'P', N'PC
'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSearchObjectsUpdate](
		@SearchObjectId		BIGINT,
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblSearchObjects 
		SET 
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted
		where 
			[Search Object Id] = @SearchObjectId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblThresholdLimitsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblThresholdLimitsUpdate]') AND type in (N'P', N'
PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblThresholdLimitsUpdate](
		@PartTypeName				NVARCHAR(50),
		@Threshold				BIGINT,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblThresholdLimits 
		SET 
			[Part Type Name] = @PartTypeName,
			[Threshold] = @Threshold,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Deleted] = @Deleted
		where 
			;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblSavedSearchUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSavedSearchUpdate]') AND type in (N'P', N'PC')
)
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSavedSearchUpdate](
		@SavedSearchId		BIGINT,
		@SearchObject				VARCHAR(50),
		@SearchColumn				VARCHAR(50),
		@SearchVariable				VARCHAR(50),
		@SearchDescription				VARCHAR(50),
		@SearchDateTime				DATETIME,
		@SearchQuery				VARCHAR(250),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblSavedSearch 
		SET 
			[Search Object] = @SearchObject,
			[Search Column] = @SearchColumn,
			[Search Variable] = @SearchVariable,
			[Search Description] = @SearchDescription,
			[Search DateTime] = @SearchDateTime,
			[Search Query] = @SearchQuery,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted
		where 
			[Saved Search Id] = @SavedSearchId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblEditableObjectsUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblEditableObjectsUpdate]') AND type in (N'P', N'
PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblEditableObjectsUpdate](
		@EditableObjectId		BIGINT,
		@TableName				NVARCHAR(30),
		@TableIDField				NVARCHAR(30),
		@SelectStoredProcedure				NVARCHAR(50),
		@InsertStoredProcedure				NVARCHAR(50),
		@UpdateStoredProcedure				NVARCHAR(50),
		@DeleteStoredProcedure				NVARCHAR(50),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@Deleted				NCHAR,
		@ExtraField				NCHAR,
		@ExtraFieldSP				NCHAR,
		@ReportStoredProcedure				NCHAR
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblEditableObjects 
		SET 
			[Table Name] = @TableName,
			[Table ID Field] = @TableIDField,
			[Select Stored Procedure] = @SelectStoredProcedure,
			[Insert Stored Procedure] = @InsertStoredProcedure,
			[Update Stored Procedure] = @UpdateStoredProcedure,
			[Delete Stored Procedure] = @DeleteStoredProcedure,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[Deleted] = @Deleted,
			[Extra Field] = @ExtraField,
			[Extra Field SP] = @ExtraFieldSP,
			[Report Stored Procedure] = @ReportStoredProcedure
		where 
			[EditableObjectId] = @EditableObjectId;

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartStatusTypesUpdate]    Script Date: 2012-03-14 19:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartStatusTypesUpdate]') AND type in (N'P', N'
PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartStatusTypesUpdate](
		@PartStatusId		BIGINT,
		@StatusTypeCode				NVARCHAR(30),
		@Inserted				NVARCHAR(15),
		@Modified				NVARCHAR(15),
		@InsertedDate				DATETIME,
		@ModifiedDate				DATETIME,
		@Deleted				BIT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE TblPartStatusTypes 
		SET 
			[Status Type Code] = @StatusTypeCode,
			[Inserted] = @Inserted,
			[Modified] = @Modified,
			[InsertedDate] = @InsertedDate,
			[ModifiedDate] = @ModifiedDate,
			[Deleted] = @Deleted
		where 
			[PartStatusId] = @PartStatusId;

END' 
END
GO


