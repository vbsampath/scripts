/****** Object:  StoredProcedure [dbo].[spTblAuditInformationUpdate]    Script Date: 02/20/2012 01:55:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAuditInformationUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblAuditInformationUpdate](
		@AuditInformationId bigint,
		@BeforeXml xml,
		@AfterXml xml,
		@Comment nvarchar(250),
		@PartInventoryId bigint,
		@PartId bigint,
		@AuditType nvarchar(25),
		@InsertedDate datetime,
		@ModifiedDate datetime,
		@InsertedBy nvarchar(15),
		@ModifiedBy nvarchar(15),
		@Deleted bit,
		@ModificationCategoryId BIGINT,
		@ModificationDescription NVARCHAR(25),
		@ModificationDetailedDescription NVARCHAR(2000)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dbo].[TblAuditInformation] 
		SET 
			[Before Xml] = @BeforeXml,
			[After Xml] = @AfterXml,
			Comment = @Comment,
			PartInventoryId = @PartInventoryId,
			PartId = @PartId,
			[Audit Type] = @AuditType,
			InsertedDate = @InsertedDate,
			ModifiedDate = @ModifiedDate,
			Inserted = @InsertedBy,
			Modified = @ModifiedBy,
			Deleted = @Deleted,
			ModificationCategoryId = @ModificationCategoryId,
			ModificationDescription = @ModificationDescription,
			ModificationDetailedDescription = @ModificationDetailedDescription 
		where 
			AuditInformationId = @AuditInformationId;

END

' 
END
GO

