/****** Object:  StoredProcedure [dbo].[spTblSearchColumnsInsert]    Script Dat
e: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblSearchColumnsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSearchColumnsInsert](
		@SearchColumnName		nvarchar(30),
		@SearchTable		nvarchar(30),
		@SearchColumnAlias		nvarchar(30),
		@SearchOperator		nvarchar(10),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblSearchColumns 
	(
		[Search Column Name],
		[Search Table],
		[Search Column Alias],
		[Search Operator],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted]
	)
	VALUES 
	(
		@SearchColumnName,
		@SearchTable,
		@SearchColumnAlias,
		@SearchOperator,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblReportObjectsInsert]    Script Dat
e: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblReportObjectsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblReportObjectsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit,
		@ExtraField		nvarchar(30),
		@ExtraFieldSP		nvarchar(30),
		@ReportStoredProcedure		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblReportObjects 
	(
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted],
		[Extra Field],
		[Extra Field SP],
		[Report Stored Procedure]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted,
		@ExtraField,
		@ExtraFieldSP,
		@ReportStoredProcedure
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartsInsert]    Script Date: 2012-
03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblPartsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartsInsert](
		@PartTypeName		nvarchar(50),
		@PartName		nvarchar(1000),
		@Barcode		nvarchar(30),
		@UPC		nvarchar(30),
		@SKU		nvarchar(50),
		@Manufacturer		nvarchar(30),
		@Spec		nvarchar(-1),
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Deleted		bit,
		@PartCode		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblParts 
	(
		[Part Type Name],
		[Part Name],
		[Barcode],
		[UPC],
		[SKU],
		[Manufacturer],
		[Spec],
		[Inserted],
		[Modified],
		[InsertedDate],
		[ModifiedDate],
		[Deleted],
		[Part Code]
	)
	VALUES 
	(
		@PartTypeName,
		@PartName,
		@Barcode,
		@UPC,
		@SKU,
		@Manufacturer,
		@Spec,
		@Inserted,
		@Modified,
		@InsertedDate,
		@ModifiedDate,
		@Deleted,
		@PartCode
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblReportColumnsInsert]    Script Dat
e: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblReportColumnsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblReportColumnsInsert](
		@ReportObjectName		nvarchar(30),
		@ReportColumnName		nvarchar(30),
		@ReportTable		nvarchar(30),
		@ReportColumnAlias		nvarchar(30),
		@ReportOperator		nvarchar(10),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit,
		@ExtraField		nvarchar(30),
		@ExtraFieldSP		nvarchar(30),
		@ReportStoredProcedure		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblReportColumns 
	(
		[Report Object Name],
		[Report Column Name],
		[Report Table],
		[Report Column Alias],
		[Report Operator],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted],
		[Extra Field],
		[Extra Field SP],
		[Report Stored Procedure]
	)
	VALUES 
	(
		@ReportObjectName,
		@ReportColumnName,
		@ReportTable,
		@ReportColumnAlias,
		@ReportOperator,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted,
		@ExtraField,
		@ExtraFieldSP,
		@ReportStoredProcedure
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartInventorySummaryInsert]    Scr
ipt Date: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblPartInventorySummaryInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartInventorySummaryInsert](
		@PartTypeName		nvarchar(50),
		@Barcode		nvarchar(30),
		@Manufacturer		nvarchar(30),
		@Spec		nvarchar(-1),
		@StockLevel		bigint,
		@Threshold		int,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblPartInventorySummary 
	(
		[Part Type Name],
		[Barcode],
		[Manufacturer],
		[Spec],
		[Stock Level],
		[Threshold],
		[Inserted],
		[Modified],
		[InsertedDate],
		[ModifiedDate],
		[Deleted]
	)
	VALUES 
	(
		@PartTypeName,
		@Barcode,
		@Manufacturer,
		@Spec,
		@StockLevel,
		@Threshold,
		@Inserted,
		@Modified,
		@InsertedDate,
		@ModifiedDate,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartInventoryInsert]    Script Dat
e: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblPartInventoryInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartInventoryInsert](
		@PartTypeId		bigint,
		@PartTypeName		nvarchar(50),
		@SerialNumber		nvarchar(20),
		@AssetNumber		nvarchar(20),
		@PartStatusId		bigint,
		@PartStatus		nvarchar(20),
		@PartName		nvarchar(1000),
		@Barcode		nvarchar(30),
		@UPC		nvarchar(30),
		@SKU		nvarchar(50),
		@Manufacturer		nvarchar(30),
		@Spec		nvarchar(-1),
		@PartCode		nvarchar(30),
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblPartInventory 
	(
		[PartTypeId],
		[Part Type Name],
		[Serial Number],
		[Asset Number],
		[PartStatusId],
		[Part Status],
		[Part Name],
		[Barcode],
		[UPC],
		[SKU],
		[Manufacturer],
		[Spec],
		[Part Code],
		[Inserted],
		[Modified],
		[InsertedDate],
		[ModifiedDate],
		[Deleted]
	)
	VALUES 
	(
		@PartTypeId,
		@PartTypeName,
		@SerialNumber,
		@AssetNumber,
		@PartStatusId,
		@PartStatus,
		@PartName,
		@Barcode,
		@UPC,
		@SKU,
		@Manufacturer,
		@Spec,
		@PartCode,
		@Inserted,
		@Modified,
		@InsertedDate,
		@ModifiedDate,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblModificationTypeInsert]    Script 
Date: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblModificationTypeInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblModificationTypeInsert](
		@Code		nvarchar(250),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@InsertedBy		nvarchar(15),
		@ModifiedBy		nvarchar(15),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblModificationType 
	(
		[Code],
		[InsertedDate],
		[ModifiedDate],
		[InsertedBy],
		[ModifiedBy],
		[Deleted]
	)
	VALUES 
	(
		@Code,
		@InsertedDate,
		@ModifiedDate,
		@InsertedBy,
		@ModifiedBy,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblAuditInformationInsert]    Script 
Date: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblAuditInformationInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblAuditInformationInsert](
		@Comment		nvarchar(250),
		@PartInventoryId		bigint,
		@AuditType		nvarchar(20),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblAuditInformation 
	(
		[Comment],
		[PartInventoryId],
		[Audit Type],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted]
	)
	VALUES 
	(
		@Comment,
		@PartInventoryId,
		@AuditType,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartCategoriesInsert]    Script Da
te: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblPartCategoriesInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartCategoriesInsert](
		@PartTypeCode		nvarchar(30),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblPartCategories 
	(
		[Part Type Code],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted]
	)
	VALUES 
	(
		@PartTypeCode,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartTransactionTypesInsert]    Scr
ipt Date: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblPartTransactionTypesInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartTransactionTypesInsert](
		@TransactionTypeCode		nvarchar(30),
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblPartTransactionTypes 
	(
		[Transaction Type Code],
		[Inserted],
		[Modified],
		[InsertedDate],
		[ModifiedDate],
		[Deleted]
	)
	VALUES 
	(
		@TransactionTypeCode,
		@Inserted,
		@Modified,
		@InsertedDate,
		@ModifiedDate,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblStaticObjectsInsert]    Script Dat
e: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblStaticObjectsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblStaticObjectsInsert](
		@StaticObjectType		nvarchar(30),
		@StaticObjectTable		nvarchar(30),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit,
		@StaticObjectDisplayField		nvarchar(30)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblStaticObjects 
	(
		[Static Object Type],
		[Static Object Table],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted],
		[Static Object Display Field]
	)
	VALUES 
	(
		@StaticObjectType,
		@StaticObjectTable,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted,
		@StaticObjectDisplayField
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartTransactionsInsert]    Script 
Date: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblPartTransactionsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartTransactionsInsert](
		@TransactionType		nvarchar(30),
		@BeforeXml		xml,
		@AfterXml		xml,
		@Comments		nvarchar(500),
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblPartTransactions 
	(
		[Transaction Type],
		[Before Xml],
		[After Xml],
		[Comments],
		[Inserted],
		[Modified],
		[InsertedDate],
		[ModifiedDate],
		[Deleted]
	)
	VALUES 
	(
		@TransactionType,
		@BeforeXml,
		@AfterXml,
		@Comments,
		@Inserted,
		@Modified,
		@InsertedDate,
		@ModifiedDate,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblDisplayColumnsInsert]    Script Da
te: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblDisplayColumnsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblDisplayColumnsInsert](
		@DisplayGroup		nvarchar(50),
		@DisplayColumn		nvarchar(50),
		@DisplayColumnTable		nvarchar(50),
		@StoredProcedure		nvarchar(50)
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblDisplayColumns 
	(
		[Display Group],
		[Display Column],
		[Display Column Table],
		[StoredProcedure]
	)
	VALUES 
	(
		@DisplayGroup,
		@DisplayColumn,
		@DisplayColumnTable,
		@StoredProcedure
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblSearchObjectsInsert]    Script Dat
e: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblSearchObjectsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSearchObjectsInsert](
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblSearchObjects 
	(
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted]
	)
	VALUES 
	(
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblThresholdLimitsInsert]    Script D
ate: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblThresholdLimitsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblThresholdLimitsInsert](
		@PartTypeName		nvarchar(50),
		@Threshold		bigint,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblThresholdLimits 
	(
		[Part Type Name],
		[Threshold],
		[Inserted],
		[Modified],
		[InsertedDate],
		[ModifiedDate],
		[Deleted]
	)
	VALUES 
	(
		@PartTypeName,
		@Threshold,
		@Inserted,
		@Modified,
		@InsertedDate,
		@ModifiedDate,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblSavedSearchInsert]    Script Date:
 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblSavedSearchInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblSavedSearchInsert](
		@SearchObject		varchar(50),
		@SearchColumn		varchar(50),
		@SearchVariable		varchar(50),
		@SearchDescription		varchar(50),
		@SearchDateTime		datetime,
		@SearchQuery		varchar(250),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblSavedSearch 
	(
		[Search Object],
		[Search Column],
		[Search Variable],
		[Search Description],
		[Search DateTime],
		[Search Query],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted]
	)
	VALUES 
	(
		@SearchObject,
		@SearchColumn,
		@SearchVariable,
		@SearchDescription,
		@SearchDateTime,
		@SearchQuery,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblEditableObjectsInsert]    Script D
ate: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblEditableObjectsInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblEditableObjectsInsert](
		@TableName		nvarchar(30),
		@TableIDField		nvarchar(30),
		@SelectStoredProcedure		nvarchar(50),
		@InsertStoredProcedure		nvarchar(50),
		@UpdateStoredProcedure		nvarchar(50),
		@DeleteStoredProcedure		nvarchar(50),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@Deleted		nchar,
		@ExtraField		nchar,
		@ExtraFieldSP		nchar,
		@ReportStoredProcedure		nchar
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblEditableObjects 
	(
		[Table Name],
		[Table ID Field],
		[Select Stored Procedure],
		[Insert Stored Procedure],
		[Update Stored Procedure],
		[Delete Stored Procedure],
		[InsertedDate],
		[ModifiedDate],
		[Inserted],
		[Modified],
		[Deleted],
		[Extra Field],
		[Extra Field SP],
		[Report Stored Procedure]
	)
	VALUES 
	(
		@TableName,
		@TableIDField,
		@SelectStoredProcedure,
		@InsertStoredProcedure,
		@UpdateStoredProcedure,
		@DeleteStoredProcedure,
		@InsertedDate,
		@ModifiedDate,
		@Inserted,
		@Modified,
		@Deleted,
		@ExtraField,
		@ExtraFieldSP,
		@ReportStoredProcedure
	);

END' 
END
GO


/****** Object:  StoredProcedure [dbo].[spTblPartStatusTypesInsert]    Script D
ate: 2012-03-14 17:55:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[s
pTblPartStatusTypesInsert]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spTblPartStatusTypesInsert](
		@StatusTypeCode		nvarchar(30),
		@Inserted		nvarchar(15),
		@Modified		nvarchar(15),
		@InsertedDate		datetime,
		@ModifiedDate		datetime,
		@Deleted		bit
)

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO TblPartStatusTypes 
	(
		[Status Type Code],
		[Inserted],
		[Modified],
		[InsertedDate],
		[ModifiedDate],
		[Deleted]
	)
	VALUES 
	(
		@StatusTypeCode,
		@Inserted,
		@Modified,
		@InsertedDate,
		@ModifiedDate,
		@Deleted
	);

END' 
END
GO
