/****** Object:  StoredProcedure [dbo].[spTblDisplayColumnsDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblDisplayColumnsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblDisplayColumnsDelete](
			@DisplayColumnId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblDisplayColumns] SET Deleted = 1 WHERE DisplayColumnId  = @DisplayColumnId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblEditableObjectsDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblEditableObjectsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblEditableObjectsDelete](
			@EditableObjectId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblEditableObjects] SET Deleted = 1 WHERE EditableObjectId  = @EditableObjectId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartInventoryDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartInventoryDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblPartInventoryDelete](
			@PartInventoryId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblPartInventory] SET Deleted = 1 WHERE PartInventoryId  = @PartInventoryId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartsDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblPartsDelete](
			@PartId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblParts] SET Deleted = 1 WHERE PartId  = @PartId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartStatusTypesDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartStatusTypesDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblPartStatusTypesDelete](
			@PartStatusTypeId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblPartStatusTypes] SET Deleted = 1 WHERE PartStatusTypeId  = @PartStatusTypeId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartsUsageDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartsUsageDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblPartsUsageDelete](
			@PartsUsageId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblPartsUsage] SET Deleted = 1 WHERE PartsUsageId  = @PartsUsageId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartTransactionsDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartTransactionsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblPartTransactionsDelete](
			@PartTransactionId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblPartTransactions] SET Deleted = 1 WHERE PartTransactionId  = @PartTransactionId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartTransactionTypesDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartTransactionTypesDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblPartTransactionTypesDelete](
			@PartTransactionTypeId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblPartTransactionTypes] SET Deleted = 1 WHERE PartTransactionTypeId  = @PartTransactionTypeId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblPartTypesDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblPartTypesDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblPartTypesDelete](
			@PartTypeId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblPartTypes] SET Deleted = 1 WHERE PartTypeId  = @PartTypeId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSavedSearchDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSavedSearchDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblSavedSearchDelete](
			@SavedSearchId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblSavedSearch] SET Deleted = 1 WHERE [Saved Search Id]  = @SavedSearchId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSearchColumnsDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchColumnsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblSearchColumnsDelete](
			@SearchColumnId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblSearchColumns] SET Deleted = 1 WHERE SearchColumnId  = @SearchColumnId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblSearchObjectsDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblSearchObjectsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblSearchObjectsDelete](
			@SearchObjectId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblSearchObjects] SET Deleted = 1 WHERE SearchObjectId  = @SearchObjectId;

	END' 
END
GO



/****** Object:  StoredProcedure [dbo].[spTblStaticObjectsDelete]    Script Date: 2012-03-12 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblStaticObjectsDelete]') AND TYPE IN (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
	CREATE PROCEDURE [dbo].[spTblStaticObjectsDelete](
			@StaticObjectId   BIGINT
	)

	AS
	BEGIN

		SET NOCOUNT ON;	
		UPDATE [dbo].[TblStaticObjects] SET Deleted = 1 WHERE StaticObjectId  = @StaticObjectId;

	END' 
END
GO

