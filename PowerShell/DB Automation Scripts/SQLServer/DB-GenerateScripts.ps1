#Loading required assemblies
[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO")

#If arguments are insufficient
if($args.count -ne 4)
{
	write-output "Usage:"
	write-output 'Syntax : "ServerName" "Database name" "Path" "Static data tables text file path" '
	write-output 'Example : "SQLPOC" "QCCDB" "C:\DbScripts" "C:\StaticDataTables.txt"'
	write-output 'Additional Information : This script generates database scripts for 1)Schema 2)Schema and Data 3)Schema and static data 4)Stored procedures 5)Views 6)Table Triggers 7)Database triggers'
};


if($args.count -eq 4)
{
	#Setting up arguments to variables
	$Server = $args[0];
	$dbName = $args[1];
	$path = $args[2];
	$StaticDataTables = $args[3];

	#Declaring variables
	$s = new-object Microsoft.SqlServer.Management.Smo.Server ($Server)
	$db = $s.Databases[$dbName]
	$continue = $True;
	$StaticDataPresent = $False;

	#Validating parameters
	if($Server -eq '')
	{	write-output "Server name cannot be empty. Please provide a valid name..."
		$continue = $False;
	}
	if($dbName -eq '')
	{
		write-output "Database name cannot be empty. Please provide a valid name..."
		$continue = $False;
	}
	if($path -eq '')
	{
		$path = 'C:\'
	}
	# Create directory if does not exist
	if((Test-Path -path $path) -ne $True)
	{
		New-Item $path -type directory | Out-Null
		$path = $path + '\'
		write-output "Folder created $path"
	}
	if($StaticDataTables -eq '')
	{
		write-output "StaticDataTables path is empty. So staticdata and tables script will be almost same..."
		$StaticDataPresent = $False;
	}
	if($StaticDataTables -ne '')
	{
		$StaticDataPresent = $True;
	}
	if($continue)
	{
		$options = new-object Microsoft.SqlServer.Management.Smo.ScriptingOptions

		#Setting up various scripting options
		$options.ScriptData = $true;
		$options.ScriptDrops = $false;
		$options.EnforceScriptingOptions = $true;
		$options.ScriptSchema = $true;
		$options.IncludeHeaders = $true;
		$options.AppendToFile = $true;
		$options.Indexes = $true;
		$options.WithDependencies = $false;
		$options.IncludeIfNotExists = $true;
		$options.NoCollation = $true;
	
	
	
		#-------------------------------------------------------------

		$filePath = $path + $db.Name + "_SchemaAndData.SQL"

		New-Item  $filePath -type file -force | Out-Null

		$options.FileName = $filePath

		# for Tables with Schema and Data
		foreach ($tbl in $db.Tables)            
		{
			$tbl.EnumScript($options) | Out-Null;
		}
		write-output "$filePath done"

		#-------------------------------------------------------------	

		$filePath = $path + $db.Name + "_Schema.SQL"

		New-Item  $filePath -type file -force | Out-Null

		$options.FileName = $filePath
		$options.ScriptData = $false;

		#for tables with only Schema
		foreach ($tbl in $db.Tables)            
		{
			$tbl.EnumScript($options) | Out-Null;
		}
		write-output "$filePath done"
	
		#-------------------------------------------------------------

		$filePath = $path + $db.Name + "_StaticData.SQL"

		New-Item  $filePath -type file -force | Out-Null

		$options.FileName = $filePath

		#Scripts tables with static data only if static data tables text file is passed
		if($StaticDataPresent)
		{
			$a = Get-Content $StaticDataTables
			$arr = $a -Split " "
		
			foreach ($tbl in $db.Tables)
			{
				if($arr -Contains $tbl.Name)
				{
					$tbl.EnumScript($options) | Out-Null;
				}
				else
				{
					$options.ScriptData = $False;
					$tbl.EnumScript($options) | Out-Null;
					$options.ScriptData = $True;
				}
			}
		}
		
		#Scripts tables with schema
		else
		{
			foreach ($tbl in $db.Tables)
			{
				$tbl.EnumScript($options) | Out-Null;
			}
		}
		write-output "$filePath done"

		#-------------------------------------------------------------

		$filePath = $path + $db.Name + "_StoredProcedures.SQL"

		New-Item  $filePath -type file -force | Out-Null

		$options.FileName = $filePath
		$options.ScriptData = $False;
		#for Stored Procedures
		foreach ($sp in $db.StoredProcedures)            
		{
			if($sp.IsSystemObject -eq $False)
			{
				$sp.Script($options) | Out-Null;
			}
		}
		write-output "$filePath done"
		
		#-------------------------------------------------------------

		$filePath = $path + $db.Name + "_Views.SQL"

		New-Item  $filePath -type file -force | Out-Null

		$options.FileName = $filePath

		#for Stored Procedures
		foreach ($v in $db.Views)            
		{
			if($v.IsSystemObject -eq $false)
			{
				$v.Script($options) | Out-Null;
			}
		} 
		write-output "$filePath done"


		#-------------------------------------------------------------

		$filePath = $path + $db.Name + "_TableTriggers.SQL"

		New-Item  $filePath -type file -force | Out-Null

		$options.FileName = $filePath

		#for Stored Procedures
		foreach ($tbl in $db.Tables)            
		{
			foreach($t in $tbl.Triggers)
			{
				$t.Script($options) | Out-Null;
			} 
		}
		write-output "$filePath done"


		#-------------------------------------------------------------

		$filePath = $path + $db.Name + "_DatabaseTriggers.SQL"

		New-Item  $filePath -type file -force | Out-Null

		$options.FileName = $filePath

		#for Database Triggers
		foreach ($t in $db.Triggers)            
		{
			$t.Script($options) | Out-Null;
		}
		write-output "$filePath done"
	}
};
