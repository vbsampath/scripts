#If arguments are insufficient
if($args.count -ne 4)
{
	write-output "Usage:"
	write-output 'Syntax : "ServerName" "Database name" "Flag" "Database Scripts path" '
	write-output 'Example : "SQLPOC" "QCCDB" "SchemaOnly" "C:\dbscripts\"'
	write-output 'Additional Information : This script generates database from previously created scripts'
};


if($args.count -eq 4)
{

		
	#Setting up arguments to variables
	$Server = $args[0]
	$dbName = $args[1] 
	$option = $args[2]
	$path = $args[3]
	
	#Declaring variables
	$continue = $True;
	$optionMentioned = $True;
	
	#Validating arguments
	if($Server -eq '')
	{	
		write-output "Server name cannot be empty. Please provide a valid name..."
		$continue = $False;
	}
	elseif($dbName -eq '')
	{
		write-output "Database name cannot be empty. Please provide a valid name..."
		$continue = $False;
	}
	elseif($path -eq '')
	{
		write-output "Cannot create $dbName without its scripts. Please provide a valid path..."
		$continue = $False;
	}
	elseif((Test-Path -path $path) -ne $True)
	{
		write-output "Provided directory $path is not a valid path. Please provide a valid path..."
		$continue = $False;
	}
	elseif($option -eq '')
	{
		write-output "No option is given. Continuing with creating $dbName with schema and static tables..."
		$optionMentioned = $False;
	}
	#Making the path a correct directory
	elseif(!$path.EndsWith("\"))
	{
		$path = $path + "\"
	}
	
	
	
	
	If($continue)
	{
		#Adding dependencies for sqlcmd to work
		Write-Host "Adding Snapins..." -ForegroundColor gray
		Add-PSSnapin -Name sqlserverprovidersnapin100 -ErrorAction SilentlyCOntinue -ErrorVariable err
		if ($err){
			Write-Host "sqlserverprovidersnapin100 snapin already exists!!! Proceding to next snapin" -ForegroundColor green
		}else{
			Write-Host "sqlserverprovidersnapin100 Snapin installed" -ForegroundColor green
		}


		Add-PSSnapin -Name sqlservercmdletsnapin100 -ErrorAction SilentlyCOntinue -ErrorVariable err
		if ($err){
			Write-Host "sqlservercmdletsnapin100 snapin already exists!!! Proceding to next snapin" -ForegroundColor green
		}else{
			Write-Host "sqlservercmdletsnapin100 Snapin installed" -ForegroundColor green
		}
		
		
		#Removing database
		Write-Host "Removing Database $db if Any...."  -ForegroundColor gray
		Start-Job -Name 'RemoveDB' -FilePath 'C:\Users\Administrator\Documents\DB Automation Scripts\Processed\DB-RemoveDatabase.ps1' -ArgumentList $Server,$dbName | out-null
		Wait-Job -Name 'RemoveDB' | out-null
		Remove-Job -Name 'RemoveDB' | out-null

		#Creating database
		Write-Host "Creating Database $db..."  -ForegroundColor gray
		Start-Job -Name 'CreateDB' -FilePath 'C:\Users\Administrator\Documents\DB Automation Scripts\Processed\DB-CreateDatabase.ps1' -ArgumentList $Server,$dbName | out-null
		Wait-Job -Name 'CreateDB' | out-null
		Remove-Job -Name 'CreateDB' | out-null



		#---------------------------------------------------------------------------
			
		Write-Host "Creating Tables with Data...."  -ForegroundColor gray
		
		If($optionMentioned)
		{
			If($option -eq 'Schemaonly')
			{
				#$na = $src + "*_Schema.sql"
				$na = $path + "*_StaticData.sql"
			}

			If($option -eq 'SchemaAndData')
			{
				$na = $path + "*_SchemaAndData.sql"
			}
		}
		Else
		{
			#Setting default options when not mentioned
			
			$option = 'Schemaonly'
			If($option -eq 'Schemaonly')
			{
				#$na = $src + "*_Schema.sql"
				$na = $path + "*_StaticData.sql"
			}
		}
		

		$a = Get-ChildItem -Path $na
		write-output $na
		$actualFile = $path + $a.name
		Invoke-Sqlcmd -Database $dbName -InputFile $actualFile -ErrorAction SilentlyContinue

		Write-Host "Completed Tables Creation..."   -ForegroundColor green


		#---------------------------------------------------------------------------


		Write-Host "Creating Stored Procedures...."  -ForegroundColor gray

		$na = $path + "*_StoredProcedures.sql"
		$a = Get-ChildItem -Path $na

		$actualFile = $path + $a.name
		Invoke-Sqlcmd -Database $dbName -InputFile $actualFile -ErrorAction SilentlyContinue

		Write-Host "Completed Stored Procedures Creation..."   -ForegroundColor green


		#---------------------------------------------------------------------------

		Write-Host "Creating Views...."  -ForegroundColor gray

		$na = $path + "*_Views.sql"
		$a = Get-ChildItem -Path $na

		$actualFile = $path + $a.name
		Invoke-Sqlcmd -Database $dbName -InputFile $actualFile -ErrorAction SilentlyContinue

		Write-Host "Completed Views Creation..."   -ForegroundColor green


		#---------------------------------------------------------------------------


		Write-Host "Creating Table Triggers...."  -ForegroundColor gray

		$na = $path + "*_TableTriggers.sql"
		$a = Get-ChildItem -Path $na

		$actualFile = $path + $a.name
		Invoke-Sqlcmd -Database dbName -InputFile $actualFile -ErrorAction SilentlyContinue

		Write-Host "Completed Table Triggers Creation..."   -ForegroundColor green


		#---------------------------------------------------------------------------


		Write-Host "Creating Database Triggers...."  -ForegroundColor gray

		$na = $path + "*_DatabaseTriggers.sql"
		$a = Get-ChildItem -Path $na

		$actualFile = $path + $a.name
		Invoke-Sqlcmd -Database $dbName -InputFile $actualFile -ErrorAction SilentlyContinue

		Write-Host "Completed Database Triggers Creation..."   -ForegroundColor green
	}
};
