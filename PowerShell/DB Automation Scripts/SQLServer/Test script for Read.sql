/****** Object:  StoredProcedure [dbo].[spTblAuditInformationRead]    Script Date: 01/26/2012 00:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTblAuditInformationRead]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spTblAuditInformationRead](
		@AuditInformationId BIGINT = NULL
)

AS
BEGIN
	SET NOCOUNT ON;
	
	IF(@AuditInformationId IS NULL)
		SELECT * FROM [dbo].[TblAuditInformation] WHERE (Deleted IS NULL) OR (Deleted != 1);
	ELSE
		SELECT * FROM [dbo].[TblAuditInformation] WHERE ((Deleted IS NULL) OR (Deleted != 1)) AND AuditInformationId = @AuditInformationId;
	

END' 
END
GO
