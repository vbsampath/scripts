Param (
	#[Parameter(Mandatory=$true,HelpMessage="Server IP with its Port.")] [ValidateNotNullOrEmpty()]  
    [Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string] $Server = $(Throw New-Object System.ArgumentException "-Server must be set to the name of a database server IP with its port","Server"),
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string] $userName = $(Throw New-Object System.ArgumentException "-userName must be set to the name of a database user","userName"),
	[Parameter(Mandatory=$false)] [string] $password = $(Throw New-Object System.ArgumentException "-password it is optional depending upon database config","password"),
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string] $dbName = $(Throw New-Object System.ArgumentException "-dbName must be set to the name of a database server","dbName"),
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string] $path = $(Throw New-Object System.ArgumentException "-path must be set to the destination path of scripts","path"),
	[Parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [string] $StaticDataTables = $(Throw New-Object System.ArgumentException "-StaticDataTables must be set to the path of StaticTables.txt file","StaticDataTables")
)

function Get-Help
{
	Write-Host "Usage:`n"
	Write-Host 'Syntax : "DatabaseServerName:DatabasePort" "Username" "Password" "Database" "Destination Directory"  "Static data tables text file path" "Without Password -- $true or $false"'"`n"
	Write-Host 'Example : "192.168.0.190:3306" "root" "123" "QCCDB" "C:\DbScripts" "C:\StaticDataTables.txt" $false'"`n"
	Write-Host 'Additional Information : 
	This script generates database scripts for 
	1))Schema 
	2)Schema and Data 
	3)Stored procedures 
	4)Schema with Static tables 
	5)Triggers
	
This script has got limitations such as it fetches mysqldump.exe from the local machine from which it is running.
So please make sure MySQL tools are installed. (atleast mysqldump.exe)
	
	'
}



#Variables
$ServerArray = $Server.Split(":");
$Server = $ServerArray[0];
$Port = $ServerArray[1];
$serverToCheckMySQLBinary = $env:computername
$Applicationfound = $false
$ExecutablesFound = $false
$SearchInstallations = @("MySQL Server","MySQL Workbench")
$AlternativeInstallationLocation = "C:\xampp\"
[Array]$searchExecutable = ('mysqldump.exe')
$curr_dir = [IO.Directory]::GetCurrentDirectory();
$log = [System.IO.Path]::GetTempFileName() | Rename-Item -NewName { $_ -replace 'tmp$', 'txt' } �PassThru
$filename = ''
$fullpath = ''
$CommonOptions = ''
$TableSpecificOptions = ''
$OperationSpecificOptions = ''



#----------------------------------------------------------------------------
#Functions
#----------------------------------------------------------------------------
function CreateDataBaseDirectory
{param(
	[string]$path,
	[string]$dbName
)
	$path = $path + '\' + $dbName #adding dbName folder for generated scripts
	if((Test-Path -path $path) -ne $True)
	{
		New-Item $path -type directory | Out-Null
		Write-Host "Folder created $path"
	}
	$path = $path + '\'
	return $path
}

function GenerateSchemaAndData
{param(
	[string]$executable,
	[string]$path, 
	[string]$CommonOptions,
	[string]$TableSpecificOptions,
	[string]$userSpecificOptions,
	[string]$dbName
	)
	$filename = "SchemaAndData.sql"
	$fullpath = $path + $filename
	$OperationSpecificOptions = $CommonOptions + $TableSpecificOptions + "--skip-triggers"
	$options = $userSpecificOptions + $OperationSpecificOptions
	$command = "$executable  $options $dbName > $fullpath"

	CMD /c $command
	Write-Host "$filename	Done"
}

function GenerateSchema
{param(
	[string]$executable, 
	[string]$path, 
	[string]$CommonOptions,
	[string]$TableSpecificOptions,
	[string]$userSpecificOptions,
	[string]$dbName
)
	$filename = "Schema.sql"
	$fullpath = $path + $filename
	$OperationSpecificOptions = $CommonOptions + $TableSpecificOptions + "--no-data --skip-triggers"
	$options = $userSpecificOptions + $OperationSpecificOptions
	$command = "$executable  $options $dbName > $fullpath"
	
	CMD /c $command
	Write-Host "$filename		Done"
}

function GenerateRoutinesAndFunctions
{param(
	[string]$executable, 
	[string]$path, 
	[string]$CommonOptions,
	[string]$userSpecificOptions,
	[string]$dbName
)
	$filename = "Routines-Functions.sql"
	$fullpath = $path + $filename
	$OperationSpecificOptions = $CommonOptions + "--routines --no-create-info --no-data --no-create-db --skip-opt --skip-triggers"
	$options = $userSpecificOptions + $OperationSpecificOptions
	$command = "$executable  $options $dbName > $fullpath"
	
	CMD /c $command
	Write-Host "$filename	Done"
}

function GenerateTriggers
{param(
	[string]$executable, 
	[string]$path, 
	[string]$CommonOptions,
	[string]$userSpecificOptions,
	[string]$dbName
)
	$filename = "Triggers.sql"
	$fullpath = $path + $filename
	$OperationSpecificOptions = $CommonOptions + "--triggers --no-create-info --no-data --no-create-db --skip-opt"
	$options = $userSpecificOptions + $OperationSpecificOptions
	$command = "$executable  $options $dbName > $fullpath"
	
	CMD /c $command
	Write-Host "$filename		Done"
}

function GenerateStaticData
{param(
	[string]$executable, 
	[string]$path, 
	[string]$CommonOptions,
	[string]$TableSpecificOptions,
	[string]$userSpecificOptions,
	[string]$dbName,
	[string]$fullpath,
	[string]$StaticDataTables
)
	$filename = "StaticData.sql"
	$fullpath = $path + $filename
	
	if($StaticDataTables -eq '')
	{
		Write-Host "StaticDataTables path is empty. So staticdata and tables script will be almost same..."
		$StaticDataPresent = $False;
	}
	else
	{
		#Checking for StaticDataTables file
		if((Test-Path -path $path) -eq $True)
		{
			$StaticDataPresent = $True;
		}
		else
		{
			$StaticDataPresent = $False
		}
	}
	
	if($StaticDataPresent)
	{
		$StaticDataContent = Get-Content $StaticDataTables
		$StaticDataTablesArray = $StaticDataContent -Split " "
		foreach ($Table in $StaticDataTablesArray)
		{
			$TablesString += $Table + " "
		}
		$OperationSpecificOptionsSchema = $CommonOptions + $TableSpecificOptions + "--no-data --skip-triggers"
		$OperationSpecificOptionsData = $CommonOptions + $TableSpecificOptions + " --no-create-info --skip-triggers"
		
		$options = $userSpecificOptions + $OperationSpecificOptionsSchema
		$command = "$executable  $options $dbName > $fullpath"
		CMD /c $command
		
		$options = $userSpecificOptions + $OperationSpecificOptionsData
		$command = "$executable  $options $dbName $TablesString >> $fullpath"
		CMD /c $command
		
		Write-Host "$filename		Done"
	}
	else
	{
		$OperationSpecificOptions = $CommonOptions + $TableSpecificOptions + "--no-data --skip-triggers"
		$options = $userSpecificOptions + $OperationSpecificOptions
		$command = "$executable  $options $dbName > $fullpath"
		
		CMD /c $command
		Write-Host "$filename		Done"
	}
}

function CheckForInstallations
{param(
	[boolean]$Applicationfound, 
	[string]$InstallLocation, 
	[array]$executableArray,
	[array]$searchExecutable,
	[string]$AlternativeInstallationLocation
)
	#-----------------------------------------------------------------------------
	#Searching installation directory for required executables of MySQL Server
	#-----------------------------------------------------------------------------
	
	if($Applicationfound)
	{
		cd $InstallLocation
		[array]$executableArray = dir -recurse -include $searchExecutable | sort name | select Name,directory
		
		if($searchExecutable.Count -eq $executableArray.Count)
		{
			Write-Host "MySQL Server executables found"
			$ExecutablesFound = $true
		}
	}

	#-----------------------------------------------------------------------------
	#Searching for alternative installations of MySQL Server
	#-----------------------------------------------------------------------------
	
	else
	{
		Write-Host "Searching for other possible installation locations"
		cd $AlternativeInstallationLocation

		[array]$executableArray = dir -recurse -include $searchExecutable | sort name | select Name,directory
		# when number of executables to be searched are equal to number of executables searched from searching directories
		if($searchExecutable.Count -eq $executableArray.Count)
		{
			Write-Host "MySQL Server is installed in a custom location:" $executableArray[0].Directory
			Write-Host "MySQL Server executables found"
			$Applicationfound = $True
			$ExecutablesFound = $True
		}
		else
		{
			Write-Host "Couldn't find MySQL Server on the $env:computername"
			return;
		}
	}
	$checkData = @{}
	$checkData.Add("ExecutablesFound",$ExecutablesFound);
	$checkData.Add("executableArray",$executableArray);
	return $checkData;
}

function SearchForInstallations
{param(
	[string]$serverToCheckMySQLBinary, 
	[array]$SearchInstallations
)
	#Getting current directory to access dependency scripts
	[Environment]::CurrentDirectory=(Get-Location -PSProvider FileSystem).ProviderPath
	$curr_dir = [IO.Directory]::GetCurrentDirectory();

	#-----------------------------------------------------------------------------
	#Searching for MySQL Server on computer
	#-----------------------------------------------------------------------------

	#Define the variable to hold the location of Currently Installed Programs

	$UninstallKey="SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall" 

	#Create an instance of the Registry Object and open the HKLM base key

	$reg=[microsoft.win32.registrykey]::OpenRemoteBaseKey('LocalMachine',$serverToCheckMySQLBinary) 

	#Drill down into the Uninstall key using the OpenSubKey Method

	$regkey=$reg.OpenSubKey($UninstallKey) 

	#Retrieve an array of string that contain all the subkey names

	$subkeys=$regkey.GetSubKeyNames() 

	#Open each Subkey and use GetValue Method to return the required values for each

	foreach($key in $subkeys){

		$thisKey=$UninstallKey+"\\"+$key 

		$thisSubKey=$reg.OpenSubKey($thisKey) 
		
		$ProgramName =  $thisSubKey.getValue("DisplayName")
		
		foreach($Search in $SearchInstallations){
			if($ProgramName  -match $Search)
			{
				Write-Host "MySQL Server is installed on this machine as '$ProgramName'"
				$Applicationfound = $true
				$InstallLocation = $thisSubKey.getValue("InstallLocation")
			}
			break;
		}
		$searchData = @{}
		$searchData.Add("Applicationfound",$Applicationfound);
		$searchData.Add("InstallLocation",$InstallLocation);
	}
	return $searchData;
}


#----------------------------------------------------------------------------
#Main logic beings
#----------------------------------------------------------------------------

Try
{
	#Searching for possible installations
	$searchData = SearchForInstallations -serverToCheckMySQLBinary $serverToCheckMySQLBinary -SearchInstallations $SearchInstallations

	$Applicationfound = $searchData.Get_Item("Applicationfound");
	$InstallLocation = $searchData.Get_Item("InstallLocation");

	#Checking for predefined executables
	$checkData = CheckForInstallations -Applicationfound $Applicationfound	-InstallLocation $InstallLocation -executableArray $executableArray	-searchExecutable $searchExecutable	-AlternativeInstallationLocation $AlternativeInstallationLocation
	$ExecutablesFound = $checkData.Get_Item("ExecutablesFound");
	$executableArray = $checkData.Get_Item("executableArray");
	
	if($ExecutablesFound -eq $false)
	{
		Throw "No executables found to process the request" ;
	}

	#-----------------------------------------------------------------------------
	#Generating scripts based on MySQL Server executables
	#-----------------------------------------------------------------------------

	#Create Database Directory
	$path = CreateDataBaseDirectory -path $path -dbName $dbName


	#Get the executable required for generating scripts
	
	$executable = $executableArray[0].Name
	$directory = $executableArray[0].Directory.FullName

	if ($password) 
	{
		$userSpecificOptions = " --user $userName --password=$password "
	} 
	else 
	{
		$userSpecificOptions = " --user $userName "
	}
	
	$hostOptions = " --port $Port --host $Server "
	$CommonOptions = " $hostOptions --add-drop-database  --compatible=ansi --create-options --events --log-error=$log --quick --quote-names --dump-date --tz-utc "
	$TableSpecificOptions = " --add-drop-table --add-locks --allow-keywords --complete-insert --extended-insert --lock-tables "


	cd $directory

	Write-Host ""
	Write-Host "Generating Scripts Started" -ForegroundColor green
	Write-Host ""


	#Schema and Data
	GenerateSchemaAndData -executable $executable -path $path -CommonOptions $CommonOptions	-TableSpecificOptions $TableSpecificOptions	-userSpecificOptions $userSpecificOptions -dbName $dbName


	#Schema
	GenerateSchema -executable $executable -path $path -CommonOptions $CommonOptions -TableSpecificOptions $TableSpecificOptions -userSpecificOptions $userSpecificOptions -dbName $dbName


	#static data
	GenerateStaticData -executable $executable -path $path -CommonOptions $CommonOptions -TableSpecificOptions $TableSpecificOptions -userSpecificOptions $userSpecificOptions -dbName $dbName -fullpath $fullpath -StaticDataTables $StaticDataTables


	#Routines and user defined functions
	GenerateRoutinesAndFunctions -executable $executable -path $path -CommonOptions $CommonOptions -TableSpecificOptions $TableSpecificOptions -userSpecificOptions $userSpecificOptions -dbName $dbName


	#Triggers
	GenerateTriggers -executable $executable -path $path -CommonOptions $CommonOptions -userSpecificOptions $userSpecificOptions -dbName $dbName


	Write-Host ""
	Write-Host "Generating Scripts Completed" -ForegroundColor green

	#------------------------------------------------------------------------------
	Write-Host ""
	Write-Host "For log details --> $log" -ForegroundColor green
	Invoke-Item $log
}
Catch
{
	#Write-Error "Error : $($_.Exception.Message)" 
	Write-Host "Error : $($_.Exception.Message)"  -ForegroundColor red
	Get-Help
	cd $curr_dir
} 




cd $curr_dir






































<#
#Schema and Data
$filename = "SchemaAndData.sql"
$fullpath = $path + $filename
$OperationSpecificOptions = $CommonOptions + $TableSpecificOptions + "--skip-triggers"
$options = $userSpecificOptions + $OperationSpecificOptions
$command = "$executable  $options $dbName > $fullpath"

CMD /c $command
Write-Host "$filename	Done"



#Schema
$filename = "Schema.sql"
$fullpath = $path + $filename
$OperationSpecificOptions = $CommonOptions + $TableSpecificOptions + "--no-data --skip-triggers"
$options = $userSpecificOptions + $OperationSpecificOptions
$command = "$executable  $options $dbName > $fullpath"

CMD /c $command
Write-Host "$filename		Done"



#static data
$filename = "StaticData.sql"
$fullpath = $path + $filename
if($StaticDataPresent)
{
	$StaticDataContent = Get-Content $StaticDataTables
	$StaticDataTablesArray = $StaticDataContent -Split " "
	foreach ($Table in $StaticDataTablesArray)
	{
		$TablesString += $Table + " "
	}
	$OperationSpecificOptionsSchema = $CommonOptions + $TableSpecificOptions + "--no-data --skip-triggers"
	$OperationSpecificOptionsData = $CommonOptions + $TableSpecificOptions + " --no-create-info --skip-triggers"
	
	$options = $userSpecificOptions + $OperationSpecificOptionsSchema
	$command = "$executable  $options $dbName > $fullpath"
	CMD /c $command
	
	$options = $userSpecificOptions + $OperationSpecificOptionsData
	$command = "$executable  $options $dbName $TablesString >> $fullpath"
	CMD /c $command
	
	Write-Host "$filename		Done"
}
else
{
	$OperationSpecificOptions = $CommonOptions + $TableSpecificOptions + "--no-data --skip-triggers"
	$options = $userSpecificOptions + $OperationSpecificOptions
	$command = "$executable  $options $dbName > $fullpath"
	
	CMD /c $command
	Write-Host "$filename		Done"
}



#Routines and user defined functions
$filename = "Routines-Functions.sql"
$fullpath = $path + $filename
$OperationSpecificOptions = $CommonOptions + "--routines --no-create-info --no-data --no-create-db --skip-opt --skip-triggers"
$options = $userSpecificOptions + $OperationSpecificOptions
$command = "$executable  $options $dbName > $fullpath"

CMD /c $command
Write-Host "$filename	Done"




#Triggers
$filename = "Triggers.sql"
$fullpath = $path + $filename
$OperationSpecificOptions = $CommonOptions + "--triggers --no-create-info --no-data --no-create-db --skip-opt"
$options = $userSpecificOptions + $OperationSpecificOptions
$command = "$executable  $options $dbName > $fullpath"

CMD /c $command
Write-Host "$filename		Done"
	
	
	
	
#Validating parameters
if($Server -eq '')
{	
	Throw "Server name cannot be empty. Please provide a valid name..."
}
if($dbName -eq '')
{
	Throw "Database name cannot be empty. Please provide a valid name..."
}
if($userName -eq '')
{	Write-Host "User name cannot be empty. Please provide a valid name..." -ForegroundColor red
	return;
}
if($withOutPassword -eq $false)
{	
	if($password -eq '')
	{	
		Write-Host "password cannot be empty. Please provide a valid password..." -ForegroundColor red
		return;
	}
}
if($path -eq '')
{
	Write-Host "Make sure the destination directory exists" -ForegroundColor red
	return;
}
# Create directory if does not exist
if((Test-Path -path $path) -ne $True)
{
	Write-Host "Make sure the destination directory exists" -ForegroundColor red
	return;
}




#If arguments are insufficient
if($args.count -ne 7)
{
	Get-Help
	return;
};


#Setting up arguments to variables
$Server = $args[0];
$userName = $args[1];
$password = $args[2];
$dbName = $args[3];
$path = $args[4];
$StaticDataTables = $args[5];
[bool]$withOutPassword = $args[6];
#>

