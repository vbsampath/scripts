#If arguments are insufficient
if($args.count -ne 7)
{
	Write-Host "Usage:"
	Write-Host 'Syntax : "ServerName" "Username" "Password" "Database" "Scripts Directory"  "Option -->SchemaOnly OR SchemaAndData OR StaticData" "Without Password -- $true or $false"'
	Write-Host 'Example : "SQLPOC" "root" "123" "QCCDB" "C:\DbScripts" "StaticData" $false'
	Write-Host 'Additional Information : This script creates database from scripts depending upon options for tables and remaining are common like 1)Schema 2)Schema and Data 3)Stored procedures 4)Schema with Static tables 5)Triggers'
	return;
};

#Setting up arguments to variables
$Server = $args[0];
$userName = $args[1];
$password = $args[2];
$dbName = $args[3];
$path = $args[4];
$option = $args[5];
[bool]$withOutPassword = $args[6];


#----------------------------------------------------------------------------------
#Validating parameters
#----------------------------------------------------------------------------------

if($Server -eq '')
{	Write-Host "Server name cannot be empty. Please provide a valid name..."
	return;
}
if($dbName -eq '')
{
	Write-Host "Database name cannot be empty. Please provide a valid name..."
	return;
}
if($userName -eq '')
{	Write-Host "User name cannot be empty. Please provide a valid name..."
	return;
}
if($path -eq '')
{
	Write-Host "Specified path is not valid. Please provide a valid path..."
	return;
}
if($withOutPassword -eq $false)
{	
	if($password -eq '')
	{	
		Write-Host "password cannot be empty. Please provide a valid password..."
		return;
	}
}



#----------------------------------------------------------------------------------
#Logical validations
#----------------------------------------------------------------------------------

if((Test-Path -path $path) -ne $True)
{
	Write-Host "Specified path is invalid. Please provide a valid path..."
	return;
}
if(!$path.EndsWith("\"))
{
	$path = $path + "\"
}

if($option -eq '')
{	
	Write-Host "No option specified so defaulting to StaticData"
	$option = "StaticData"
}




#Variables
$Applicationfound = $false
$ExecutablesFound = $false
$Search = 'MySQL Server'
$AlternativeInstallationLocation = "C:\XAMP\"
[Array]$searchExecutable = ('mysql.exe')
$log = [System.IO.Path]::GetTempFileName() | Rename-Item -NewName { $_ -replace 'tmp$', 'txt' } �PassThru
$filename = ''
$fullpath = ''
$CommonOptions = ''
$OperationSpecificOptions = ''




#----------------------------------------------------------------------------------
# Main logic starts from here
#----------------------------------------------------------------------------------



#Getting current directory to access dependency scripts
[Environment]::CurrentDirectory=(Get-Location -PSProvider FileSystem).ProviderPath
$curr_dir = [IO.Directory]::GetCurrentDirectory();



#-----------------------------------------------------------------------------
#Searching for MySQL Server on computer
#-----------------------------------------------------------------------------

#Define the variable to hold the location of Currently Installed Programs

$UninstallKey="SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall" 

#Create an instance of the Registry Object and open the HKLM base key

$reg=[microsoft.win32.registrykey]::OpenRemoteBaseKey('LocalMachine',$Server.computername) 

#Drill down into the Uninstall key using the OpenSubKey Method

$regkey=$reg.OpenSubKey($UninstallKey) 

#Retrieve an array of string that contain all the subkey names

$subkeys=$regkey.GetSubKeyNames() 

#Open each Subkey and use GetValue Method to return the required values for each

foreach($key in $subkeys){

	$thisKey=$UninstallKey+"\\"+$key 

	$thisSubKey=$reg.OpenSubKey($thisKey) 
	
	$ProgramName =  $thisSubKey.getValue("DisplayName")
	
	if($ProgramName  -match $Search)
	{
		Write-Host "MySQL Server is installed on this machine"
		$Applicationfound = $true
		$InstallLocation = $thisSubKey.getValue("InstallLocation")
	}
} 

#-----------------------------------------------------------------------------
#Searching installation directory for required executables of MySQL Server
#-----------------------------------------------------------------------------
if($Applicationfound)
{
	cd $InstallLocation
	[array]$executableArray = dir -recurse -include $searchExecutable | sort name | select Name,directory
	
	if($searchExecutable.Count -eq $executableArray.Count)
	{
		Write-Host "MySQL Server executables found"
		$ExecutablesFound = $true
	}
}

#-----------------------------------------------------------------------------
#Searching for alternative installations of MySQL Server
#-----------------------------------------------------------------------------

else
{
	Write-Host "Searching for other possible installation locations"
	cd $AlternativeInstallationLocation

	[array]$executableArray = dir -recurse -include $searchExecutable | sort name | select Name,directory
	if($searchArray.Count -eq $executableArray.Count)
	{
		Write-Host "MySQL Server is installed in a custom location"
		Write-Host "MySQL Server executables found"
		$Applicationfound = $True
		$ExecutablesFound = $True
	}
	else
	{
		Write-Host "Couldn't find MySQL Server on the $computer.computername"
		return;
	}

}


#----------------------------------------------------------------------------------
# Importing logic starts from here
#----------------------------------------------------------------------------------


if($ExecutablesFound)
{
	#Get the executable required for generating scripts
	
	$executable = $executableArray[0].Name
	$directory = $executableArray[0].Directory.FullName

	
	$CommonOptions = "--user=$userName --show-warnings "
	$PasswordOptions = " --password=$password "
	
	
	cd $directory
	
	if($withOutPassword)
	{
		#Removing database if already exists
		Write-Host "Removing Database $dbName if Any...."  -ForegroundColor gray
		$OperationSpecificOptions = " -e  `"DROP DATABASE $dbName`""
		$command = "$executable $CommonOptions   $OperationSpecificOptions 1>>$log 2>>&1"
		CMD /c $command
		
		#Creating database
		Write-Host "Creating Database $dbName ...."  -ForegroundColor gray
		$OperationSpecificOptions = " -e  `"CREATE DATABASE $dbName`""
		$command = "$executable $CommonOptions   $OperationSpecificOptions 1>>$log 2>>&1"
		CMD /c $command
		
		$CommonOptions = $CommonOptions + " --database=$dbName "
		#--------------------------------------------------------------------------
		
		Write-Host ""
		Write-Host "Importing from Scripts Started" -ForegroundColor gray
		Write-Host ""
		
		#--------------------------------------------------------------------------
		# Tables with Schema only or SchemaAndData or StaticData
		#--------------------------------------------------------------------------


		If($option -eq 'SchemaOnly')
		{
			$filename = "Schema.sql"
			$fullpath = $path + $filename
		}
		If($option -eq 'SchemaAndData')
		{
			$filename = "SchemaAndData.sql"
			$fullpath = $path + $filename
		}
		If($option -eq 'StaticData')
		{
			$filename = "StaticData.sql"
			$fullpath = $path + $filename
		}
		
		$command = "$executable $CommonOptions  < $fullpath 1>> $log 2>>&1"
		CMD /c $command
		Write-Host "$filename		Done" -ForegroundColor green
		
		#--------------------------------------------------------------------------
		# Routines and Functions
		#--------------------------------------------------------------------------
		

		$filename = "Routines-Functions.sql"
		$fullpath = $path + $filename
		
		$command = "$executable $CommonOptions  < $fullpath 1>> $log 2>>&1"
		CMD /c $command
		Write-Host "$filename	Done" -ForegroundColor green
		
		#--------------------------------------------------------------------------
		# Triggers
		#--------------------------------------------------------------------------
		

		$filename = "Triggers.sql"
		$fullpath = $path + $filename
		
		$command = "$executable $CommonOptions  < $fullpath 1>> $log 2>>&1"
		CMD /c $command
		Write-Host "$filename		Done" -ForegroundColor green
		
	}
	else
	{
		#Removing database if already exists
		Write-Host "Removing Database $dbName if Any...."  -ForegroundColor gray
		$OperationSpecificOptions = " -e  `"DROP DATABASE IF EXISTS $dbName`""
		$command = "$executable $CommonOptions  $PasswordOptions $OperationSpecificOptions 1>>$log 2>>&1"
		CMD /c $command
		
		#Creating database
		Write-Host "Creating Database $dbName ...."  -ForegroundColor gray
		$OperationSpecificOptions = " -e  `"CREATE DATABASE $dbName`""
		$command = "$executable $CommonOptions  $PasswordOptions $OperationSpecificOptions 1>>$log 2>>&1"
		CMD /c $command
		
		$CommonOptions = $CommonOptions + " --database=$dbName "
		#--------------------------------------------------------------------------
		
		Write-Host ""
		Write-Host "Importing from Scripts Started" -ForegroundColor gray
		Write-Host ""
		
		#--------------------------------------------------------------------------
		# Tables with Schema only or SchemaAndData or StaticData
		#--------------------------------------------------------------------------


		If($option -eq 'SchemaOnly')
		{
			$filename = "Schema.sql"
			$fullpath = $path + $filename
		}
		If($option -eq 'SchemaAndData')
		{
			$filename = "SchemaAndData.sql"
			$fullpath = $path + $filename
		}
		If($option -eq 'StaticData')
		{
			$filename = "StaticData.sql"
			$fullpath = $path + $filename
		}
		
		$command = "$executable $CommonOptions $PasswordOptions < $fullpath 1>> $log 2>>&1"
		CMD /c $command
		Write-Host "$filename		Done" -ForegroundColor green
		
		#--------------------------------------------------------------------------
		# Routines and Functions
		#--------------------------------------------------------------------------
		

		$filename = "Routines-Functions.sql"
		$fullpath = $path + $filename
		
		$command = "$executable $CommonOptions $PasswordOptions < $fullpath 1>> $log 2>>&1"
		CMD /c $command
		Write-Host "$filename	Done" -ForegroundColor green
		
		#--------------------------------------------------------------------------
		# Triggers
		#--------------------------------------------------------------------------
		

		$filename = "Triggers.sql"
		$fullpath = $path + $filename
		
		$command = "$executable $CommonOptions $PasswordOptions < $fullpath 1>> $log 2>>&1"
		CMD /c $command
		Write-Host "$filename		Done" -ForegroundColor green
	}
	
}
	
Write-Host ""
Write-Host "Importing from Scripts Complete" -ForegroundColor green

#------------------------------------------------------------------------------
Write-Host ""
Write-Host "For log details --> $log" -ForegroundColor green
Invoke-Item $log



cd $curr_dir
		
