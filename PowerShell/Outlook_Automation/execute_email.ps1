# (C) Dmitry Sotnikov
# http://dmitrysotnikov.wordpress.com
# This is a PowerShell companion script for Outlook
# macro processing PowerShell commands from email

# Delete any previous transcripts and start a new one
Remove-Item "C:\Docs\PowerShell_Scripts\Outlook_Automation\email_transcript.txt" -ErrorAction SilentlyContinue
Start-Transcript "C:\Docs\PowerShell_Scripts\Outlook_Automation\email_transcript_temp.txt"

# wait till Outlook_saves the script email
while ( -not (Test-Path "C:\Docs\PowerShell_Scripts\Outlook_Automation\outlook.ps1")) {
    Start-Sleep -Seconds 1
}

# Read the script, skip the header lines, execute the rest
Get-Content "C:\Docs\PowerShell_Scripts\Outlook_Automation\outlook.ps1" | Where { $i++ -gt 4 } > "C:\Docs\PowerShell_Scripts\Outlook_Automation\justscript.ps1"
. "C:\Docs\PowerShell_Scripts\Outlook_Automation\justscript.ps1"

# Remove the old script
Remove-Item "C:\Docs\PowerShell_Scripts\Outlook_Automation\outlook.ps1" -ErrorAction SilentlyContinue
Remove-Item "C:\Docs\PowerShell_Scripts\Outlook_Automation\justscript.ps1" -ErrorAction SilentlyContinue

# Stop transcript and make it available for Outlook to send back
Stop-Transcript
Rename-Item "C:\Docs\PowerShell_Scripts\Outlook_Automation\email_transcript_temp.txt" -NewName "email_transcript.txt"