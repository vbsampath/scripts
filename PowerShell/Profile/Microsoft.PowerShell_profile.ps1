
[string]$modulePath = [Environment]::GetEnvironmentVariable("PSModulePath", "User")

function Import-UserModules
{
	Get-Module -ListAvailable | Foreach-Object {
		$moduleBase = $_.ModuleBase
		$moduleName = $_.Name
		if($moduleBase.Contains($modulePath))
		{
			Import-Module $moduleName #-verbose
			Write-Host $("Imported "+$moduleName)
		}
	}
}
Import-UserModules
Clear-Host