<# 
    .SYNOPSIS 
    Its a module for all profile related stuff
 
    .DESCRIPTION 
    Its a module for consolidating all profile related functions
	If anything related to a user profile then it goes here
	
	.NOTES 
		File Name  : Profile.psm1
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3 
		Date 	   : 13-June-2014
#> 

function Get-UserFavorites
{
<#
.SYNOPSIS
Gets user favorites
.Description
Gets user favorites in windows explorer left top bar with their names and locations for future purposes or just to make a note of it
.Parameter key
key to search for a specific favorite or else print all favorites

.Link
http://powershell.org/wp/forums/topic/find-shortcut-targets/

.Example
	# PS> Get-UserFavorites

	Description
	-----------
	Prints all user favorites
.Example
	# PS> Get-UserFavorites "Outlook"

	Description
	-----------
	Searches for "Outlook" favorite and then prints it. This is initialization without mentioning parameter name
.Example
	# PS> Get-UserFavorites -key "Outlook"

	Description
	-----------
	Searches for "Outlook" favorite and then prints it. With parameter name
#>
[CmdletBinding()]
param(
	[Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)][string]$key=""
)
	$favorites = Join-Path $env:userprofile "Links"
    $Shortcuts = Get-ChildItem -Recurse $favorites -Include *.lnk
    $Shell = New-Object -ComObject WScript.Shell
	$links=@{}
    foreach ($Shortcut in $Shortcuts)
	{
		$links[$Shortcut.BaseName] = $Shell.CreateShortcut($Shortcut).targetpath
	}
	[Runtime.InteropServices.Marshal]::ReleaseComObject($Shell) | Out-Null
	if($key -ne "")
	{
		Write-Output $links.Get_Item($key)
	}
	else
	{
		Write-Output $links
	}
}


function Get-SpecialFolders
{
<#
.SYNOPSIS
Prints all special folders on the user profile or accessible to user
.Description
Gets all initialized special folders to user
.Parameter key
key to search for a specific special folder or else print all special folders

.Link
http://windowsitpro.com/powershell/easily-finding-special-paths-powershell-scripts

.Example
	# PS> Get-SpecialFolders

	Description
	-----------
	Prints all special folders
.Example
	# PS> Get-SpecialFolders "Bit Repositories"

	Description
	-----------
	Searches for "Bit Repositories" special folder and then prints it. This is initialization without mentioning parameter name
.Example
	# PS> Get-SpecialFolders -key "Bit Repositories"

	Description
	-----------
	Searches for "Bit Repositories" special folder and then prints it. With parameter name
#>
[CmdletBinding()]
param(
	[Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)][string]$key=""
)
	$SpecialFolders = @{}

	$names = [Environment+SpecialFolder]::GetNames([Environment+SpecialFolder])
	foreach($name in $names)
	{
		if($path = [Environment]::GetFolderPath($name)){
			$SpecialFolders[$name] = $path
		}
	}
	if($key -ne "")
	{
		Write-Output $SpecialFolders.Get_Item($key)
	}
	else
	{
		Write-Output $SpecialFolders
	}
}


Set-Alias guf Get-UserFavorites
Set-Alias gsf Get-SpecialFolders

Export-ModuleMember -function "*"
Export-ModuleMember -Alias "*"