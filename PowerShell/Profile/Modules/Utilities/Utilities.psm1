<# 
    .SYNOPSIS 
    Its a module for all generic functions 
 
    .DESCRIPTION 
    This module contains all functions which make powershell work easy and fun.
	
	.NOTES 
		File Name  : Utilities.psm1
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3  
		Date 	   : 13-June-2014
#> 

function modulesavailable
{
	Get-Module -ListAvailable
}

function pro 
{ 
	npp $profile 
}

function powershelldir 
{
	 Set-Location 'E:\Programs\BitBucket\Git\Repositories\Scripts\PowerShell\'
}

function phonegapdir 
{
	 Set-Location 'E:\Programs\PhoneGap\'
}

function phonegaprunandroid
{
	 phonegap run android
}

function findfile ([string] $glob)
{
<# 
    .SYNOPSIS 
    find files in current directory with regex pattern
 
    .DESCRIPTION 
    find files in current directory with regex pattern
	
	.LINK
	http://thesoftwaresimpleton.blogspot.in/2011/05/own-your-powershell-profile.html
 
    .EXAMPLE 
		PS C:\>ff *.txt

		Description
		-----------
		Returns an string with downloaded content. This is case with parameterized names for inputs
#> 
	get-childitem -recurse -include $glob
}

function Remove-InvalidFileNameChars 
{ 
    <# 
    .SYNOPSIS 
    This is a PowerShell function to remove invalid characters from strings to be used as file names. 
 
    .DESCRIPTION 
    The function takes a string parameter called Name and returns a string that has been stripped of invalid file name characters, i.e. *, :, \, /.  The Name parameter will also receive input from the pipeline. 
 
    .PARAMETER Name 
    Specifies the file name to strip of invalid characters. 
 
    .INPUTS 
    Parameter Name accepts System.String objects from the pipeline. 
 
    .OUTPUTS 
    System.String.  Outpus a string object 
 
    .EXAMPLE 
    Remove-InvalidFileNameChars -Name "<This/name\is*an:illegal?filename>" 
    PS C:\>Thisnameisanillegalfilename 
 
    .NOTES 
    It would be easiest to copy the function from the script file and place it in your profile.  However, you may also dot-source the script to load the function into PowerShell: 
    i.e. PS C:\>. .\Remove-InvalidFileNameChars.ps1 
    #> 
 
    [CmdletBinding()] 
 
    param(
		[Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)] [String]$Name,
		[Parameter(Mandatory=$false, Position=1, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)] [String]$ReplaceWith 
    ) 
	
    return [RegEx]::Replace($Name, "[{0}]" -f ([RegEx]::Escape([String][System.IO.Path]::GetInvalidFileNameChars())), $ReplaceWith) 
}


function Get-UrlAsString
{
<# 
    .SYNOPSIS 
    Downloads url as a string
 
    .DESCRIPTION 
    Downloads url as a string and returns the same. This is a basic version.
 
    .PARAMETER Url 
    Specifies the url which needs to be downloaded as string
 
    .INPUTS 
    Parameter Url accepts System.String objects from the pipeline. 
	
    .EXAMPLE 
		PS C:\>Get-UrlAsString -Url "http://technet.microsoft.com/en-in/library/ee692685.aspx"

		Description
		-----------
		Returns an string with downloaded content. This is case with parameterized names for inputs
		
	.EXAMPLE 
		PS C:\>Get-UrlAsString "http://technet.microsoft.com/en-in/library/ee692685.aspx"

		Description
		-----------
		Returns an string with downloaded content. This is case for piped inputs
    
	.OUTPUTS
		System.String
		
    .NOTES 
		Function Name  : Get-UrlAsString
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3   
#> 

[CmdletBinding()] 

param(
	[Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)] [ValidateNotNullOrEmpty()] [string]$Url
)
	[string]$reply=$null
	try
	{
		$webclient = New-Object System.Net.WebClient
		$reply = $webclient.DownloadString($Url)
	}
	finally
	{
		$webclient.dispose()
	}
	
	Return $reply
}


function Get-RegexMatches
{
<# 
    .SYNOPSIS 
    Return matches after matching a string with regex pattern.
 
    .DESCRIPTION 
    Return matches after matching a string with regex pattern.
 
    .PARAMETER InputString 
    Specifies the string which needs to be matched against regex pattern.
 
    .INPUTS 
    Parameter InputString accepts System.String objects from the pipeline. 
	
	.PARAMETER RegexPattern 
    Specifies the regex pattern.
 
    .INPUTS 
    Parameter RegexPattern accepts System.String objects from the pipeline. 
 
    .EXAMPLE 
		PS C:\>Get-RegexMatches -InputString "this is a string" -RegexPattern "\w+" 

		Description
		-----------
		Returns an array with matches. This is case with parameterized names for inputs
		
	.EXAMPLE 
		PS C:\>Get-RegexMatches "this is a string" "\w+" 

		Description
		-----------
		Returns an array with matches. This is case for piped inputs
    
	.OUTPUTS
		System.Array
		
    .NOTES 
		Function Name  : Get-RegexMatches 
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3   
#> 

[CmdletBinding()] 

param(
	[Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)] [ValidateNotNullOrEmpty()] [string]$InputString,
	[Parameter(Mandatory=$true, Position=1, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)] [ValidateNotNullOrEmpty()] [string]$RegexPattern
)
	[System.Array]$matches=$null
	$found = $InputString -Match $RegexPattern
	if($found -eq $True)
	{
		$matches
	}
	Return $matches
}

function Set-SpecificDirectoryHierarchy
{
<# 
    .SYNOPSIS 
    Moves current location number level in hierarchy
 
    .DESCRIPTION 
    Sets current location to number times in hierarchy level
	u 2 takes location to 2 levels upper
 	
	.PARAMETER Num 
    Specifies the how many levels should the location move upwards in hierarchy.
 
    .INPUTS 
    Parameter Num accepts System.Integer objects from the pipeline. 
 
    .EXAMPLE 
		PS C:\>u -Num 2

		Description
		-----------
		Moves directory to number level hierarchy. This is case with parameterized names for inputs
		
	.EXAMPLE 
		PS C:\>u 2

		Description
		-----------
		Moves directory to number level hierarchy. This is case with parameterized names for inputs
		
    .NOTES 
		Function Name  : Set-SpecificDirectoryHierarchy
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3   
		Date	   : 13-June-2014
#> 
[CmdletBinding()] 

param(
	[Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)][string]$Num=""
)
	for($i = 1; $i -le $num; $i++){
		$u =  "".PadLeft($i,"u")
		$d =  $u.Replace("u","../")
	}
	Invoke-Expression "push-location $d"
}

function Get-EnvironmentVariable
{
<# 
    .SYNOPSIS 
    Adds environment variables
 
    .DESCRIPTION 
    Adds environment variables to specified key and as specified user
 	
	.PARAMETER Key
    Specifies the variable which we are going to add to environment variables.
 
    .INPUTS 
    Parameter Key accepts System.String objects from the pipeline. 
	
	.PARAMETER Value
    Specifies the variable value which we are going to add to environment variables.
 
    .INPUTS 
    Parameter Value accepts System.String objects from the pipeline. 
	
	.PARAMETER InstallFor
    Specifies the installation scope of the environmental variable.
 
    .INPUTS 
    Parameter InstallFor accepts System.String objects from the pipeline. 
 
    .EXAMPLE 
		PS C:\>Add-EnvironmentVariable -Key "JAVA_HOME" -Value "C:\Program Files\Java\jdk1.6.0_23" -InstallFor "User"

		Description
		-----------
		Adds environmental variable specified by user. This is an example with parameterized initializing
		
	.EXAMPLE 
		PS C:\>Add-EnvironmentVariable "JAVA_HOME" "C:\Program Files\Java\jdk1.6.0_23" "User"

		Description
		-----------
		Adds environmental variable specified by user. This is an example without parameters
		
    .NOTES 
		Function Name  : Get-EnvironmentVariable
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3   
		Date	   : 13-June-2014
#> 
[CmdletBinding(DefaultParameterSetName = "Get")]
param(
	[Parameter(ParameterSetName="Get", Mandatory=$true, Position=0)]
	[Parameter(ParameterSetName="Add", Mandatory=$true, Position=0)][string]$Key,
	[Parameter(ParameterSetName="Add", Mandatory=$true, Position=1)][string][switch]$Value,
	[Parameter(ParameterSetName="Add", Mandatory=$true, Position=2)][string]$InstallFor
)
	switch ($PsCmdlet.ParameterSetName)
    {
	"Get"  { 
		if($Key -ne "All")
		{
			[Environment]::GetEnvironmentVariable($Key,"User"); break 
		}
		else
		{
			Get-ChildItem Env:
		}
	}
    "Add"  { [Environment]::SetEnvironmentVariable($Key, $Value, $InstallFor); break }
    } 
}

function Get-MyCodeBlocks
{
<# 
    .SYNOPSIS 
    Displays a list of all defined functions, aliases and other stuff in user modules
 
    .DESCRIPTION 
    Displays a list of all defined functions, aliases and other stuff in user modules. 
	The only dependency is that we need to have PSModulePath defined for user because we are getting only user defined stuff here
 	
    .EXAMPLE 
		PS C:\>Get-MyCodeBlocks

		Description
		-----------
		Returns list of all available functions, aliases and other stuff defined in user modules
    
	.OUTPUTS
		System.Object
		
    .NOTES 
		Function Name  : Get-MyCodeBlocks
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3   
		Date		: 14-June-2014
#> 

	[string]$modulePath = [Environment]::GetEnvironmentVariable("PSModulePath", "User")
	[System.Array]$modulesArray=$null
	Get-ChildItem $modulePath | 
	Foreach-Object{
		$moduleName = $_.Name
		#$moduleLastWrittenTime = $_.LastWrittenTime
		$modulesArray += $moduleName
	}
	$modulesString = [string]::join(',', $modulesArray)
	$getCommandObjects = "Get-Command -Module $modulesString" | Invoke-Expression
	$getCommand=@()
	foreach($getCommandObject in $getCommandObjects)
	{
		$object = New-Object System.Object
		$object | Add-Member -Type NoteProperty �Name CommandType �Value $getCommandObject.CommandType
		$object | Add-Member -Type NoteProperty �Name ModuleName �Value $getCommandObject.ModuleName
		$object | Add-Member -Type NoteProperty �Name Name �Value $getCommandObject.Name
		$object | Add-Member -Type NoteProperty �Name Parameters �Value $null
		if($getCommandObject.CommandType -eq "Function")
		{
			$commandParameterSetInfos = $getCommandObject.ParameterSets[0] | select -ExpandProperty parameters | Where-Object {($_.Position -ge 0)}
			if($commandParameterSetInfos -ne $null)
			{
				[System.Array]$paramterArray=$null
				foreach($commandParameterSetInfo in $commandParameterSetInfos)
				{
					$value = $("{"+$commandParameterSetInfo.Name + " - "+$commandParameterSetInfo.ParameterType.FullName + "}")
					$paramterArray+=$value
				}
				$parametersString =  [string]::join(", ",$paramterArray)
				$object.Parameters = $parametersString
			}
		}
		$getCommand += $object
	}
	Write-Output $getCommand | Sort-Object CommandType | Format-Table -Autosize
}


Set-Alias ric Remove-InvalidFileNameChars
Set-Alias gs Get-UrlAsString
Set-Alias grm Get-RegexMatches
Set-Alias psdir powershelldir
Set-Alias pgdir phonegapdir
Set-Alias pra phonegaprunandroid
Set-Alias gma modulesavailable
Set-Alias u Set-SpecificDirectoryHierarchy
Set-Alias env Get-EnvironmentVariable
Set-Alias my Get-MyCodeBlocks

Export-ModuleMember -Alias "*"
Export-ModuleMember -Function "*"