<# 
    .SYNOPSIS 
    Its a module for all aliases
 
    .DESCRIPTION 
    Its a module for all aliases
	
	.NOTES 
		File Name  : Aliases.psm1
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3  
		Date 	   : 13-June-2014
#> 

#Simple Aliases
Set-Alias l Get-ChildItem
Set-Alias ga Get-Alias
#get-alias | where-object {($_.Definition -Contains $_)}
Set-Alias gh Get-Help
set-alias npp "C:\Program Files\Notepad++\notepad++.exe"

#Complex Aliases
Set-alias Out-Clipboard -value $env:SystemRoot\system32\clip.exe

Export-ModuleMember -Alias "*"