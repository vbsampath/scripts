<# 
    .SYNOPSIS 
    Its a module for all UI modding
 
    .DESCRIPTION 
    Its a module for all UI modding
	
	.NOTES 
		File Name  : UI.psm1
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3  
#> 


if($Host.UI.RawUI.WindowTitle -like "*administrator")
{
	$Host.UI.RawUI.ForegroundColor = "Red"
}



<#
$rawUI = (Get-Host).UI.RawUI
$windowSize = $rawUI.WindowSize
$windowSize.Width = 140
$windowSize.Height = 60
$rawUI.WindowSize = $windowSize
#>

<#[Environment]::SetEnvironmentVariable("p", "C:\MyPath", "User")#>

<#
function New-PSSecureRemoteSession
{
	param ($sshServerName, $Cred)
	$Session = New-PSSession $sshServerName -UseSSL -Credential $Cred -ConfigurationName C2Remote
	Enter-PSSession -Session $Session
}
set-alias pssh New-PSRemoteSession
#>



<#
$registryItemPath = Join-Path HKCU:\Console "Sampath PowerShell"

function Reset-Registry {
	if (Test-Path $registryItemPath) {
		foreach ($registryItemPath in $script:registryItemPaths) {
			if (Test-Path $registryItemPath) {
				Remove-Item $registryItemPath
				"Removed `"$registryItemPath`""
			}
		}
	}
}
function New-RegistryItem {
    param (
        [string]$Path
    )
    $x = New-Item $Path
	
    # http://twinside.free.fr/dotProject/?p=125
    $colors = @(
        0x00eeeeee, 0x0000749e, 0x00fefefe, 0x00fbe7b2,
        0x0098cafd, 0x00f8f5de, 0x00506070, 0x003f3f3f,
        0x008080c0, 0x00ffafaf, 0x007f9f7f, 0x00d3d08c,
        0x007071e3, 0x00c880c8, 0x00506070, 0x00ffffff
    )
    for ($index = 0; $index -lt $colors.Length; $index++) {
        $x = New-ItemProperty $Path -Name ("ColorTable" + $index.ToString("00")) -PropertyType DWORD -Value $colors[$index]
    }
    "Created `"$Path`""
}
#>


<#
#Reset-Registry
# Remove registry item.

if (Test-Path $registryItemPath) {
	Remove-Item $registryItemPath
}
New-RegistryItem $registryItemPath
if (-not $script:registryItemPaths) {
	$script:registryItemPaths = @()
}
$script:registryItemPaths += $registryItemPath
#>



<#
## From Windows PowerShell Cookbook (O'Reilly)
## by Lee Holmes (http://www.leeholmes.com/guide)

Set-StrictMode -Version Latest

Push-Location
Set-Location HKCU:\Console
New-Item '.\%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe'
Set-Location '.\%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe'

Set-ItemProperty . ColorTable00 -type DWORD -value 0x00506070 #000000
Set-ItemProperty . ColorTable07 -type DWORD -value 0x00f0edee #c0c0c0
New-ItemProperty . FaceName -type STRING -value "Lucida Console"
New-ItemProperty . FontFamily -type DWORD -value 0x00000036
New-ItemProperty . FontSize -type DWORD -value 0x000c0000
New-ItemProperty . FontWeight -type DWORD -value 0x00000190
New-ItemProperty . HistoryNoDup -type DWORD -value 0x00000000
New-ItemProperty . QuickEdit -type DWORD -value 0x00000001
New-ItemProperty . ScreenBufferSize -type DWORD -value 0x0bb80078
New-ItemProperty . WindowSize -type DWORD -value 0x00320078

Pop-Location
#>


<#

#changing other colours which we might not think needed for now
$Host.PrivateData.ErrorForegroundColor = 'Red'
$Host.PrivateData.ErrorBackgroundColor = $bckgrnd
$Host.PrivateData.WarningForegroundColor = 'Magenta'
$Host.PrivateData.WarningBackgroundColor = $bckgrnd
$Host.PrivateData.DebugForegroundColor = 'Yellow'
$Host.PrivateData.DebugBackgroundColor = $bckgrnd
$Host.PrivateData.VerboseForegroundColor = 'Green'
$Host.PrivateData.VerboseBackgroundColor = $bckgrnd
$Host.PrivateData.ProgressForegroundColor = 'Cyan'
$Host.PrivateData.ProgressBackgroundColor = $bckgrnd


#Adding other console fonts to powershell 

$key = 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Console\TrueTypeFont'
Set-ItemProperty -Path $key -Name '0' -Value 'Lucida Console'
Set-ItemProperty -Path $key -Name '00' -Value 'Courier New'
Set-ItemProperty -Path $key -Name '000' -Value 'Consolas'
Set-ItemProperty -Path $key -Name '0000' -Value 'Lucida Sans Typewriter'
Set-ItemProperty -Path $key -Name '00000' -Value 'OCR A Extended'


#>



<#
Change colors from the properties window and Color Tab
BackgroundColor = RGB(80,96,112) PURPLE #506070
ForegroundColor = RGB(232,230,212) YELLOW #EEE3D7
#>
