#http://get-powershell.com/post/2008/11/14/PowerShell-and-TFS-Work-Items.aspx
function Get-TfsServer {
param([string] $serverName = "itstfs")
 
# load the required dll
[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.Client")
 
$propertiesToAdd = (
        ('VCS', 'Microsoft.TeamFoundation.VersionControl.Client', 'Microsoft.TeamFoundation.VersionControl.Client.VersionControlServer'),
        ('WIT', 'Microsoft.TeamFoundation.WorkItemTracking.Client', 'Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItemStore'),
        ('CSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.ICommonStructureService'),
        ('GSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.IGroupSecurityService')
    )
 
    # fetch the TFS instance, but add some useful properties to make life easier
    # Make sure to "promote" it to a psobject now to make later modification easier
    [psobject] $tfs = [Microsoft.TeamFoundation.Client.TeamFoundationServerFactory]::GetServer($serverName)
    foreach ($entry in $propertiesToAdd) {
        $scriptBlock = '
            [System.Reflection.Assembly]::LoadWithPartialName("{0}") > $null
            $this.GetService([{1}])
        ' -f $entry[1],$entry[2]
        $tfs | add-member scriptproperty $entry[0] $ExecutionContext.InvokeCommand.NewScriptBlock($scriptBlock)
    }
    return $tfs
}

$workspaceFolder = "C:\Users\sampathv.IE\Documents\Projects\iCrowdIPO"
$tfs = Get-TfsServer  http://tfs.ideaentity.com:8080/tfs/IE
$workspace = $tfs.vcs.GetWorkspace($workspaceFolder)
$serverFiles = @()
$serverFiles += "$/iCrowdIPO/Code/iCrowdMain-Dev/iCrowdPHP/protected/components/IEJavaModel.php"

[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.VersionControl.Client")

$GetRequests = @()
foreach($file in $serverFiles)
{
    $changesetNumber = 12510
    $ItemSpec = New-Object -TypeName Microsoft.TeamFoundation.VersionControl.Client.ItemSpec -ArgumentList $serverFiles, 'None'
    $GetRequest = New-Object -TypeName Microsoft.TeamFoundation.VersionControl.Client.GetRequest -ArgumentList $ItemSpec, $changesetNumber
    $GetRequests += $GetRequest
}

foreach($GetRequest in $GetRequests)
{
    $workspace.Get($GetRequest,'Overwrite')
}
#$itemSpecClass = New-Object -TypeName Microsoft.TeamFoundation.VersionControl.Client.Item| Get-Member
#$itemSpecClass = New-Object [Microsoft.TeamFoundation.VersionControl.Client.ItemSpec]
#$serverFiles += "$/iCrowdIPO/Code/iCrowdMain-Dev/iCrowdPHP/nbproject/project.properties"
#$itemSpecs = $tfs.vcs.ItemSpec.FromStrings($serverFiles)
#$workspace = $tfs.vcs
#WorkspaceInfo wsInfo = Workstation.Current.GetLocalWorkspaceInfo(@"C:\Users\sampathv.IE\Documents\Projects\iCrowdIPO");
#IEHYDCPU0033
#$wsInfo = Workstation.Current.GetLocalWorkspaceInfo(@"C:\Users\sampathv.IE\Documents\Projects\iCrowdIPO");
#Write-Output $wsInfo
#Write-Output $tfs.vcs.Workstation.Current.GetLocalWorkspaceInfo("C:\Users\sampathv.IE\Documents\Projects\iCrowdIPO")
#[Microsoft.TeamFoundation.VersionControl.Client.Workstation]$workstation = $tfs.vcs.Workstation
#$tfs | add-type "Microsoft.TeamFoundation.VersionControl.Client"
#$itemSpec = new-object $tfss.itemspec("$/foo", $tfss.RecursionType::none)
#$itemSpec
#Add-Type 'Microsoft.TeamFoundation.VersionControl.Client'
#$itemSpecClass = New-Object -TypeName Microsoft.TeamFoundation.VersionControl.Client.Item | Get-Member
#Write-Output $itemSpecClass.FromStrings($serverFiles)
#Write-Output $itemSpecClass
#$client = $tfs.GetService([Microsoft.TeamFoundation.VersionControl.Client])
#$tfs | Get-Member