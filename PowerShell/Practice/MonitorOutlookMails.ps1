Unregister-Event MyNewEmailAlert
$ol = New-Object -ComObject Outlook.Application

$Action = {
	#Write-Output "Mail " | Out-File C:\Docs\PowerShell_Scripts\practice\MonitorOutlookMails.log -Append -width 120 
	$ol1 = New-Object -ComObject Outlook.Application
	$namespace = $ol1.GetNamespace("MAPI")
	#$inbox = $ol1.Session.GetDefaultFolder($ol1.OlDefaultFolders.olFolderInbox)
	#$folder = $ol1.Session.GetDefaultFolder(6).Folders.Item("Inbox")
	#$folder = $ol1.Session.GetDefaultFolder(6).Folders.Item(16).Folders.Item(4)
	#Code Changes
	
	#Write-Output $namespace | Out-File C:\Docs\PowerShell_Scripts\practice\MonitorOutlookMails.log -Append -width 120 
	#Write-Output $inbox | Out-File C:\Docs\PowerShell_Scripts\practice\MonitorOutlookMails.log -Append -width 120 
	#Write-Output $ol1.Session.GetDefaultFolder(6).Folders.Item(16).Folders.Item(4) | Out-File C:\Docs\PowerShell_Scripts\practice\MonitorOutlookMails.log -Append -width 120 
	
	#$folder = $ol1.Session.GetDefaultFolder(6).Folders.Item(16).Folders.Item(4)
	
	$folder = $ol1.Session.GetDefaultFolder(6).Folders.Item(16).Folders.Item(4)
	$item = $folder.Session.GetItemFromID($Event.SourceArgs)
	#Write-Output $item.GetType().FullName | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
	#Write-Output $item | Out-File C:\Docs\PowerShell_Scripts\practice\MonitorOutlookMails.log -Append -width 120 
	
	#Write-Output $item.HTMLBody | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
	#Write-Output $item.Parent | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
	#Write-Output $folder | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 

	# iterate through folders
	#foreach ($folder in $namespace.Folders) {
	#	Write-Output $folder | Out-File C:\Docs\PowerShell_Scripts\practice\MonitorOutlookMails.log -Append -width 120 

	#foreach ($subfolder in $folder.Folders) {
	#	Write-Output " >" $subfolder | Out-File C:\Docs\PowerShell_Scripts\practice\MonitorOutlookMails.log -Append -width 120 
	#}}
	
	#foreach ($subfolder in $folder.Folders) {
	#	Write-Output " >" $subfolder | Out-File C:\Docs\PowerShell_Scripts\practice\MonitorOutlookMails.log -Append -width 120 
	#}

	#Write-Output $item.Parent.StoreID | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
	#Write-Output $folder.StoreID | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
	#Write-Output $item.SenderName | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
	#TFSSERVICE
   #$folder = $ol.Session.GetDefaultFolder(6).Folders.Item("Inbox")
   #$item = $folder.Session.GetItemFromID($Event.SourceArgs)
   #if ($item.Parent.StoreID -eq $folder.StoreID -and $item.SenderEmailAddress -eq 'TFSSERVICE@ideaentity.com') {
   
   # variables
   [string]$workspaceFolder = "C:\Users\sampathv.IE\Documents\Projects\iCrowdIPO"
   [string]$ieTfsUrl = "http://tfs.ideaentity.com:8080/tfs/IE"
   [string]$currentDir = Get-Location
   [string]$gitDir = "C:\Users\sampathv.IE\Documents\Fallback"
   
   # Functions
	function Add-XMLHeader {
	param(
		[System.Object]$item
	)
		[string]$page=''
		$page = '<?xml version="1.0" encoding="UTF-8"?>'
		$page = $page + $item.HTMLBody
		return $page
	}
	function Modify-HTMLString {
	param(
		[string]$HtmlPageAsString
	)
		[string]$pageString=''
		$pageString = $HtmlPageAsString -replace '<br>','<br />'
		$pageString = $pageString -replace '\r\n',''
		$pageString = $pageString -replace 'lang=EN-US',''
		$pageString = $pageString -replace 'link=blue',''
		$pageString = $pageString -replace 'vlink=purple',''
		$pageString = $pageString -replace 'class=([a-z]|[A-Z]|[0-9])+',''
		$pageString = $pageString -replace '&reg;',''
		$pageString = $pageString -replace '&nbsp;','&amp;nbsp;'
		$pageString = $pageString -replace "title=(\w+)(\.)(\w+)(\>)",'title="$1$2$3"$4'
		#$pageString = $pageString -replace 'class=WordSection1',''
		#$pageString = $pageString -replace 'class=MsoNormal',''
		$pageString = $pageString -replace 'border=0',''
		$pageString = $pageString -replace 'cellspacing=0',''
		$pageString = $pageString -replace 'cellpadding=0',''
		$pageString = $pageString -replace 'valign=top',''
		$pageString = $pageString -replace 'nowrap',''
		$pageString = $pageString -replace 'border=1',''
		$pageString = $pageString -replace 'width=0',''
		return $pageString
	}
	function Remove-HeadFromHTML {
	param(
		[string]$HtmlPageAsString
	)
		[string]$page=''
		$page = $HtmlPageAsString
		$pos = $page.IndexOf("<head>")
		$posEnd = $page.IndexOf("</head>")
		$headEnd = $posEnd + 7
		#Write-Output $pageStringLength | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
		#Write-Output $headEnd | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
		$leftPart = $page.Substring(0, $pos)
		$rightPart = $page.Substring($headEnd)
		$page = $leftPart + $rightPart
		#Write-Output $page | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120000000 
		return $page
	}
	function Create-XMLFromHTML {
	param(
		[System.Object]$item,
		[string]$HtmlPageAsString
	)
		$htmlFileXML = "C:\Docs\PowerShell_Scripts\practice\$($item.EntryID).xml"
		Write-Output $HtmlPageAsString | Out-File $htmlFileXML
		return $htmlFileXML
	}
	function Get-InfoFromXML {
	param(
		[string]$htmlFileXML
	)
		[xml]$page = Get-Content $htmlFileXML
	
		#Write-Output $page.html.body.div.table | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
		#Write-Output $page.html.body | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120
		#Write-Output $page | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120
		
		$summary =  $page.html.body.div.table[0]
		$items =  $page.html.body.div.table[1]
		$info = @($summary, $items)
		return $info
	}
	[System.Reflection.Assembly]::LoadWithPartialName("System.Web") | out-null
	function ConvertTo-UrlEncodedString([string]$dataToConvert) {
		begin {
			function EncodeCore([string]$data) { return [System.Web.HttpUtility]::UrlEncode($data) }
		}
		process { if ($_ -as [string]) { EncodeCore($_) } }
		end { if ($dataToConvert) { EncodeCore($dataToConvert) } }
	}
	function ConvertFrom-UrlEncodedString([string]$dataToConvert) {
		begin {
			function DecodeCore([string]$data) { return [System.Web.HttpUtility]::UrlDecode($data) }
		}
		process { if ($_ -as [string]) { DecodeCore($_) } }
		end { if ($dataToConvert) { DecodeCore($dataToConvert) } }
	}
	function ConvertTo-Base64EncodedString([string]$dataToConvert) {
		begin {
			function EncodeCore([string]$data) { return [System.Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($data)) }
		}
		process { if ($_ -as [string]) { EncodeCore($_) } }
		end { if ($dataToConvert) { EncodeCore($dataToConvert) } }
	}
	function ConvertFrom-Base64EncodedString([string]$dataToConvert) {
		begin {
			function DecodeCore([string]$data) { return [System.Text.Encoding]::Unicode.GetString([System.Convert]::FromBase64String($data)) }
		}
		process { if ($_ -as [string]) { DecodeCore($_) } }
		end { if ($dataToConvert) { DecodeCore($dataToConvert) } }
	}
	function Get-ModifiedCodeItems {
	param(
		[hashtable]$codeItems
	)
		$hash = @{}
		foreach($key in $codeItems.keys)
		{
			$codeChangesDetailsProcessedArray = $()
			$url = ConvertFrom-UrlEncodedString -dataToConvert $codeItems[$key][0]
			$uri = [System.Uri]$url
			$query = [System.Web.HttpUtility]::ParseQueryString($uri.Query)
			$mpath = $query.Get_Item("mpath");
			$opath = $query.Get_Item("opath");
			$mcs = $query.Get_Item("mcs");
			$ocs = $query.Get_Item("ocs");
			
			#modified file path
			#modified changeset
			#original file path
			#orignal changeset
			#fullPath
			#change
			#folder
			$codeChangesDetailsProcessedArray = $mpath, $mcs, $opath, $ocs, $codeItems[$key][0], $codeItems[$key][1], $codeItems[$key][2]
			$hash.Add($key, $codeChangesDetailsProcessedArray)
		}
		return $hash
	}
	function Bind-SummaryItems {
	param(
		[array]$summaryItems
	)
		$summary=@{}
		<#
		Security Reviewer: 0
		Checked in by: 1
		Team Project(s): 2
		Checked in on: 3
		Performance Reviewer: 4
		Code Reviewer: 5
		Comment: 6
		#>

		$summary.Add($summaryItems[0],$summaryItems[1])
		$summary.Add($summaryItems[2],$summaryItems[3])
		$summary.Add($summaryItems[4],$summaryItems[5])
		$summary.Add($summaryItems[6],$summaryItems[7])
		$summary.Add($summaryItems[8],$summaryItems[9])
		$summary.Add($summaryItems[10],$summaryItems[11])
		$summary.Add($summaryItems[12],$summaryItems[13])
		return $summary
	}
	function Get-SummaryItemsFromHtml {
	param(
		[System.Xml.XmlElement]$summary
	)
		$summaryArray=@()
		foreach( $summaryItem in $summary.tr)
		{ 
			$summaryTR = $summaryItem
			foreach( $summaryTD in $summaryTR.td)
			{ 
				if($summaryTD.HasChildNodes)
				{
					foreach( $summaryDetails in $summaryTD)
					{ 
						$summaryArray += $summaryDetails.FirstChild.InnerText
					}
				}
			} 
		}
		return $summaryArray
	}
	function Get-CodeItemsFromHtml {
	param(
		[System.Xml.XmlElement]$codeItems
	)
		$countItems = 0
		$hash=@{}
		foreach( $item in $codeItems.tr)
		{ 
			$itemTR = $item
			$codeChangesDetailsArray = @()
			$counter = 0
			foreach( $itemTD in $itemTR.td)
			{ 
				if($itemTD.HasChildNodes)
				{
					if($counter -eq 0)
					{
						$codeChangesDetailsArray += $itemTD.FirstChild.span.a.href
					}
					if($counter -eq 1)
					{
						$codeChangesDetailsArray += $itemTD.FirstChild.InnerText
					}
					if($counter -eq 2)
					{
						$codeChangesDetailsArray += $itemTD.FirstChild.InnerText
					}
				}
				$counter++
			}
			$hash.Add($countItems, $codeChangesDetailsArray)
			$countItems++
		}
		$hash.Remove(0) #remove legend of 'Name, Change, Folder'
		return $hash
	}
	function Get-TfsServer {
	param([string] $serverName = "itstfs")
	 
	# load the required dll
	[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.Client")
	 
	$propertiesToAdd = (
			('VCS', 'Microsoft.TeamFoundation.VersionControl.Client', 'Microsoft.TeamFoundation.VersionControl.Client.VersionControlServer'),
			('WIT', 'Microsoft.TeamFoundation.WorkItemTracking.Client', 'Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItemStore'),
			('CSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.ICommonStructureService'),
			('GSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.IGroupSecurityService')
		)
	 
		# fetch the TFS instance, but add some useful properties to make life easier
		# Make sure to "promote" it to a psobject now to make later modification easier
		[psobject] $tfs = [Microsoft.TeamFoundation.Client.TeamFoundationServerFactory]::GetServer($serverName)
		foreach ($entry in $propertiesToAdd) {
			$scriptBlock = '
				[System.Reflection.Assembly]::LoadWithPartialName("{0}") > $null
				$this.GetService([{1}])
			' -f $entry[1],$entry[2]
			$tfs | add-member scriptproperty $entry[0] $ExecutionContext.InvokeCommand.NewScriptBlock($scriptBlock)
		}
		return $tfs
	}
	function Get-TFSWorkspace {
	param(
		[string] $workspaceFolder,
		[string] $tfsUrl,
	)
		$tfs = Get-TfsServer -serverName $tfsUrl 
		$workspace = $tfs.vcs.GetWorkspace($workspaceFolder)
		return $workspace
	}
	function Create-GetRequests {
	param(
		[hashtable] $itemsProcessedHash
	)
		[array]$GetRequests = @()
		foreach($key in $itemsProcessedHash.keys)
		{
			$codeChangesDetailsProcessedArray = $()
			$codeChangesDetailsProcessedArray = $itemsProcessedHash[$key]
			#modified file path
			#modified file path
			#original file path
			#original changeset
			#fullPath
			#change
			#folder
			$mpath = $codeChangesDetailsProcessedArray[0]
			$mcs = $codeChangesDetailsProcessedArray[1]
			$opath = $codeChangesDetailsProcessedArray[2]
			$ocs = $codeChangesDetailsProcessedArray[3]
			$fullpath = $codeChangesDetailsProcessedArray[4]
			$change = $codeChangesDetailsProcessedArray[5]
			$folder = $codeChangesDetailsProcessedArray[6]
		
			$ItemSpec = New-Object -TypeName Microsoft.TeamFoundation.VersionControl.Client.ItemSpec -ArgumentList $opath, 'None'
			$GetRequest = New-Object -TypeName Microsoft.TeamFoundation.VersionControl.Client.GetRequest -ArgumentList $ItemSpec, $mcs
			$GetRequests += $GetRequest
		}
		return $GetRequests
	}
	function Do-GetRequests {
	param(
		[array] $getRequests,
		[Microsoft.TeamFoundation.VersionControl.Client.Workspace] $workspace
	)
		foreach($getRequest in $getRequests)
		{
			$workspace.Get($getRequest,'Overwrite')
		}
	}
	
   
   [string]$pageString = ''
   if ($item.Parent.StoreID -eq $folder.StoreID -and $item.SenderName -eq 'Sampath Vangari') {
		Write-Output "Entered if " | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
		Try
		{
			#Write-Output $item | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
			
			# variables
			$summaryArray = @()
			$summaryHash = @{}
			$itemsArray = @()
			$itemsHash = @{}
			$itemsProcessedHash=@{}
			$commitMessage = @{}
			$GetRequests = @()
			
			#convert item.HTMLBody to a valid xml so that we can traverse through the object
			[string]$page = Add-XMLHeader -item $item
			[string]$page = Modify-HTMLString -HtmlPageAsString $page
			[string]$page = Remove-HeadFromHTML -HtmlPageAsString $page
			[string]$htmlFileXML = Create-XMLFromHTML -item $item -HtmlPageAsString $page
			[array]$info = Get-InfoFromXML -htmlFileXML $htmlFileXML
			
			#getting System.xml.XMLElement from array
			$summary = $info[0]
			$items = $info[1]
			
			# Process the html data and convert it into information
			$summaryArray = Get-SummaryItemsFromHtml -summary $summary
			$summaryHash = Bind-SummaryItems -summaryItems $summaryArray
			$itemsHash = Get-CodeItemsFromHtml -codeItems $items
			$itemsProcessedHash = Get-ModifiedCodeItems -codeItems $itemsHash

			Write-Output $summaryHash | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
			Write-Output $itemsProcessedHash | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
			
			# Create commit message
			$commitMessage = $summaryHash
			$commitMessage.Add('Changeset',$itemsProcessedHash.Get_Item(1)[1])
			[string]$commitMsg = Write-Output $commitMessage | Out-String
			Write-Output $commitMsg
			
			#Load TFS Client so that we get necessary classes loaded to get files from TFS
			[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.VersionControl.Client")
			
			#Iterate files in $itemsProcessedHash to construct getRequests which we can make on workspace to get all those files updated
			$GetRequests = Create-GetRequests -itemsProcessedHash $itemsProcessedHash
			
			#Get mapped workspace for predefined folder
			$workspace = Get-TFSWorkspace -workspaceFolder $workspaceFolder -tfsUrl $ieTfsUrl
			
			#Do Get Requests from the created get requests array
			Do-GetRequests -getRequests $GetRequests -workspace $workspace
			
			#Goto git directory to execute git commands
			Set-Location $gitDir 
			
			#After files are updated then commit to git with the commit message
			git commit -a -m $commitMsg
			
			#Revert the location to source so that we can handle other requests
			Set-Location $currentDir 
			
			#Remove the xml file which is of no use now
			Remove-Item $htmlFileXML -ErrorAction SilentlyContinue
		}
		Catch
		{
			Write-Output "Error : $($_.Exception.Message)"  | Out-File C:\Docs\PowerShell_Scripts\practice\HtmlTest.txt -Append -width 120 
			Set-Location $currentDir 
		}
   }
}

$null = Register-ObjectEvent $ol NewMailEx -SourceIdentifier MyNewEmailAlert -Action $Action


<#
#Example of register-objectevent
#$query = New-Object System.Management.WqlEventQuery "__InstanceCreationEvent", (New-Object TimeSpan 0,0,1), "TargetInstance isa 'Win32_Process'"
#$processWatcher = New-Object System.Management.ManagementEventWatcher $query
#register-objectEvent -inputObject $processWatcher -eventName "EventArrived"


# write to this file in appending mode 
#$BalloonTipText = $item.SenderEmailAddress, $item.SentOn, $item.Subject -join "`t"
#$BalloonTipText = "james Bennett"
#Write-Host $BalloonTipText -BackgroundColor Yellow -ForegroundColor DarkBlue
#Add-Type -AssemblyName System.Windows.Forms
#$objNotifyIcon = New-Object System.Windows.Forms.NotifyIcon
#$objNotifyIcon.Icon = "C:\Windows\System32\PerfCenterCpl.ico"
#$objNotifyIcon.BalloonTipIcon = "Info"
#$objNotifyIcon.BalloonTipText = $BalloonTipText
#$objNotifyIcon.BalloonTipTitle = "Email Alert"
#$objNotifyIcon.Visible = $True
#$objNotifyIcon.ShowBalloonTip(20000)
#>