#Get applications which are installed
#Get-WmiObject -Class Win32_Product -Computer  wave | Sort-object Name | select Name

#Searching installed applications by application name
#Get-WmiObject -Class Win32_Product | sort-object Name | select Name | where { $_.Name -match �MySQL�}

#Get-WmiObject -Class Win32_Product | sort-object Name | select Name | where { $_.Name -match "MySql Server"}

$computers = "wave"

$array = @()
$found = $false
$Search = 'MySQL Server 5.5'
foreach($pc in $computers){

    $computername=$pc.computername

    #Define the variable to hold the location of Currently Installed Programs

    $UninstallKey="SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall" 

    #Create an instance of the Registry Object and open the HKLM base key

    $reg=[microsoft.win32.registrykey]::OpenRemoteBaseKey('LocalMachine',$computername) 

    #Drill down into the Uninstall key using the OpenSubKey Method

    $regkey=$reg.OpenSubKey($UninstallKey) 

    #Retrieve an array of string that contain all the subkey names

    $subkeys=$regkey.GetSubKeyNames() 

    #Open each Subkey and use GetValue Method to return the required values for each

    foreach($key in $subkeys){

        $thisKey=$UninstallKey+"\\"+$key 

        $thisSubKey=$reg.OpenSubKey($thisKey) 
		
		$ProgramName =  $thisSubKey.getValue("DisplayName")
		#$InstallLocation = $thisSubKey.getValue("InstallLocation")
		#write-output $ProgramName $InstallLocation
		if($ProgramName -contains $Search)
		{
			$found = $true
			$InstallLocation = $thisSubKey.getValue("InstallLocation")
		}
		
		#if (-not $thisSubKey.getValue("DisplayName")) { continue }

        #$obj = New-Object PSObject

        #$obj | Add-Member -MemberType NoteProperty -Name "ComputerName" -Value $computername

        #$obj | Add-Member -MemberType NoteProperty -Name "DisplayName" -Value $($thisSubKey.GetValue("DisplayName"))

        #$obj | Add-Member -MemberType NoteProperty -Name "DisplayVersion" -Value $($thisSubKey.GetValue("DisplayVersion"))

        #$obj | Add-Member -MemberType NoteProperty -Name "InstallLocation" -Value $($thisSubKey.GetValue("InstallLocation"))

        #$obj | Add-Member -MemberType NoteProperty -Name "Publisher" -Value $($thisSubKey.GetValue("Publisher"))

        #$array += $obj

    } 

}
write-output $InstallLocation
write-output $found

$array | Where-Object { $_.DisplayName } | select ComputerName, DisplayName, InstallLocation,DisplayVersion, Publisher | ft -auto