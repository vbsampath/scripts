Get-ChildItem -Path C:\Docs\* -Recurse | Where-Object {$_.fullname -notmatch 'C:\\Docs\\BitBucket\\'} | Where-Object  {
   $_.LastWriteTime.ToShortDateString() -eq (Get-Date).ToShortDateString() 
   #-or $_.CreatedTime.ToShortDateString() -eq (Get-Date).ToShortDateString()
}