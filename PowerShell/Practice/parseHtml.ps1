<#
$page = Invoke-WebRequest "http://www.apk.se"
$html = $page.parsedHTML
$products = $html.body.getElementsByTagName("TR")
$headers = @()
foreach($product in $products)
{
    $colID = 0;
    $hRow = $false
    $returnObject = New-Object Object
    foreach($child in $product.children)
    {  
        if ($child.tagName -eq "TH")
        {
            $headers += @($child.outerText)
            $hRow = $true
        }
 
        if ($child.tagName -eq "TD")
        {
            $returnObject | Add-Member NoteProperty $headers[$colID] $child.outerText
        }
        $colID++
 
    }
    if (-not $hRow) { $returnObject }
}


$htmlDoc= New-Object -com "HTMLFILE"
$page = "<html><body>this is a test</body></html>"
$htmlDoc.write($page)
$htmlDoc.close

#>

<#
#get-date | convertto-xml
#$page = "<html><body>this is a test</body></html>"
#$emailXml = $page | convertto-xml
#Write-Output $emailXml.Objects.Object
#$states.Add("Alaska", "Fairbanks")
#Write-Output $summary | Select-XML -Expand Node
#Write-Output $summary[0].td[0]
#Write-Output $summary[0].td[0]
#Write-Output $summary
#Write-Output $summaryDetails."#text"
#Write-Output $summaryDetails.InnerXml
#$uri = new Uri("http://domain.test/Default.aspx?var1=true&var2=test&var3=3");
#$query = HttpUtility.ParseQueryString(uri.Query);
#Write-Output $itemsHash[$key][0] #path
#Write-Output $url
#$query = Get-ParseQueryString($uri.Query);
#Write-Output $itemsHash[$key][1] #change
#Write-Output $itemsHash[$key][2] #folder
#[xml]$page = $pageString
#>

[xml]$page = Get-Content valid_xhtml.xml

# initializations
$summary =  $page.html.body.table[0]
$items =  $page.html.body.table[1]

# variables
$summaryArray = @()
$summaryHash = @{}
$itemsArray = @()
$itemsHash = @{}
$itemsProcessedHash=@{}

# Functions

[System.Reflection.Assembly]::LoadWithPartialName("System.Web") | out-null
function ConvertTo-UrlEncodedString([string]$dataToConvert)
{
    begin {
        function EncodeCore([string]$data) { return [System.Web.HttpUtility]::UrlEncode($data) }
    }
    process { if ($_ -as [string]) { EncodeCore($_) } }
    end { if ($dataToConvert) { EncodeCore($dataToConvert) } }
}
function ConvertFrom-UrlEncodedString([string]$dataToConvert)
{
    begin {
        function DecodeCore([string]$data) { return [System.Web.HttpUtility]::UrlDecode($data) }
    }
    process { if ($_ -as [string]) { DecodeCore($_) } }
    end { if ($dataToConvert) { DecodeCore($dataToConvert) } }
}
function ConvertTo-Base64EncodedString([string]$dataToConvert)
{
    begin {
        function EncodeCore([string]$data) { return [System.Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($data)) }
    }
    process { if ($_ -as [string]) { EncodeCore($_) } }
    end { if ($dataToConvert) { EncodeCore($dataToConvert) } }
}
function ConvertFrom-Base64EncodedString([string]$dataToConvert)
{
    begin {
        function DecodeCore([string]$data) { return [System.Text.Encoding]::Unicode.GetString([System.Convert]::FromBase64String($data)) }
    }
    process { if ($_ -as [string]) { DecodeCore($_) } }
    end { if ($dataToConvert) { DecodeCore($dataToConvert) } }
}
function Get-ModifiedCodeItems
{param(
	[hashtable]$codeItems
)
	$hash = @{}
	foreach($key in $codeItems.keys)
	{
		$codeChangesDetailsProcessedArray = $()
		$url = ConvertFrom-UrlEncodedString -dataToConvert $codeItems[$key][0]
		$uri = [System.Uri]$url
		$query = [System.Web.HttpUtility]::ParseQueryString($uri.Query)
		$mpath = $query.Get_Item("mpath");
		$opath = $query.Get_Item("opath");
		$mcs = $query.Get_Item("mcs");
		$ocs = $query.Get_Item("ocs");
		#modified file path
		#modified changeset
		#fullPath
		#change
		#folder
		$codeChangesDetailsProcessedArray = $mpath, $mcs, $codeItems[$key][0], $codeItems[$key][1], $codeItems[$key][2]
		$hash.Add($key, $codeChangesDetailsProcessedArray)
	}
	return $hash
}
function Bind-SummaryItems
{param(
	[array]$summaryItems
)
	$summary=@{}
	<#
	Security Reviewer: 0
	Checked in by: 1
	Team Project(s): 2
	Checked in on: 3
	Performance Reviewer: 4
	Code Reviewer: 5
	Comment: 6
	#>

	$summary.Add($summaryItems[0],$summaryItems[1])
	$summary.Add($summaryItems[2],$summaryItems[3])
	$summary.Add($summaryItems[4],$summaryItems[5])
	$summary.Add($summaryItems[6],$summaryItems[7])
	$summary.Add($summaryItems[8],$summaryItems[9])
	$summary.Add($summaryItems[10],$summaryItems[11])
	$summary.Add($summaryItems[12],$summaryItems[13])
	return $summary
}
function Get-SummaryItemsFromHtml
{param(
	[System.Xml.XmlElement]$summary
)
	$summaryArray=@()
	foreach( $summaryItem in $summary.tr)
	{ 
		$summaryTR = $summaryItem
		foreach( $summaryTD in $summaryTR.td)
		{ 
			if($summaryTD.HasChildNodes)
			{
				foreach( $summaryDetails in $summaryTD)
				{ 
					$summaryArray += $summaryDetails.FirstChild.InnerText
				}
			}
		} 
	}
	return $summaryArray
}
function Get-CodeItemsFromHtml
{param(
	[System.Xml.XmlElement]$codeItems
)
	$countItems = 0
	$hash=@{}
	foreach( $item in $items.tr)
	{ 
		$itemTR = $item
		$codeChangesDetailsArray = @()
		$counter = 0
		foreach( $itemTD in $itemTR.td)
		{ 
			if($itemTD.HasChildNodes)
			{
				if($counter -eq 0)
				{
					$codeChangesDetailsArray += $itemTD.FirstChild.href
				}
				if($counter -eq 1)
				{
					$codeChangesDetailsArray += $itemTD.FirstChild.InnerText
				}
				if($counter -eq 2)
				{
					$codeChangesDetailsArray += $itemTD.FirstChild.InnerText
				}
			}
			$counter++
		}
		$hash.Add($countItems, $codeChangesDetailsArray)
		$countItems++
	}
	$hash.Remove(0) #remove legend of 'Name, Change, Folder'
	return $hash
}


# Process the html data and convert it into information
$summaryArray = Get-SummaryItemsFromHtml -summary $summary
$summaryHash = Bind-SummaryItems -summaryItems $summaryArray
$itemsHash = Get-CodeItemsFromHtml -codeItems $items
$itemsProcessedHash = Get-ModifiedCodeItems -codeItems $itemsHash

Write-Output $summaryHash
Write-Output $itemsProcessedHash


