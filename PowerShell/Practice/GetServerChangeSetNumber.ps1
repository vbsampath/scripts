#Set-Location -Path  
try
{
	$filePath = $args[0]
	$exepath = "C:\Program Files\Microsoft Visual Studio 11.0\Common7\IDE\TF.exe"
	$result = & $exepath info $filePath
	$matches = ($result | select-string "\d+")
	$tokens =  $matches[1];
	$tokens = $tokens.ToString().split(':')
	Write-Output $tokens[1].trim();
}
Catch
{
	#Write-Error "Error : $($_.Exception.Message)" 
	Write-Host "Error : $($_.Exception.Message)"  -ForegroundColor red
} 