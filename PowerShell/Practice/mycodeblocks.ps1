function Get-MyCodeBlocks
{
<# 
    .SYNOPSIS 
    Displays a list of all defined functions, aliases and other stuff in user modules
 
    .DESCRIPTION 
    Displays a list of all defined functions, aliases and other stuff in user modules. 
	The only dependency is that we need to have PSModulePath defined for user because we are getting only user defined stuff here
 	
    .EXAMPLE 
		PS C:\>Get-MyCodeBlocks

		Description
		-----------
		Returns list of all available functions, aliases and other stuff defined in user modules
    
	.OUTPUTS
		System.Object
		
    .NOTES 
		Function Name  : Get-MyCodeBlocks
		Author     : Sampath Vangari - vbsampath@gmail.com
		Requires   : PowerShell V2 CTP3   
		Date		: 14-June-2014
#> 

	[string]$modulePath = [Environment]::GetEnvironmentVariable("PSModulePath", "User")
	[System.Array]$modulesArray=$null
	Get-ChildItem $modulePath | 
	Foreach-Object{
		$moduleName = $_.Name
		#$moduleLastWrittenTime = $_.LastWrittenTime
		$modulesArray += $moduleName
	}
	$modulesString = [string]::join(',', $modulesArray)
	$getCommandObjects = "Get-Command -Module $modulesString" | Invoke-Expression
	$getCommand=@()
	foreach($getCommandObject in $getCommandObjects)
	{
		$object = New-Object System.Object
		$object | Add-Member -Type NoteProperty �Name CommandType �Value $getCommandObject.CommandType
		$object | Add-Member -Type NoteProperty �Name ModuleName �Value $getCommandObject.ModuleName
		$object | Add-Member -Type NoteProperty �Name Name �Value $getCommandObject.Name
		$object | Add-Member -Type NoteProperty �Name Parameters �Value $null
		if($getCommandObject.CommandType -eq "Function")
		{
			$commandParameterSetInfos = $getCommandObject.ParameterSets[0] | select -ExpandProperty parameters | Where-Object {($_.Position -ge 0)}
			if($commandParameterSetInfos -ne $null)
			{
				[System.Array]$paramterArray=$null
				foreach($commandParameterSetInfo in $commandParameterSetInfos)
				{
					$value = $("{"+$commandParameterSetInfo.Name + " - "+$commandParameterSetInfo.ParameterType.FullName + "}")
					$paramterArray+=$value
				}
				$parametersString =  [string]::join(", ",$paramterArray)
				$object.Parameters = $parametersString
			}
		}
		$getCommand += $object
	}
	Write-Output $getCommand | Sort-Object CommandType | Format-Table -Autosize
}
Get-MyCodeBlocks


