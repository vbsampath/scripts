

[string]$global:requestToken=""
[System.Net.CookieContainer]$global:cookieContainer=$null

Function DoLogin{
[CmdLetBinding()]
param(
	  $url,
	  $postData
 )
	$bytesToPost = [System.Text.Encoding]::UTF8.GetBytes($postData)
	$cookieContainer = New-object System.Net.CookieContainer
	$webRequest = [System.Net.WebRequest]::Create($url)
	$webRequest.Method = "POST"
	$webRequest.ContentType = "application/x-www-form-urlencoded"
	#$webRequest.Headers.Add("X_API_VERSION","1.5")
	$webRequest.ContentLength = $bytesToPost.Length
	$webRequest.PreAuthenticate = $false;
	$webRequest.ServicePoint.Expect100Continue = $false
	$webRequest.CookieContainer = $cookieContainer
	$requestStream = $webRequest.GetRequestStream()
	$requestStream.Write($bytesToPost, 0, $bytesToPost.Length)
	$requestStream.Close()
	[System.Net.WebResponse]$response = $webRequest.GetResponse()
	$responseStream = $response.GetResponseStream()
	$responseStreamReader = New-Object System.IO.StreamReader -ArgumentList $responseStream
	[string]$responseString = $responseStreamReader.ReadToEnd()
	$regex = "\<head\sdata-user\=\""admin\""\sdata-requesttoken\=\""\w+\""\>"
	[System.Array]$matches=$null
	$found = $responseString -Match $regex
	if($found -eq $True)
	{
		$matches
	}
	if($matches -ne $null)
	{
		$tokenString = $matches[0].Get_Item(0)
		$splittedToken = [regex]::Split($tokenString, "\""")
		$global:requestToken = $splittedToken[3]
	}
	$global:cookieContainer = $cookieContainer
	
	#Return $cookieContainer
}


Function DeleteBookmarks{
[CmdLetBinding()]
 param(
	  $url,
	  $bookmarkId, 
	  $authenticationData,
	  $requestToken
 )
	$stringToPost = "id=$bookmarkId"
	$bytesToPost = [System.Text.Encoding]::UTF8.GetBytes($stringToPost)
	$webRequest = [System.Net.WebRequest]::Create($url)
	$webRequest.Method = "POST"
	$webRequest.ContentType = "application/x-www-form-urlencoded"
	$webRequest.Headers.Add("requesttoken","$requestToken")
	$webRequest.ContentLength = $bytesToPost.Length
	$webRequest.PreAuthenticate = $true;
	$webRequest.ServicePoint.Expect100Continue = $false
	$webRequest.CookieContainer = $authenticationData
	$requestStream = $webRequest.GetRequestStream()
	$requestStream.Write($bytesToPost, 0, $bytesToPost.Length)
	$requestStream.Close()
	[System.Net.WebResponse]$response = $webRequest.GetResponse()
	$responseStream = $response.GetResponseStream()
	$responseStreamReader = New-Object System.IO.StreamReader -ArgumentList $responseStream
	[string]$responseString = $responseStreamReader.ReadToEnd()
	Return $responseString
}


DoLogin -url "http://sampathv.owncloud.arvixe.com/index.php" -postData "user=admin&password=&timezone-offset=5.5"


for($i=15;$i -lt 812; $i++)
{
	$response = DeleteBookmarks -url "http://sampathv.owncloud.arvixe.com/index.php/apps/bookmarks/ajax/delBookmark.php" -bookmarkId $i -authenticationData $cookieContainer -requestToken $global:requestToken
	Write-Output $response
}
