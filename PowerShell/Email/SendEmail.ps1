#Verify if the required arguments are passed
if($args.count -ne 4)
{
write-output 'Usage:'
write-output 'Syntax: "From address" "To address" "Subject" "Body"'
write-output 'Example: "xyz@domain.com" "abc@domain.com" "This is test" "Sent from ps script"'
write-output 'Additional Information: To addresses can be seperated by commas, Body is optional but one need to pass null argument'
}
if($args.count -eq 4)
{
	#Assigning arguments to variable for future usage

	$From = $args[0];
	$To = $args[1];
	$Subject = $args[2];
	$Body = $args[3];
	$IsBodyEmpty = $False;
	$SmtpServer = "mail.ideaentity.com"
	#Validating parameters
	
	if($From -eq '')
	{
		write-output "From address cannot be empty. Please try again with a valid email address"
	}
	Elseif($To -eq '')
	{
		write-output "To address cannot be empty. Please try again with a valid email address"
	}
	Elseif($Subject -eq '')
	{
		write-output "Subject cannot be empty. Please try again with a valid Subject"
	}
	Elseif($Body -eq '')
	{
		$IsBodyEmpty = $True;
	}
	if($IsBodyEmpty)
	{
		send-mailmessage -from $From -to $To -subject $Subject -smtpServer $SmtpServer
	}
	elseif($IsBodyEmpty -eq $False)
	{
		send-mailmessage -from $From -to $To -subject $Subject -body $Body -smtpServer $SmtpServer
	}
}

<#
$emailFrom = "sampath.vangari@ideaentity.com"
$emailTo = "sampath.vangari@ideaentity.com"
$subject = "test"
$body = "sent email from ps"
$smtpServer = "mail.ideaentity.com"
$smtp = new-object Net.Mail.SmtpClient($smtpServer)
$smtp.Send($emailFrom, $emailTo, $subject, $body)

$c = Get-Credential
write-output $c

send-mailmessage -from "User01 <user01@example.com>" -to "User02 <user02@example.com>", "User03 <user03@example.com>" -subject "Sending the Attachment" -body "Forgot to send the attachment. Sending now." -Attachments "data.csv" -priority High -dno onSuccess, onFailure -smtpServer smtp.fabrikam.com -credential ie\sampathv -useSSL -BodyAsHtml
#>