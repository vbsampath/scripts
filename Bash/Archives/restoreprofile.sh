#!/bin/bash

#close firefox
    killall firefox

    zenity --info --text "Make sure Firefox is closed before proceeding" &&

    #Input variables
    PROFILE=$(cat $HOME/.mozilla/firefox/profile.txt)

if test -z "$(mount | grep -F "${HOME}/.mozilla/firefox/${PROFILE}" )"
then
    zenity --info --text "The profile is not mounted. The script will check if the fstab has Firefox entries..." &&
    if test -z "$(cat /etc/fstab | grep -F "firefox" )"
    then
        zenity --info --text "The fastab has no Firefox entries. It seems all necessary steps have been taken to uload the profile from RAM, but you should check your profile folders to see if everthing is in place."
    else
        zenity --info --text "The script will try to restore the fstab, remove temporary files, clear cron jobs and restore your profile, but you should check your profile folders to see if everthing is in place."
        #remove cron jobs
        echo "" > ${HOME}/.mozilla/firefox/.change.cron
        crontab ${HOME}/.mozilla/firefox/.change.cron

        #delete temporary files, restore the profile to disk and restore fstab
        rm -fr ${HOME}/.mozilla/firefox/${PROFILE}
        rm -f ${HOME}/.mozilla/firefox/.change.cron
        rm -f ${HOME}/.mozilla/firefox/profile.txt
        rm -f ${HOME}/.mozilla/firefox/profileselect.sh
        mv ${HOME}/.mozilla/firefox/ramprofile ${HOME}/.mozilla/firefox/${PROFILE}
        sudo mv /etc/fstab.bak /etc/fstab
        zenity --info --text "Profile successfuly unmounted. You might need to restore a profile from a backup."
    fi
else
    if test -z "$(cat /etc/fstab | grep -F "firefox" )"
    then
       zenity --info --text "The fastab has no Firefox entries, but the profile is mounted. The script will try to fix this..."

       #backup and modify fstab based on selected variables
       sudo cp /etc/fstab /etc/fstab.bak
       cat /etc/fstab > ${HOME}/.mozilla/firefox/fstab
       echo "firefox ${HOME}/.mozilla/firefox/${PROFILE} tmpfs size=200M,noauto,user,exec,uid=1000,gid=1000 0 0" >> ${HOME}/.mozilla/firefox/fstab
       sudo mv ${HOME}/.mozilla/firefox/fstab /etc/fstab

       #remove the profile from RAM
       umount "${HOME}/.mozilla/firefox/${PROFILE}"

       #remove cron jobs
       echo "" > ${HOME}/.mozilla/firefox/.change.cron
       crontab ${HOME}/.mozilla/firefox/.change.cron

       #delete temporary files, restore the profile to disk and restore fstab
       rm -fr ${HOME}/.mozilla/firefox/${PROFILE}
       rm -f ${HOME}/.mozilla/firefox/.change.cron
       rm -f ${HOME}/.mozilla/firefox/profile.txt
       rm -f ${HOME}/.mozilla/firefox/profileselect.sh
       mv ${HOME}/.mozilla/firefox/ramprofile ${HOME}/.mozilla/firefox/${PROFILE}
       sudo mv /etc/fstab.bak /etc/fstab

       zenity --info --text "Profile successfuly unmounted. You might need to restore a profile from a backup."

    else
        #remove cron jobs
        echo "" > ${HOME}/.mozilla/firefox/.change.cron
        crontab ${HOME}/.mozilla/firefox/.change.cron

        #sync the profile from RAM to disk
        rsync -av -p -o -g --delete ${HOME}/.mozilla/firefox/${PROFILE}/ ${HOME}/.mozilla/firefox/ramprofile/
    
        #remove the profile from RAM
        umount "${HOME}/.mozilla/firefox/${PROFILE}"

        #delete temporary files, restore the profile to disk and restore fstab
        rm -fr ${HOME}/.mozilla/firefox/${PROFILE}
        rm -f ${HOME}/.mozilla/firefox/.change.cron
        rm -f ${HOME}/.mozilla/firefox/profile.txt
        rm -f ${HOME}/.mozilla/firefox/profileselect.sh
        mv ${HOME}/.mozilla/firefox/ramprofile ${HOME}/.mozilla/firefox/${PROFILE}
        sudo mv /etc/fstab.bak /etc/fstab

        #optimize profile databases
        find ${HOME}/.mozilla/firefox/${PROFILE} \( -name "*.sqlite" \) -exec sqlite3  {} "vacuum" \;

        zenity --info --text "Profile ${PROFILE} was successfully unloaded."
    fi
fi
exit
