#!/bin/bash
export WINEPREFIX=${HOME}/.myphoneexplorer
# Create the myphoneexplorer wine drive
if [ ! -d ${WINEPREFIX} ]; then
   echo --- Creating WINE prefix ${WINEPREFIX}...
   wineprefixcreate > myphoneexplorer.log 2>&1
fi
if [ ! -f "MyPhoneExplorer_wine.zip" ]; then
	echo ---  download http://www.fjsoft.at/files/MyPhoneExplorer_wine.zip...
	wget "http://www.fjsoft.at/files/MyPhoneExplorer_wine.zip"
fi
echo --- Download VB6 runtimes
if [ ! -f "vbrun60sp6.exe" ]; then
	wget "http://download.microsoft.com/download/5/a/d/5ad868a0-8ecd-4bb0-a882-fe53eb7ef348/VB6.0-KB290887-X86.exe"
#	rm VB6.0-KB290887-X86.exe
	cabextract -F vbrun60sp6.exe VB6.0-KB290887-X86.exe vbrun60sp6.exe
fi
echo -e REGEDIT4\\n\\n[HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides]\\n\"asycfilt\"=\"native\"\\n\"comcat\"=\"native\"\\n\"msvbvm60\"=\"native\"\\n\"oleaut32\"=\"native\"\\n\"oleaut32\"=\"native\"\\n\"olepro32\"=\"native\"\\n\"stdole2.tlb\"=\"native\"\\n[HKEY_CURRENT_USER\\Software\\MyPhoneExplorer]\\n\"Language\"=\"English.lng\" > temp.reg
wine regedit temp.reg
mv ${WINEPREFIX}/drive_c/windows/system32/oleaut32.dll ${WINEPREFIX}/drive_c/windows/system32/oleaut32-alt.dll
mv ${WINEPREFIX}/drive_c/windows/system32/olepro32.dll ${WINEPREFIX}/drive_c/windows/system32/olepro32-alt.dll
wine vbrun60sp6.exe /Q >> myphoneexplorer.log 2>&1
unzip -q MyPhoneExplorer_wine.zip -d ${WINEPREFIX}/drive_c
wine regsvr32 c:/MyPhoneExplorer/DLL/ccrpDtp6.ocx
wine regsvr32 c:/MyPhoneExplorer/DLL/ccrpUCW6.dll
wine regsvr32 c:/MyPhoneExplorer/DLL/ShellMgr.dll
wine regsvr32 c:/MyPhoneExplorer/DLL/SSubTmr6.dll
wine regsvr32 c:/MyPhoneExplorer/DLL/vbalExpBar6.ocx
wine regsvr32 c:/MyPhoneExplorer/DLL/vbalIml6.ocx
wine regsvr32 c:/MyPhoneExplorer/DLL/vbalSGrid6.ocx
wine regsvr32 c:/MyPhoneExplorer/comctl32.ocx
wine regsvr32 c:/MyPhoneExplorer/MSCOMM32.OCX
ln -is /dev/ttyACM0 ${WINEPREFIX}/dosdevices/com0
#ln -is /dev/rfcomm0 ${WINEPREFIX}/dosdevices/com2
sudo chmod 777 ${WINEPREFIX}/dosdevices/com0 #${WINEPREFIX}/dosdevices/com2
#Desktop Icon...
echo -e [Desktop Entry]\\nEncoding=UTF-8\\nType=Application\\nTerminal=false\\nName[ru_RU]=My Phone Explorer\\nExec=env WINEPREFIX=${WINEPREFIX} wine c:/MyPhoneExplorer/MyPhoneExplorer.exe\\nName=My Phone Explorer\\nIcon=20e3_myphoneexplorer.0 > ~/Desktop/MyPhoneExplorer
chmod 777 ~/Desktop/MyPhoneExplorer
