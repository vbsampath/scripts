#!/bin/sh
#
# SCRIPT: shifting.sh
#
# AUTHOR: Randy Michael
#
# DATE:   01-22-1999
#
# REV:    1.1.A
#
# PLATFORM: Not platform dependent
#
# PURPOSE: This script is used to process all of the tokens which
# Are pointed to by the command-line arguments, $1, $2, $3,etc...
#
# REV. LIST:
#
#
# Initialize all variables
COUNT=0          # Initialize the counter to zero
NUMBER=$#          # Total number of command-line arguments to process
#  Start a while loop
while [ $COUNT -lt $NUMBER ]
do
     COUNT=`expr $COUNT + 1`  # A little math in the shell script
     TOKEN=’$’$COUNT         # Loops through each token starting with $1
                           process each $TOKEN
     shift                 # Grab the next token, i.e. $2 becomes $1
done

