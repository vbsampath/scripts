#!/bin/bash
#Input variables
    PROFILE=$(cat $HOME/.mozilla/firefox/profile.txt)

if test -z "$(mount | grep -F "${HOME}/.mozilla/firefox/${PROFILE}" )"
then
    zenity --info --text "The profile is not mounted."
else
    #sync the profile from RAM to disk
    rsync -av -p -o -g --delete ${HOME}/.mozilla/firefox/${PROFILE}/ ${HOME}/.mozilla/firefox/ramprofile/
    zenity --info --text "Profile ${PROFILE} in RAM was successfully synchronized with disk copy."
fi
exit
