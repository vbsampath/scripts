#!/bin/sh
pkill -f gtk_desktop_info.py
rm -f /tmp/*.jpg
rm -f /tmp/.plugin_forecast*

sleep 20

#weather forecast
gtk-desktop-info --allworkspaces --logfile=/tmp/gtk-desktop-info.log --interval=1800 --width=510 --height=300 --x=-5 --y=691 --plugin=forecast --css=/usr/share/gtk-desktop-info/style/plugin_forecast_custom.css --noheader &

#deluge
gtk-desktop-info --allworkspaces --logfile=/tmp/gtk-desktop-info.log --interval=1 --width=270 --height=455 --x=696 --y=0 --plugin=deluge --css=/usr/share/gtk-desktop-info/style/plugin_deluge_custom.css &

#rhythmbox
gtk-desktop-info --allworkspaces --logfile=/tmp/gtk-desktop-info.log --interval=1 --width=270 --height=164 --x=696 --y=456 --plugin=rhythmbox --css=/usr/share/gtk-desktop-info/style/plugin_rhythmbox_custom.css --noheader &

