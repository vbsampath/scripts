#!/bin/sh
#
# SCRIPT: shifting.sh
# AUTHOR: Randy Michael
# DATE:   01-22-1999
# REV:    1.1.A
# PLATFORM: Not platform dependent
#
# PURPOSE: This script is used to process all of the tokens which
# Are pointed to by the command-line arguments, $1, $2, $3, etc... -
#
# REV LIST:
#
#
#  Start a for loop
for TOKEN in $*
do
         process each $TOKEN
done

