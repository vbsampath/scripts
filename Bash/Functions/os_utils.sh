#!/bin/bash

# Get OS architecture
get_os_architecture() {
	#local  __resultvar=$1
	local ARCH=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/');
	#if [[ "$__resultvar" ]]; then
	#	eval $__resultvar="'$ARCH'"
	#else
	echo "$ARCH"
	#fi
}


# Get os type
os_type() {
local os;
case `uname` in
  Linux )
     LINUX=1
	if [ -f /usr/bin/yum ]; then
	os='centos';
	echo "$os"
	fi
	if [ -f /usr/bin/apt-get ]; then
	os='debian';
	echo "$os"
	fi
esac
}
