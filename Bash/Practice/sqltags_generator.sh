#!/bin/sh
# Program to create ctags for ESQL, C++ and C files
ESQL_EXTN=pc
tag_file1=tags_file.1
tag_file2=tags_file.2
which_tag=ctags
rm -f $tag_file1 $tag_file2 tags
aa=`ls *.$ESQL_EXTN`
#echo $aa
for ii in $aa 
do
	#echo $ii
	jj=`echo $ii | cut -d'.' -f1`
	#echo $jj
	if [ ! -f $jj.cpp ]; then
		echo " "
		echo " "
		echo "***********************************************"
		echo "ESQL *.cpp files does not exist.. "
		echo "You must generate the *.cpp from *.pc file"
		echo "using the Oracle Pro*C pre-compiler or Sybase"
		echo "or Informix esql/c pre-compiler."
		echo "And then re-run this command"
		echo "***********************************************"
		echo " "
		exit
	fi
	rm -f tags
	$which_tag $jj.cpp
	kk=s/$jj\.cpp/$jj\.pc/g
	#echo $kk > sed.tmp
	#sed -f sed.tmp tags >> $tag_file1
	#sed -e's/sample\.cpp/sample\.pc/g' tags >> $tag_file1
	sed -e $kk tags >> $tag_file1
done
# Now handle all the C++/C files - exclude the ESQL *.cpp files
rm -f tags $tag_file2
bb=`ls *.cpp *.c`
aa=`ls *.$ESQL_EXTN`
for mm in $bb 
do
	ee=`echo $mm | cut -d'.' -f1`
	file_type="NOT_ESQL"
	# Exclude the ESQL *.cpp and *.c files
	for nn in $aa 
	do
		dd=`echo $nn | cut -d'.' -f1`
		if [ "$dd" = "$ee" ]; then
				file_type="ESQL"
				break
		fi
	done
	if [ "$file_type" = "ESQL" ]; then
		continue
	fi
	rm -f tags
	$which_tag $mm
	cat tags >> $tag_file2
done
mv -f $tag_file2 tags
cat  $tag_file1 >> tags
rm -f $tag_file1
# Must sort tags file for it work properly ....
sort tags > $tag_file1
mv $tag_file1 tags
