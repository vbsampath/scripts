#!/bin/sh
tmp_tag=tags_file
tmp_tag2=tags_file2
echo " "
echo " "
echo " "
echo " "
echo " "
echo "Generate tags for ...."
while :
do
	echo "    Enter file extension for which you want to generate tags."
	echo -n "    File-extension should be like sh, js, ksh, etc... : "
	read ans
	if [ "$ans" == "" ]; then
		echo " "
		echo "Wrong entry. Try again!"
	else
		break
	fi
done
\rm -f $tmp_tag
aa=`ls *.$ans`
for ii in $aa
do
	jj=`echo $ii | cut -d'.' -f1`
	#echo $jj
	cp $ii $jj.c
	ctags $jj.c
	echo "s/$jj.c/$ii/g" > $tmp_tag2
	sed -f $tmp_tag2 tags >> $tmp_tag
	\rm -f tags $jj.c
done
sort $tmp_tag > tags
\rm -f $tmp_tag $tmp_tag2

