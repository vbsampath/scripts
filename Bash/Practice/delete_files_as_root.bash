1#!/bin/bash
 # delete_as_root.bash - This Nautilus script attempts to delete all the files 
 # given as parameters, if it fails it will ask for password and try again.
 #
 # WARNING: This script will quite certainly succeed in irreversibly deleting 
 #   _any_ file, that's a dangerous thing to have around in a much too easily 
 #   accessible right-click menu...
 #
 
 # Use zenity to make this a little safer to click
/usr/bin/zenity --question \
        --text "Are you sure you want to permanently delete the files: $* ?" \
        || exit

for arg
do
        rm -rf "$arg" || rootargs[${#rootargs}]="$arg"
done
[[ ${#rootargs} -gt 0 ]] && \
        gksudo rm -rf "${rootargs[@]}"
 
