#!/bin/sh

jarCommitFile='/home/sampath/ftp/iCrowd/jarcommit.txt'
jarDir='/home/sampath/Projects/icrowd/Code/development/Component/iCrowdJar/'
currDir=`pwd`
while inotifywait -e modify "$jarCommitFile"; do
  #get last line from the file
  lastLine="`tail -1 "$jarCommitFile"`"
  echo $lastLine
  
  #split the revision from comment
  revision=`echo $lastLine | cut -d \, -f 1`
  comment=`echo $lastLine | cut -d \, -f 2`
  
  echo $revision
  echo $comment
  
  #update svn with the revision number
  #cd $jarDir
  #svn update -r $revision
  #cd $currDir
  
  #notifiy me about update and ask for automatic jar deployment
  kdialog --msgbox "Jar needs love!"

done