#!/usr/bin/env bash

#REMOVE UNNECESSARY BACKUPS AND OLD FILES FROM CURRENT WORKING DIR TO TRASH

#DEPENDS ON ZENITY
#FILE_INFO : This script is used to delete the famous backups and old files on our home directories and it also works well if you have permissions on other directories ....Here I use DEL command which I made it for my purpose for more safety..but if you dont have then you can replace it by rm -f "$a"

#Breif workflow of the script

#First of all I got the list of all removable files by ls command with different switches...If you got many types of other removable files then feel free to add below
#Then I exported the results to a file in trash then I counted the number of files in the file by using basic commands like WC
#Then I used some notification (zenity) of names and number of files we are going to delete
#Then just kep intact "forever gone" notification because it tells me that the process really starts from here
#Then started a for loop with upper limit as number of files to be deleted
#Then checks for the condition if the number of files to be deleted are less than one
#Then checks for various conditions like null, current directory, parent directory
#Then Im in business of deleting my unnecessary files by using DEL command
#Then atlast I delete the file containing the list of files to be deleted 


#Listing out the files to be deleted from various sources like old, backups, chunks, and other backups .....I don't know what they call but they are created when we make changes to files ..It't like backup copy but we don't need much of it 
trash_dir="/home/`whoami`/.local/share/Trash/files"
b="."
c=".."
ls -a1 *~ >> "$trash_dir"/rbkp
ls -a1 .*~ >> "$trash_dir"/rbkp
ls -a1 *.old >> "$trash_dir"/rbkp
ls -a1 *.bak >> "$trash_dir"/rbkp
ls -a1 *.OLD >> "$trash_dir"/rbkp
ls -a1 *.BAK >> "$trash_dir"/rbkp
ls -a1 *.CHK >> "$trash_dir"/rbkp

#Counts the number of entried in the file rbkp which gives the file count to be deleted #in the current working directory
no_files="$(cat "$trash_dir"/rbkp | wc -l)"   

#Gives the names and the number of files to be removed
zenity --text-info --title "These "$no_files" files will be removed" --width=640 --height=480 --filename="$trash_dir"/rbkp

#Initiates the cheking and deletion process
zenity --info --text "Forever Gone."

# Here the $no_files has the total number of files to be deleted but we check for any #erros if any
i=1
until [  "$i" -gt "$no_files" ]; do

#checks whether the entries are less than one 
#if [ "$no_files" -lt 1 ]
#then
#echo 'There are no backup files'
#continue
#else  #Here is the part where there are some files to delete
 
#Here the variable a loads the filename of the file to be deleted or checked for further
a="$(head -$i "$trash_dir"/rbkp | tail -1)"
echo 'file number '$i' and file name '$a' to be deleted'
#	if [ -z "$a" ] #cheking whether the string obtained is null or not
#	then
#	echo 'Some entries are empty which will be omited.'
#	continue
#	fi
	
 	if [ "$a" = "$b" ] #cheking for current directory
 	then 
	echo 'Couldnt delete current directory.'
	let i=i+1
	continue
#	fi
	
 	elif [ "$a" = "$c" ] #cheking for parent directory
 	then 
	echo 'Couldnt delete parent directory.'
	let i=i+1
	continue
#	fi

# deleting the specified filename through the delete command which doesn't remove totally instead it will delete to Trash so that we can recover the files we need or accidentally delete
	else 
	echo 'deleting '$a' '
	del "$a"
	fi

let i=i+1 #incrementing the i value for running the loop till all the files in the filelist are deleted
done  #end of for loop

#deleting the filelist file 
rm -f "$trash_dir"/rbkp



