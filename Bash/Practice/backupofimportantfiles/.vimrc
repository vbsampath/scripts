"highlight Normal guibg=lightyellow
"set guifont=8x13bold

set autoindent
"set ai
"This option sets the editor so that lines following an indented line will have the same indentation as the previous line. 
"If you want to back over this indentation, you can type ^D at the very first character position. 
"This ^D works in the insert mode, and not in command mode. Also, the width of the indentations can be set with shiftwidth
set ignorecase 
set background=dark
syntax enable
filetype on
filetype plugin on
set tabstop=4
set softtabstop=4
set noexpandtab
set smarttab
set noerrorbells
set nocompatible
set si
set cindent
set mouse=a
set nowrap
set incsearch
set showmatch
set mat=5
set wrap
set hls
set cul
set sol
set showcmd
set smartcase
set hidden
:source $VIMRUNTIME/macros/matchit.vim
" You may want to turn off the beep sounds (if you want quite) with visual
" bell
set vb
" " Source in your custom filetypes as given below -
" " so $HOME/vim/myfiletypes.vim
" " Make command line two lines high
set ch=2
" " Make shift-insert work like in Xterm
map <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>
"make F2 to display the tabs list as in :args
map #2 :args<CR>
"make right alt+right buttons to browse between tabs towards right
"map [1;3C :n
"make right alt+left buttons to browse between tabs towards left
"map [1;3D :bn
" Format paragraph
"map #4 ^[:.,/^$/!fmt^[
" Add multiline C program construct
"map #3 ^[i#include ^Mmain(argc, argv) ^M	int argc;^M	char #argv[];^M{^M}^M^[
" tab navigation like firefox
"map th :tabnext<CR>
"map tl :tabprev<CR>
"map tn :tabnew<CR>
"map td :tabclose<CR>
" tab navigation like firefox (doesnt work in kubuntu with vim 7.2)
" :nmap <C-S-tab> :tabprevious<CR>
" :nmap <C-tab> :tabnext<CR>
" :map <C-S-tab> :tabprevious<CR>
" :map <C-tab> :tabnext<CR>
" :imap <C-S-tab> <Esc>:tabprevious<CR>i
" :imap <C-tab> <Esc>:tabnext<CR>i
" :nmap <C-t> :tabnew<CR>
" :imap <C-t> <Esc>:tabnew<CR>

"this setup is working fine for me because Alt is never used in system before
"..so it has no conflicts with other global shortcuts
:map <A-left> :tabprevious<CR>
:map <A-right> :tabnext<CR>
:map <A-up> :tabnew<CR>
:map <A-down> :tabclose<CR>
" Set the tag file search order
set tags=./tags,tags,~/tags,/home/sam/tags
"Turn off messages if this option is unset using :set nomesg, so that nobody can bother you while using the editor.
"set nomesg

set number
"set nonumber
"set nu
"Displays lines with line numbers on the left side.

set shiftwidth=4
"set sw
"This option takes a value, and determines the width of a software tabstop.(The software tabstop is used for the << and >> commands.) For example, you would set a shift width of 4 with this command: :set sw=4.

set showmode
"set noshowmode
"set smd
"This option is used to show the actual mode of the editor that you are in. If you are in insert mode, the bottom line of the screen will say INPUT MODE.

set warn
"This option warns you if you have modified the file, but haven't saved it yet.

"window (wi)
"This option sets up the number of lines on the window that VI uses. For example, to set the VI editor to use only 12 lines of your screen (because your modem is slow) you would use this: :set wi=12.

set wrapscan
"set ws
"This option affects the behavior of the word search. If wrapscan is set, if the word is not found at the bottom of the file, it will try to search for it at the beginning.

set wrapmargin=40
"set wm
"If this option has a value greater than zero, the editor will automatically word wrap. That is, if you get to within that many spaces of the left margin, the word will wrap to the next line, without having to type return. For example, to set the wrap margin to two characters, you would type this: :set wm=2.

set ignorecase
"set noignorecase
"Do not distinguish between capital and lowercase letters during searches.

set autoprint
"set ap
"set noap
"Display changes after each command 

set autowrite
"set aw
"set noaw
"Automatically save file before :n, :!

"set beautify 
"set bf
"set nobf  
"Ignore all control characters during input (except tab, newline, formfeed) 

"set list
"set nolist
"Display all tabs, end of lines

set magic
"set nomagic
"Enable more regex expressions

set mesg
"set nomesg
"Allows mesgs to be sent to terminal 

set open
"set noopen
"Allows open and visual

set optimize 
"set opt
"set nooptimize 
"Optimizes throughput of text by not sending carriage returns when printing text

"set paragraphs= 
"set para=IPLPPPQPPLIbp
"Sets the delimiters for { & }

set prompt
"set noprompt
"Command mode input gives : prompt
