﻿# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
ORACLE_HOSTNAME=localhost.localdomain
export ORACLE_HOSTNAME

export ORACLE_HOME=/home/sam/app/sam/product/11.1.0/db_1
export ORACLE_SID=genie
PATH=$PATH:$HOME/bin:$ORACLE_HOME/bin:/sbin
export TNS_ADMIN=$HOME
export PATH
export o=/home/sam/app/sam/product/11.1.0/db_1
export s=genie
PS1="\[\033[0;36m\][\T \u@\h \W]\\$\[\033[0;38m\]"
export PS1
alias sq='sqlplus / as sysdba'

#alias ll='ls -l'
#alias l= 'ls -C=auto'
#alias la='ls -a'
#alias lla='ls -al'
#PS1="\[\033[1;33m\]\h:\w>\[\033[0;38m\]"
# 30 - Black/Dark grey
# 31 - Red
# 32 - Green
# 33 - Yellow
# 34 - Blue
# 35 - Magenta
# 36 - Fuscia
# 37 - White/light grey
# 38 - Default foreground color. Use it at the end of the prompt, so that further text is not coloured.
#\a an ASCII bell character (07)
#\d the date in "Weekday Month Date" format (e.g., "Tue May 26")
#\e an ASCII escape character (033)
#\h the hostname up to the first `.'
#\H the hostname
#\j the number of jobs currently managed by the shell
#\l the basename of the shell's terminal device name
#\n newline
#\r carriage return
#\s the name of the shell, the basename of $0 (the portion following the final slash)
#\t the current time in 24-hour HH:MM:SS format
#\T the current time in 12-hour HH:MM:SS format
#\@ the current time in 12-hour am/pm format
#\u the username of the current user
#\v the version of bash (e.g., 2.00)
#\V the release of bash, version + patchlevel (e.g., 2.00.0)
#\w the current working directory
#\W the basename of the current working direcÂ­ tory
#\! the history number of this command
#\# the command number of this command
#\$ if the effective UID is 0, a #, otherwise a $
#\nnn the character corresponding to the octal number nnn
#\\ a backslash
#\[ begin a sequence of non-printing characters, which could be used to embed a terminal conÂ­ trol sequence into the prompt
#\] end a sequence of non-printing characters
