#!/bin/bash
if which ntpdate >/dev/null; then
else
 su -c "yum install ntp"
fi

# Check iCrowd DIR 
if [ ! -d /var/log/iCrowd ]; 
then
	mkdir /var/log/iCrowd
	echo "iCrowd log directory doesn't exists so created it."
fi

# Check syncWithUTC.log file 
if [ ! -f /var/log/iCrowd/syncWithUTC.log ]; 
then
	touch /var/log/iCrowd/syncWithUTC.log
	echo "syncWithUTC.log file doesn't exists so created it."
fi

ntpdate pool.ntp.org >> /var/log/iCrowd/syncWithUTC.log 2>&1
