#!/bin/bash


while getopts p: option
do
        case "${option}"
        in
                p) STORAGETARGET=${OPTARG};;
        esac
done


# Check Storage directory
echo "Check Storage directory"
if [ ! -d $STORAGETARGET ]; 
then
	echo "Error - Storage targe directory doesn't exists."
	exit
fi
echo "Check Storage target PATH -- pass"

cd $STORAGETARGET

mkdir iCrowd_Storage
cd iCrowd_Storage
mkdir documents pictures
cd documents
mkdir advisoryboard idea
cd ../pictures
mkdir group idea profile
cd ../..

chown apache:apache -R iCrowd_Storage
chmod g+w -R iCrowd_Storage
