#!/bin/bash

#===========================
# Dependency functions
#===========================
source os_utils.sh
#source zend_utils.sh

#===========================
# Get input parameters
#===========================

while getopts p:j:z:f:e:b:r:n:u: option
do
        case "${option}"
        in
                p) PHPCODEPATH=${OPTARG};;
                j) JAVAPATH=${OPTARG};;
                z) ZENDPATH=${OPTARG};;
                f) JAVAFILESPATH=${OPTARG};;
                e) EXISTINGPHPCODEPATH=${OPTARG};;
                b) BACKUPDIRSPATH=${OPTARG};;
                r) PERMISSIONSDIRSPATH=${OPTARG};;
                n) SITENAME=${OPTARG};;
                u) USERNAME=${OPTARG};;


        esac
done


#===========================
# Variables
#===========================

# Directories
currentDirectory=''
userHome=''
projectDir=''
completeProjectDir=''
zendBinDir=''
zendSitesConfDir=''
backupDir=''

# strings
project='icrowd'
icrowdJar='ICrowd.jar'
jarExecutablePath='/usr/bin/jar'

# arrays
declare -a properties
declare -a jars
declare -a backupDirs
declare -a permissionsDirs



#===========================
# Check input parameters
#===========================

echo "
#===========================
# Checking input parameters
#===========================



"

# Check PHP CODE 
echo "Check PHP CODE PATH"
if [ ! -d "$PHPCODEPATH" ]; 
then
	echo "Error - PHP Code directory doesn't exists"
	exit
fi
echo "Check PHP CODE PATH -- pass"



# Check JAVA DIR 
echo "Check JAVA DIR"
if [ ! -d "$JAVAPATH" ]; 
then
	echo "Error - JAVA JARs & Properties directory doesn't exists"
	exit
fi
echo "Check JAVA DIR --pass"



# Check Zend installation path
echo "Check Zend installation path"
if [ ! -d "$ZENDPATH" ]; 
then
	echo "Error - Zend Installation directory doesn't exists"
	exit
fi
echo "Check Zend installation path -- pass"



# Check Java files config path
echo "Check Java files config path"
if [ ! -f "$JAVAFILESPATH" ]; 
then
	echo "Error - JAVA Configuration file doesn't exists"
	exit
fi
echo "Check Java files config path -- pass"



# Check existing php code path
echo "Check existing php code path"
if [ ! -d "$EXISTINGPHPCODEPATH" ]; 
then
	echo "Error - Existing PHP Code directory doesn't exists"
	exit
fi
echo "Check existing php code path -- pass"



# Check backup dirs config path
echo "Check backup dirs config path"
if [ ! -f "$BACKUPDIRSPATH" ]; 
then
	echo "Error - Backup Directories config file doesn't exists"
	exit
fi
echo "Check backup dirs config path -- pass"



# Check permissions dirs config path
echo "Check permissions dirs config path"
if [ ! -f "$PERMISSIONSDIRSPATH" ]; 
then
	echo "Error - Permission Directories config file doesn't exists"
	exit
fi
echo "Check permissions dirs config path -- pass"



# Verify java JARS and PROPERTIES files from the java config file with given javapath directory
echo "Verify java JARS and PROPERTIES files from the java config file with \"$JAVAFILESPATH\" directory"
files="`cat $JAVAFILESPATH`"
filesCount="`cat $JAVAFILESPATH | wc -l`"
fileCount=0
for file in $files
do
	# Check each Java file is existing
	cd $JAVAPATH
	if [ -f "$file" ]; then
		fileCount=$((fileCount+1))
	fi
done

if [ $fileCount -ne $filesCount ];
then
	echo "Error - Files mismatch from config file to $JAVAPATH directory"
	exit
fi
echo "All JARS and PROPERTIES are existing -- pass"



# Check if JAR executable is existing
echo "Check if JAR executable is existing"
if [ ! -f "$jarExecutablePath" ];
then
	echo "Error - JAR executable file doesn't exists in path "$jarExecutablePath""
	exit
fi
echo "Check if JAR executable is existing -- pass"



# Check if Site name in invalid
echo "Check if Site name in invalid"
if [ -z "$SITENAME" ]; 
then
	echo "Error - Site name is empty"
	exit
fi
echo "Check if Site name in invalid -- pass"



# Check if user name in invalid
echo "Check if user name in invalid"
if [ -z "$USERNAME" ]; 
then
	echo "Error - user name is empty"
	exit
fi
echo "Check if user name in invalid -- pass"



# Check if user name exists on machine
echo "Check if user name exists on machine"
if [ -z `grep "^$USERNAME:" /etc/passwd` ]; 
then
	echo "Error - user name is doesn't exists on machine"
	exit
fi
echo "Check if user name exists on machine -- pass"



# Check if user home directory exists
echo "Check if user home directory exists"
if [ -z $(eval echo ~$USERNAME) ]; 
then
	echo "Error - user home directory doesn't exists"
	exit
fi
echo "Check if user home directory exists -- pass"



#===========================
# Setting variables
#===========================

currentDirectory=`pwd`
userHome=$(eval echo ~$USERNAME)
now=$(date +"%m%d%Y-%I%M%S%p")
projectDir=$project"-"$now
zendBinDir="$ZENDPATH"bin/
zendSitesConfDir="$ZENDPATH"etc/sites.d/http/__default__/0/
# Get os name
os=$(os_type)
	#====================================
	# Setting properties and jars arrays
	#====================================
	files="`cat $JAVAFILESPATH`"
	for file in $files
	do	
		# Check each Java file and put them in respective arrays for further processing
		extension=${file//*.}
		if [ "$extension" == 'properties' ]; 
		then
			countP=${#properties[@]}
			properties["$countP"]=$file
		elif [ "$extension" == 'jar' ]; 
		then
			countJ=${#jars[@]}
			jars["$countJ"]=$file
		else
			echo "Error - Files shouldn't be other than JARS and PROPERTIES"
			exit
		fi
	done

	#====================================
	# Setting backup dirs array
	#====================================
	files="`cat $BACKUPDIRSPATH`"
	for file in $files
	do	
		# Read backup dirs config file and put them in arrays for further processing
		countB=${#backupDirs[@]}
		backupDirs["$countB"]=$file
	done

	

	#====================================
	# Setting permission dirs array
	#====================================
	files="`cat $PERMISSIONSDIRSPATH`"
	for file in $files
	do	
		# Read permission dirs config file and put them in arrays for further processing
		countR=${#permissionsDirs[@]}
		permissionsDirs["$countR"]=$file
	done



#===========================
# Main logic
#===========================


	#=============================================================
	# Copy PHP code to a specific folder on users home directory
	#=============================================================

echo "



#=============================================================
# Copy PHP code to a specific folder on users home directory
#=============================================================



"

	# Creating necessary folders for copying PHP code
	echo "Creating necessary folders for copying PHP code"
	if [ ! -d "$userHome"/projects/"$project" ]; 
	then
		mkdir -p "$userHome"/projects/"$project"
		echo "created $userHome"/projects/"$project"
	fi


	# Create a project specific folder
	completeProjectDir="$userHome"/projects/"$project"/"$projectDir"/"$project"/
	# Create a project specific backup folder to save live site stuff
	backupDir="$userHome"/projects/"$project"/"$projectDir"/backup/


	# Creating project specific folder for copying PHP code
	echo "Creating project specific folder for copying PHP code"
	if [ ! -d "$completeProjectDir" ]; 
	then
		mkdir -p "$completeProjectDir"
		echo "created $completeProjectDir"0
	fi


	# Copy PHP code to a folder on user directory
	echo "Copying PHP code "$PHPCODEPATH" to $completeProjectDir"
	\cp -rf $PHPCODEPATH/* $completeProjectDir




	#=============================================================
	# Creating a config file for project under zend sites.d
	#=============================================================


echo "



#=============================================================
# Creating a config file for project under zend sites.d
#=============================================================



"
	
	# goto zend server sites config directory
	cd $zendSitesConfDir


	# Create a config based on the input parameters and directories created for deployment
	conf="Alias /$SITENAME \"$completeProjectDir\"\n
	   \t# This is needed to work-around configuration problem in SLES\n
	    \t<Directory \"$completeProjectDir\">\n
		\t\tOptions +Indexes\n
		\t\tDirectoryIndex index.php\n
		\t\tOrder allow,deny\n
		\t\tAllow from all\n
	    \t</Directory>\n"


	# Check if a config file already exists
	echo "Checking if a config file with name '$SITENAME' already exists"
	if [ -f "$SITENAME".conf ];
	then
		echo "Error - A configuration file with same name exists in the directory "$zendSitesConfDir""
		echo "Do you want to overwrite the file with generated config [y/n]"
		read overwriteDecision
			if [ "$overwriteDecision"=='y' ];
			then
				echo "Overwriting '$SITENAME'.conf with generated config"
				
				# Overwriting a config file
				echo -e $conf > "$SITENAME".conf
				echo "Checking if a config file with name "$SITENAME" already exists -- pass"
			else
				echo "Error - Cannot proceed further"
				exit
			fi
	else
		# Creating a config file
		echo "Creating a config file for '$SITENAME' under "$zendSitesConfDir""
		echo -e $conf > "$SITENAME".conf
		echo "config file with name '$SITENAME'.conf is created"
	fi
		
		
	



	#=============================================================
	# Copying JARS and PROPERTIES into bin directory of zend
	#=============================================================


echo "



#=============================================================
# Copying JARS and PROPERTIES into bin directory of zend
#=============================================================



"
	
	# Change to JAVA Code path
	cd $JAVAPATH
	
	# Copy JARS and apply necessary permissions to it
	echo "Copy JARS and apply necessary permissions to it"
	for i in ${!jars[*]}; do
		file=${jars[$i]}
		echo "Copying $file to "$zendBinDir""
		\cp -uf $JAVAPATH$file $zendBinDir
		chmod 666 $zendBinDir$file
	done
	echo "Copy JARS and apply necessary permissions to it -- done"
	echo ""


	# Copy PROPERTIES and inject them into JAR
	echo "Copy PROPERTIES and inject them into JAR"
	for i in ${!properties[*]}; do
		file=${properties[$i]}
		echo "Copying $file to "$zendBinDir""
		\cp -uf $JAVAPATH$file $zendBinDir
		echo "Injecting "$file" to "$icrowdJar""
		cd $zendBinDir
		\jar -uf $icrowdJar $file
	done
	echo "Copy PROPERTIES and inject them into JAR -- done"





	#=============================================================
	# Back up profile_pics and abdocuments 
	#=============================================================

echo "



#=============================================================
# Back up profile_pics and abdocuments 
#=============================================================



"
	
	# Change to existing live site directory
	cd $EXISTINGPHPCODEPATH
	
	# Creating backup directory for saving existing documents from live site
	echo "Creating necessary folders for backing up documents from live site"
	if [ ! -d $backupDir ]; 
	then
		mkdir -p $backupDir
		echo "created $backupDir"
	fi
	
	# Backing up each directory mentioned in backupDirs config file from current live site to backup location
	echo "Backing up each directory mentioned in backupDirs config file from current live site to backup location"
	for i in ${!backupDirs[*]}; do
		file=${backupDirs[$i]}
		echo "Backing up $file to "$backupDir""
		\cp -rf $file $backupDir
	done
	echo "Backed up necessary folders from current live site  -- done"
	



	#===================================================================
	# Restore profile_pics and abdocuments from backup to deployed site
	#===================================================================


echo "



#===================================================================
# Restore profile_pics and abdocuments from backup to deployed site
#===================================================================



"
	
	# Change to backup directory
	cd $backupDir
	
	# Restoring profile_pics and abdocuments from backup to deployed site
	echo "Restoring profile_pics and abdocuments from backup to deployed site"
	for i in ${!backupDirs[*]}; do
		file=${backupDirs[$i]}
		echo "Restoring $file to "$completeProjectDir""
		\cp -rf $file $completeProjectDir
	done
	echo "Restored necessary folders from backup to live site -- done"



	

	#===================================================================
	# Apply necessary permissions to folders on live site
	#===================================================================

	
echo "



#===================================================================
# Apply necessary permissions to folders on live site
#===================================================================



"

	# Change to project directory for setting permissions
	cd $completeProjectDir
	

	# Setting up permissions for folders on live site
	echo "Setting up permissions for folders on live site"
	for i in ${!permissionsDirs[*]}; do
		file=${permissionsDirs[$i]}
		if [ "$os" == 'debian' ];
		then
			echo "Setting up owner www-data and group www-data for "$file""
			chown www-data:www-data -R $file
		elif [ "$os" == 'centos' ];
		then
			echo "Setting up owner apache and group apache for "$file""
			chown apache:apache -R $file
		else
			echo ""
		fi
		
		echo "Setting up write permissions to group members for "$file""
		chmod g+w -R $file
	done
	echo "Permissions are set on folders for live site -- done"

	

	#===================================================================
	# Apply necessary permissions to files on live site
	#===================================================================


	
echo "



#===================================================================
# Apply necessary permissions to files on live site
#===================================================================



"

	# Change to zend bin directory for setting permissions
	cd $zendBinDir

	# Setting up permissions for JARS on live site
	echo "Setting up permissions for JARS on live site"
	for i in ${!jars[*]}; do
		file=${jars[$i]}
		echo "Setting up 666 permissions to "$file""
		chmod 666 $file
	done
	echo "Setting up permissions for JARS on live site -- done"
	echo ""

	# Setting up permissions for PROPERTIES on live site
	echo "Setting up permissions for PROPERTIES on live site"
	for i in ${!properties[*]}; do
		file=${properties[$i]}
		echo "Setting up 666 permissions to "$file""
		chmod 666 $file
	done
	echo "Setting up permissions for PROPERTIES on live site -- done" 



	#===================================================================
	# Restarting Zend server and Java Bridge
	#===================================================================


	
echo "



#===================================================================
# Restarting Zend server and Java Bridge
#===================================================================



"

	# Change to zend bin directory for setting permissions
	cd $zendBinDir
	
	./zendctl.sh restart
	./java_bridge.sh restart
	
	
cd $currentDirectory

echo ""
echo ""
echo "Deployment is complete. Go check your site"

