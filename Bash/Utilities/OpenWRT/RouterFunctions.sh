#!/bin/bash

id_directory="/var/mobile/Downloads/Scripts/"
id_name="id_rsa_openwrt"
id_file=$id_directory$id_name

username="root"
ip_series="192.168"
subnet="1"
ip="1"
router_ip="$ip_series"".""$subnet"".""$ip"
connection_string="$username""@""$router_ip"

echo $connection_string
exit


# Functions
reboot()
{
	ssh -i $id_file root@192.168.1.1 ls
}

if [ -z "$1" ]
then
  echo "Error: No Function name provided"
  exit $E_BADARGS  # No arg? Bail out.   
fi 

if [ "$1" = "reboot" ]
	then
	  reboot
	else  
	  echo "Usage: Function Name like \"reboot\""
	  exit $E_BADARGS  # No arg? Bail out.
fi

