#!/bin/ash

target_dir='/usr/lib/opkg/info/'
target_dir_escaped="s|$target_dir||g"

text=''
for i in $target_dir*.control
do
  size=`grep Size "$i" | cut -f 2 -d :` name="${i%.control}"
  text=$text$size":"$name" \n"
done

text=`printf "%b" $text | sort -n`
#echo $text

text=`echo $text | sed $target_dir_escaped`
#echo $text

set -- junk $text
shift
for word; do
  #printf $word
  size=`printf $word | cut -d: -f1`
  name=`printf $word | cut -d: -f2`
  #echo $size "-->" $name
  if [ "$size" -gt "1024" ]
	then
	  package_size=$(($size / 1024))
	  kb="Kb"
	  package_size=$package_size$kb
	  echo -e $package_size "\t" $name
  fi
done
