#!/bin/ash

opkg update

#filesystems

opkg install kmod-fs-ext4 
opkg install libext2fs 
opkg install kmod-fs-ntfs 
opkg install ntfs-3g

#usb related

opkg install kmod-usb-storage 
opkg install usbutils 
opkg install block-mount

#important

opkg install fdisk 
opkg install hdparm 
opkg install luci-theme-bootstrap