#!/bin/bash

scripts_directory="/var/mobile/Downloads/Scripts/"
usage_name="usage"
usage_file=$scripts_directory$usage_name

content=$(curl -sL http://portal.beamtele.com/index.php/package-usage | grep right11 | sed 's/<label class=\\'right11\\'>/\ /;s/<\/label>/\r\n/')
variable=$(echo $content | sed 's/<[./]*t[d]>/\n/g')
variable=$(echo $variable | sed 's/\&nbsp\;/\ /g')
variable=$(echo $variable | sed 's/<\/tr>//g')
variable=$(echo $variable | sed 's/<td\scolspan\=\"2\">//g')
variable=$(echo $variable | sed 's/<label//g')
variable=$(echo $variable | sed 's/class\=//g')
variable=$(echo $variable | sed -e 's/right11//')
echo $variable > $usage_file 2>&1
echo $(cat $usage_file | cut -d'>' -f6)