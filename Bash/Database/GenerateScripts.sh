#!/bin/bash
NOWDATE=`date +%d-%m-%y_%H-%M-%S`
log=$(mktemp /tmp/icrowd_$NOWDATE.XXXXXXXXXX.log)

dbName="icrowddeploy"
userName="root"
password="p@ssw0rd"
path="/home/dev/Documents/iCrowd/database_builds/"

cd $path
if [ ! -d $dbName ] ; then
	mkdir $dbName
	cd $dbName
fi

if [ -d $dbName ] ; then
	cd $dbName
fi

mkdir $NOWDATE
cd $NOWDATE

mysqldump --user=$userName --password=$password --add-drop-database  --compatible=ansi --create-options --events --quick --quote-names --dump-date --tz-utc --comments --add-drop-table --add-locks --allow-keywords --complete-insert --extended-insert --lock-tables --skip-triggers $dbName > SchemaAndData
mysqldump --user=$userName --password=$password --add-drop-database  --compatible=ansi --create-options --events --quick --quote-names --dump-date --tz-utc --comments --add-drop-table --add-locks --allow-keywords --complete-insert --extended-insert --lock-tables --no-data --skip-triggers $dbName  >Schema
mysqldump --user=$userName --password=$password --add-drop-database  --compatible=ansi --create-options --events --quick --quote-names --dump-date --tz-utc --comments --routines --no-create-info --no-data --no-create-db --skip-opt --skip-triggers $dbName > Routines-Functions
mysqldump --user=$userName --password=$password --add-drop-database  --compatible=ansi --create-options --events --quick --quote-names --dump-date --tz-utc --comments --triggers --no-create-info --no-data --no-create-db --skip-opt $dbName > Triggers
echo "Generating Scripts Completed"
cd ../..
